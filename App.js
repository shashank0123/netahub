import React, { Component, useEffect } from "react";
import {
  I18nManager
} from "react-native";
import {store} from './src/redux/store';
import AsyncStorage from '@react-native-community/async-storage';
import {Provider} from 'react-redux';
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
import { 
  View,
  Text,
  StyleSheet,TextInput
} from "react-native";
import Navigation from "./src/Navigation";
import {fcmService} from './src/utils/FCMService'
import {localNotificationService} from './src/utils/LocalNotificationService'
import 'react-native-gesture-handler';


const translationGetters = {
  // lazy requires (metro bundler does not support symlinks)
  en: () => require("./src/assets/en-US.json"),
  hi: () => require("./src/assets/hi.json")
};

const translate = memoize(
  (key, config) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key)
);

const setI18nConfig = () => {
  // fallback if no available language fits
  const fallback = { languageTag: "en", isRTL: false };

  const { languageTag, isRTL } =
    RNLocalize.findBestAvailableLanguage(Object.keys(translationGetters)) ||
    fallback;

  // clear translation cache
  translate.cache.clear();
  // update layout direction
  I18nManager.forceRTL(isRTL);
  // set i18n-js config
  i18n.translations = { [languageTag]: translationGetters[languageTag]() };
  i18n.locale = languageTag;
};

const handleLocalizationChange = () => {
    setI18nConfig();
    this.forceUpdate();
  };


export default function App() {

  useEffect(() => {
    Text.defaultProps = Text.defaultProps || {};
Text.defaultProps.allowFontScaling = false;
TextInput.defaultProps = TextInput.defaultProps || {};
TextInput.defaultProps.allowFontScaling = false;
    setI18nConfig();
    RNLocalize.addEventListener("change", handleLocalizationChange);
    fcmService.registerAppWithFCM()
    fcmService.register(onRegister, onNotification, onOpenNotification)
    localNotificationService.configure(onOpenNotification)
    console.log(fcmService)

    function onRegister(token){
      console.log("onRegister", token)
    }

    function onOpenNotification(notify){
      console.log("[App], onOpenNotification", notify)
      alert("Open Notification", notify.body)
    }

    function onNotification(notify) {
      console.log("onNotification ", notify)
      const options = {
        soundName : "default",
        playSound : true
      }




      localNotificationService.showNotification(
        0, 
        notify.title, 
        notify.body,
        notify, 
        options
        ) 
    }
  }, [])

  
  

    
    return (
      <Provider store={store}>
     
      <Navigation/>
      </Provider>
    );
}

