import { StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
export default StyleSheet.create({
    container: {
        width,
        height: height * 1,
        display: 'flex',
    },
    upperBar: {
        width,
        height: height * 0.075,
        // backgroundColor: '#00cced',
        backgroundColor: '#00b8e6',
        flexDirection: 'row',
        alignItems: 'center',
    },
    backIconContainer: {
    },
    headingContainer: {
        width: '60%',
        marginLeft: '10%',
        alignItems: 'center',
    },
    barText: {
        color: "#000000",
        fontSize: height * 0.03,
        fontWeight: 'bold'
    },
    barMenuIcon: {
        height: height * 0.04,
        width: height * 0.04,
        marginLeft: width * 0.04,
    },
    // scrollWrapper: {
    //     width,
    //     display: 'flex',
    //     justifyContent: 'flex-start',
    //     alignItems: 'center',
    //     paddingBottom: 35
    //     // backgroundColor: 'pink',
    // },
    wrapper: {
        width: width * 1,
        height: height * 0.33,
        backgroundColor: '#00cccc',
        // alignItems:'center'
    },
    header: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '8%'
    },
    middleWrapper: {
        width: '100%',
        height:'23%',
        display: 'flex',
        flexDirection: 'row',
        // justifyContent: 'center',
        alignItems: 'center',
        // justifyContent: 'center',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        backgroundColor: '#00cced',
        paddingLeft: 15
    },
    docDetailedWrapper2: {
        width: width * 0.9,
        height: height * 0.27,
        display: 'flex',
        // justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#ffffff',
        marginTop: 10,
        borderRadius: 15,
        shadowColor: 'rgba(1, 1, 1, 1)',
        elevation: 4,
        borderWidth: 1,
        borderColor: '#00cccc'
        // marginLeft: 18,
    },
    boxNominate: {
        width: width * 0.25,
        height: height * 0.04,
        display: 'flex',
        backgroundColor: '#00cced',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        // marginLeft:16
    },
    profileIcon: {
        height: height * 0.045,
        width: height * 0.045,
        borderRadius: 50,
        marginLeft: width * 0.03,
        
    },
    // bellIcon: {
    //     height: height * 0.045,
    //     width: height * 0.045,
    // },
    closeIcon: {
        height: height * 0.04,
        width: height * 0.04,
        marginLeft: width * 0.04,
    },
    nameWrapper: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#ffffff'
    },
    searchWrapper: {
        height: height * 0.075,
        width: width * 0.85,
        // alignSelf:"center",
        marginTop: height * 0.01,
        backgroundColor: "#dedede",
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    boxNominate2: {
        width: width * 0.33,
        height: height * 0.05,
        display: 'flex',
        backgroundColor: '#96e6a9',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        marginLeft: 16
    },
});