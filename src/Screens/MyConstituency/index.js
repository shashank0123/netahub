import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Dimensions,
  Image, Picker, FlatList, I18nManager
} from "react-native";
const { width, height } = Dimensions.get('window');
import InputField from '../../Commons/input';
import { Images } from '../../utils'
import styles from './style';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { myConstituency } from '../../redux/actions/myConstituency';
import { deleteConstituency } from '../../redux/actions/deleteConstituency';
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
import AwesomeAlert from 'react-native-awesome-alerts';
import Header from "../../Commons/header";
const translationGetters = {
  // lazy requires (metro bundler does not support symlinks)
  en: () => require("../../assets/en-US.json"),
  hi: () => require("../../assets/hi.json")
};
const translate = memoize(
  (key, config) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key)
);

class MyConstituency extends Component {
  constructor(props) {
    super(props);
    this._getStorageValue();
    this.state = {
      show: false,
      profile_pic: '',
      showAlert: false
    }
  }
  async _getStorageValue() {
    var value = await AsyncStorage.getItem('token')
    var profile_pic = await AsyncStorage.getItem('profile_pic')
    this.state.profile_pic = profile_pic
    if (value == null) {
      console.log(value)
      this.props.navigation.replace('Login')
    }
    else {
      this.state.token = value
      this.props.myConstituency(value).then(async () => {
        if (this.props.myConstituency_error == "Invalid Token") {
          await AsyncStorage.setItem('token', "")
          this.props.navigation.replace('Login')
        }
        this.setState({ myConstituency: this.props.myConstituency1 })
        // console.log(this.state)
      })
    }
    return value
  }
  deleteCons(item) {
    item.token = this.state.token
    this.props.deleteConstituency(item).then(async () => {
      if (this.props.deleteConstituency_error == "Invalid Token") {
        await AsyncStorage.setItem('token', "")
        this.props.navigation.replace('Login')
      }
      this._getStorageValue()
    })
  }
  showAlert = (item) => {
    this.setState({
      showAlert: true,
      item: item

    });

  };
  hideAlert = () => {
    this.setState({
      showAlert: false,
      item: null
    });
    // this.props.navigation.replace('MyConstituency')
  };
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.upperBar}>
          <View style={styles.backIconContainer}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Image source={Images.backIcon} style={styles.barMenuIcon} />
            </TouchableOpacity>
          </View>
          <View style={styles.headingContainer}>
            <Text style={styles.barText}>My Constituency</Text>
          </View>
          <View>
            <TouchableOpacity onPress={() =>
              this.props.navigation.navigate('Profile')}>
              <Image source={{ uri: 'https://netahub.com/images/user/' + this.state.profile_pic }} style={styles.profileIcon} />
            </TouchableOpacity>
          </View>
        </View>
        {/* <Header title="my_constituency"
          // bellIcon={Images.bell}
          // refer={Images.refer}
          profile_pic={this.state.profile_pic}
          onPressBack={() => this.props.navigation.navigate('Settings')}
          onPressNotification={() => this.props.navigation.navigate('NotificationScreen')}
          onPressProfile={() => this.props.navigation.navigate('Profile')}
          onPressRefer={() => this.props.navigation.navigate('ReferAndEarn')} /> */}
        <View style={styles.wrapper}>
          <View style={styles.header}>
            <Image source={Images.netaHubLogo} style={{ width: width * 0.35, height: height * 0.19, borderRadius: 111 }} />
          </View>
          <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', marginRight: 20, marginTop: 10 }}>
            <TouchableOpacity
              activeOpacity={1}
              style={styles.boxNominate2}
              onPress={() =>
                this.props.navigation.navigate('AddConstituency')
              }>
              <Text style={{ fontWeight: 'bold' }}>New Mapping</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ alignItems: 'center', justifyContent: 'center', marginBottom: '30%' }}>
          <FlatList
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{ paddingBottom: '55%' }}
            data={this.state.myConstituency}
            renderItem={({ item }) => (
              <>
                <View style={styles.docDetailedWrapper2}>
                  <View style={styles.middleWrapper}>

                    <View style={{ position: 'absolute', width: '80%', left: '8%' }}>
                      <Text numberOfLines={1} style={styles.nameWrapper} >Election Name : {item.election_name}</Text>

                    </View>
                    <View style={{ position: "absolute", left: '88%' }}>

                      <TouchableOpacity
                        onPress={() => this.showAlert(item)}
                      // onPress={() => this.deleteCons(item)}
                      >
                        <Image source={Images.trash} style={styles.closeIcon} />
                      </TouchableOpacity>


                    </View>

                  </View>
                  <View style={{
                    flexDirection: 'row', borderBottomColor: '#808080',
                    marginTop: 20, width: '90%', justifyContent: 'space-between'
                  }}>
                    <Text style={{ fontSize: 16, color: '#5e6063', marginTop: 5, }}>Location Type:</Text>
                    <Text numberOfLines={1} style={{ fontSize: 16, color: '#5e6063', marginTop: 5, }}>{item.location_type}</Text>
                  </View>
                  <View style={{
                    flexDirection: 'row', borderBottomColor: '#808080',
                    marginTop: 10, width: '90%', justifyContent: 'space-between'
                  }}>
                    <Text style={{ fontSize: 16, color: '#5e6063', marginTop: 5, }}>Location Name:</Text>
                    <Text numberOfLines={1} style={{ fontSize: 16, color: '#5e6063', marginTop: 5, }}>{item.location_name}</Text>
                  </View>
                  <View style={{
                    flexDirection: 'row', borderBottomColor: '#808080',
                    marginTop: 10, width: '90%', justifyContent: 'space-between'
                  }}>
                    <Text style={{ fontSize: 15, color: '#5e6063', marginTop: 5, fontWeight: 'bold' }}>Constituency:</Text>
                    <Text numberOfLines={1} style={{ fontSize: 16, color: '#5e6063', marginTop: 5, marginBottom: 15, fontWeight: 'bold' }}>{item.cons_name}</Text>
                  </View>
                </View>

              </>
            )}
            keyExtractor={item => item.id}
          />
          <AwesomeAlert
            show={this.state.showAlert}
            showProgress={false}
            title={"NETAHUB"}
            titleStyle={{ fontSize: 14, color: '#000', fontWeight: "bold" }}
            message="Want to Delet this Contituency ?"
            closeOnTouchOutside={true}
            closeOnHardwareBackPress={false}
            showCancelButton={true}
            showConfirmButton={true}
            confirmText="Okay"
            cancelText="No, cancel"
            confirmButtonColor="#00cced"
            onConfirmPressed={() => {
              this.setState({
                showAlert: false
              });
              this.deleteCons(this.state.item);
            }}
            onCancelPressed={() => {
              this.hideAlert();
            }}
          />
        </View>
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  // console.log(state)
  return {
    myConstituency1: state.myConstituency.myConstituency,
    myConstituency_error: state.myConstituency.myConstituency_error,
    deleteConstituency1: state.deleteConstituency.deleteConstituency,
    deleteConstituency_error: state.deleteConstituency.deleteConstituency_error
  };
};
export default connect(mapStateToProps, {
  myConstituency, deleteConstituency
})(MyConstituency);
