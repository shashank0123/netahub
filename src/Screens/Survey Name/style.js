import { StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
export default StyleSheet.create({
    container: {
        width,
        height,
        display: 'flex',
        justifyContent: 'flex-start',
    },
    scrollWrapper: {
        width,
        display: 'flex',
        justifyContent: 'flex-start',
        // alignItems: 'center',
        paddingBottom: 35
        // backgroundColor: 'pink',
    },
    wrapper: {
        width: width * 1,
        height: height * 0.5,
        backgroundColor: '#00cced',
        alignItems: 'center'
    },
    headerStyle: {
        fontSize: 44,
        fontWeight: 'bold',
        color: '#FFFFFF'
    },
    headerStyle1: {
        fontSize: 44,
        fontWeight: 'normal',
        color: '#FFFFFF'
    },
    header: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '8%',
    },
    loginArea: {
        paddingHorizontal: 10,
        paddingVertical: 45,
        borderWidth: 1,
        marginHorizontal: 20,
        borderRadius: 25,
        marginTop: -140,
        borderColor: 'white',
        elevation: 3,
        backgroundColor: "#ffffff"
    },
    loginTitle: {
        paddingLeft: 15,
    },
    boxBtn2: {
        width: width * 0.6,
        height: height * 0.07,
        display: 'flex',
        backgroundColor: '#00cced',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
       
    },
    btnText: {
        fontWeight: 'bold',
        textShadowColor: '#ffffff',
        fontSize: 20,
        color: '#ffffff'
    },
    forgetText: {
        fontWeight: 'bold',
        marginTop: '10%',
        fontSize: 20
    },
    textStyle: {
        fontSize: 14,
        color: '#000',
        fontWeight:'bold'

    },
    formBox: {
        // justifyContent:'center',
        alignItems:'center',
        // borderWidth:1,
        width:"100%",
        // height:'12%',
        flexDirection:'row',
        // borderRadius:width*0.02,
        // borderWidth:2,
        borderBottomWidth:1,
        borderBottomColor:'#808080',
        marginTop:height*0.03,
    },
    searchIcon: {
        height:height*0.04,
        width:height*0.04,
        marginRight:width*0.02,
    },
    formshortInput: {
        width:"80%",
        // height:'100%',
        // marginLeft:width*0.01,
        fontSize:width*0.04,
        justifyContent:'center',
        // marginTop:10
    },
    placeholderText: {
        fontSize:14,
        color:"#6f7a8c",
        marginBottom:15,
        fontWeight:'bold'
    },


});