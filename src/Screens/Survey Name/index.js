import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image, Picker, I18nManager
} from "react-native";
const { width, height } = Dimensions.get('window');
import InputField from '../../Commons/input';
import { Images } from '../../utils'
import styles from './style';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { survey } from '../../redux/actions/survey';
import DateTimePicker from '@react-native-community/datetimepicker';
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
import AwesomeAlert from 'react-native-awesome-alerts';
import Header from "../../Commons/header";
const translationGetters = {
    // lazy requires (metro bundler does not support symlinks)
    en: () => require("../../assets/en-US.json"),
    hi: () => require("../../assets/hi.json")
};
const translate = memoize(
    (key, config) => i18n.t(key, config),
    (key, config) => (config ? key + JSON.stringify(config) : key)
);
class SurveyName extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show_date: false,
            date_placeholder: true,
            selectedValue: '',
            selectedValue1: '',
            election_id: '',
            token: '',
            from_date: '',
            to_date: '',
            survey_name: '',
            survey_desc: '',
            date: new Date(),
            profile_pic: '',
            showAlert: false
        }
        this._getStorageValue();
        var date_ob = new Date();
        this.state.from_date = this.formatDate(date_ob)
    }
    onChangeDate = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        if (Platform.OS === 'ios')
            this.setState({ show_date: true })
        else
            this.setState({ show_date: false })
        var date_ob = new Date();
        date_ob = new Date(currentDate);
        var date = date_ob.getDate();
        var month = date_ob.getMonth() + 1;
        var year = date_ob.getFullYear();
        console.log(year + "-" + month + "-" + date)
        this.state.to_date = this.formatDate(currentDate)
        this.setState({ date: currentDate, date_placeholder: false });
    };
    formatDate = date => {
        date = new Date(date)
        if (date != "0000-00-00"){
        var month = ('0' + (date.getMonth() + 1)).slice(-2);
        var day1 = ('0' + date.getDate()).slice(-2);
        return `${date.getFullYear()}-${month}-${day1}`;
      }
        // {date.getFullYear()}-${month}-${day1}
      };
    showDatepicker = () => {
        this.setState({ show_date: true });
    };
    async _getStorageValue() {
        console.log(this.props.route.params.election_id)
        // this.setState({})
        var value = await AsyncStorage.getItem('token')
        var profile_pic = await AsyncStorage.getItem('profile_pic')
        this.state.profile_pic = profile_pic
        if (value == null) {
            this.props.navigation.replace('Login')
        }
        else {
            this.setState({ token: value, election_id: this.props.route.params.election_id })
        }
        console.log(this.state)
    }
    submitSurvey() {
        this.props.survey(this.state).then(async () => {
            if (this.props.survey_error == "Invalid Token") {
                await AsyncStorage.setItem('token', "")
                this.props.navigation.replace('Login')
            }
            // this.props.navigation.replace('AppHome')
        })
        this.showAlert()
    }
    showAlert = () => {
        this.setState({
            showAlert: true
        });
    };
    hideAlert = () => {
        this.setState({
            showAlert: false
        });
        this.props.navigation.replace('AppHome')
    };
    render() {
        const { showAlert } = this.state;
        return (
            <View style={styles.container}>
                <KeyboardAvoidingView behavior="padding">
                    {/* <ScrollView
                        contentContainerStyle={styles.scrollWrapper}
                        showsVerticalScrollIndicator={false}> */}
                    <View style={styles.wrapper}>
                        <View style={styles.header}>
                            <Image source={Images.netaHubLogo} style={{ width: width * 0.35, height: height * 0.19, borderRadius: 111 }} />
                        </View>
                    </View>
                    <View style={styles.loginArea}>
                        <InputField
                            name={'Survey Name'}
                            //   errorMessage={this.state.loginMsg}
                            placeholder={' Enter Survey Name'}
                            alertMessage={'Please Enter New Survey Name'}
                            onChangeText={survey_name => this.setState({ survey_name })}
                            value={this.state.survey_name}
                        />
                        <InputField
                            name={'Description (Optional)'}
                            //   errorMessage={this.state.loginMsg}
                            placeholder={' Enter Description'}
                            alertMessage={'Please Enter Discription of Survey'}
                            onChangeText={survey_desc => this.setState({ survey_desc })}
                            value={this.state.survey_desc}
                        />
                        <View style={{ marginLeft: 20, marginRight: 10, marginBottom: 20 }}>
                            <Text style={styles.textStyle}>Select Expiry Date</Text>
                            <TouchableOpacity
                                onPress={this.showDatepicker}>
                                <View style={styles.formBox}>
                                    <View style={styles.formshortInput}
                                        onPress={this.showDatepicker}>
                                        <Text style={styles.placeholderText}>{(this.state.date_placeholder) ? (" Expiry Date") : (this.formatDate(this.state.date))}</Text>
                                        {this.state.show_date && (
                                            <DateTimePicker
                                                onPress={this.showDatepicker}
                                                style={styles.formshortInput}
                                                testID="dateTimePicker"
                                                value={this.state.date}
                                                mode='date'
                                                is24Hour={true}
                                                display="default"
                                                onChange={this.onChangeDate}
                                            />
                                        )}
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{ justifyContent: 'center', alignItems: 'center',marginTop: 10, marginBottom: -30 }}>
                            <TouchableOpacity
                                activeOpacity={1}
                                style={styles.boxBtn2}
                                onPress={() =>
                                    this.submitSurvey()
                                }>
                                <Text
                                    style={styles.btnText}>
                                    Create
                                    </Text>
                            </TouchableOpacity>
                        </View>
                        <AwesomeAlert
                            show={this.state.showAlert}
                            showProgress={false}
                            title={"NETAHUB"}
                            titleStyle={{ fontSize: 14, color: '#000', fontWeight: "bold" }}
                            message="Survey Created Succesfully"
                            closeOnTouchOutside={true}
                            closeOnHardwareBackPress={false}
                            //showCancelButton={true}
                            showConfirmButton={true}
                            confirmText="Okay"
                            confirmButtonColor="#00cced"
                            onConfirmPressed={() => {
                                this.hideAlert();
                            }}
                        />
                    </View>
                    {/* </ScrollView> */}
                </KeyboardAvoidingView>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    // console.log(state)
    return {
        survey1: state.survey.survey,
        survey_error: state.survey.survey_error
    };
};
export default connect(mapStateToProps, {
    survey
})(SurveyName);
