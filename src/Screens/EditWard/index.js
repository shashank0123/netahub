import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image, FlatList, I18nManager, ActivityIndicator
} from "react-native";
const { width, height } = Dimensions.get('window');
import InputField from '../../Commons/input';
import { Images } from '../../utils'
import styles from './style';
// import { Picker } from '@react-native-community/picker';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { editWard } from '../../redux/actions/editWard';
import { wardlist } from '../../redux/actions/wardlist';
import { userward } from '../../redux/actions/userward';
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
import AwesomeAlert from 'react-native-awesome-alerts';
import { Picker } from 'react-native-woodpicker';
const LoadingIcon = ({ loadingApp }) => <ActivityIndicator
    style={{ zIndex: 5, position: 'absolute', alignItems: 'center', top: '63%', left: '45%', }} size="large" color="#00b8e6" animating={loadingApp} ></ActivityIndicator>;
const translationGetters = {
    // lazy requires (metro bundler does not support symlinks)
    en: () => require("../../assets/en-US.json"),
    hi: () => require("../../assets/hi.json")
};
const translate = memoize(
    (key, config) => i18n.t(key, config),
    (key, config) => (config ? key + JSON.stringify(config) : key)
);

class EditWard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedValue: '',
            selectedValue1: '',
            profile_pic: '',
            showAlert: false,
            buttonText: 'Next',
            wardlist: [],
            ward_name: '',
            village_id: this.props.route.params.village_id,
            ward_id: 0,
            loadingApp: false,
            pickedWard: '',

        }
        this._getStorageValue()
    }
    data = [
        { label: "Select Ward ", value: 0 },
    ];
    handlePicker = data => {

        this.setState({ pickedWard: data });
        if (this.props.userward1[0] && this.props.userward1[0].ward_id != data.ward_id) {
            this.setState({ buttonText: 'Continue' })
        }
        else
            this.setState({ buttonText: 'Next' })
        this.setState({ ward_id: data.value })
    };
    async _getStorageValue() {

        var value = await AsyncStorage.getItem('token')
        var profile_pic = await AsyncStorage.getItem('profile_pic')
        this.state.profile_pic = profile_pic
        if (value == null) {
            this.props.navigation.replace('Login')
        }
        else {
            this.setState({ 'token': value })
            this.props.wardlist(this.state).then(async () => {
                if (this.props.profile_error == "Invalid Token") {
                    await AsyncStorage.setItem('token', "")
                    this.props.navigation.replace('Login')
                }
                if (this.props.wardlist1[0])
                var wardlist = []
            console.log('e na cholbe')
                wardlist = this.props.wardlist1.map((obj) => {
                    var obj1 = {
                        label: obj.ward_name,
                        value: obj.ward_id
                    }
                    return obj1
                    // console.log(obj1)
                })
                this.data = wardlist
                    this.setState({ wardlist: this.props.wardlist1 })
            })
            if (this.props.wardlist1[0])
                this.props.userward(value).then(async () => {
                    if (this.props.userward_error == "Invalid Token" || this.props.userward_error != "") {
                        await AsyncStorage.setItem('token', "")
                        this.props.navigation.replace('Login')
                    }
                    else
                        if (this.props.userward1 && this.props.userward1[0])
                            this.setState({ userward: this.props.userward1[0].ward_id, selectedValue1: this.props.userward1[0].ward_id, ward_id: this.props.userward1[0].ward_id, 
                                pickedWard : 
                        { 
                            "label": this.props.userward1[0].ward_name, 
                            "value": this.props.userward1[0].ward_id 
                        } })
                })

        }
        return value
    }

    
    renderForm() {

        if (this.state.selectedValue1 == '' || this.state.selectedValue1 == 0)
            return (<>
                <View style={{ marginTop: 15 }}>
                    <InputField
                        name={'Ward Name'}
                        alertMessage={'Please Enter Your Ward Name'}
                        placeholder={'Enter Ward'}
                        onChangeText={
                            ward_name => {

                                this.setState({ ward_name, buttonText: 'Continue' })

                            }}
                        value={this.state.ward_name}
                    />
                </View>
            </>)
        else {

        }
    }

    submitWard() {

        this.setState({ loadingApp: true })

        if (this.state.buttonText == 'Next') {
            this.props.navigation.replace('Profile');
        }
        else
            this.props.editWard(this.state).then(async () => {
                if (this.props.editWard_error == "Invalid Token") {
                    await AsyncStorage.setItem('token', "")
                    this.props.navigation.replace('Login')
                }
                else
                    this.props.navigation.replace('Profile');
            })
    }
    loadingRender() {
        if (this.state.loadingApp) {
            return (
                <LoadingIcon />
            )
        }
    }

    showAlert = () => {
        this.setState({
            showAlert: true
        });
    };
    hideAlert = () => {
        this.setState({
            showAlert: false
        });
        this.props.navigation.replace('Profile')
    };
    render() {
        return (
            <View style={styles.container}>
                <View style={{ zIndex: 5, }}>
                    {this.loadingRender()}
                    <View style={styles.upperBar}>
                        <View style={styles.backIconContainer}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                                <Image source={Images.backIcon} style={styles.barMenuIcon} />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.headingContainer}>
                            <Text style={styles.barText}>Ward</Text>
                        </View>
                        <View>
                            <TouchableOpacity onPress={() =>
                                this.props.navigation.navigate('Profile')}>
                                <Image source={{ uri: 'https://netahub.com/images/user/' + this.state.profile_pic }} style={styles.profileIcon} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <KeyboardAvoidingView behavior="padding">
                        <ScrollView
                            contentContainerStyle={styles.scrollWrapper}
                            showsVerticalScrollIndicator={false}>
                            <View style={styles.wrapper}>
                                <View style={styles.header}>
                                    <Image source={Images.netaHubLogo} style={{ width: width * 0.35, height: height * 0.19, borderRadius: 111 }} />
                                </View>
                            </View>
                            <View style={styles.loginArea}>
                                {/* <InputField
                                name={'Feedback'}
                                //   errorMessage={this.state.loginMsg}
                                placeholder={'Write your Feedback'}
                            /> */}
                                {/* <View style={styles.formShortBox2}>
                                
                                <Picker
                                    selectedValue={this.state.selectedValue1}
                                    style={styles.picker1}
                                    onValueChange={(itemValue, itemIndex) => this.changeWard(itemValue)}>
                                    <Picker.Item label="Add New" value="0" />
                                    {this.state.wardlist.map(nom => <Picker.Item key={nom.ward_id} label={nom.ward_name} value={nom.ward_id} />)}
                                </Picker>
                            </View> */}

                                <View style={styles.pickerBox}>
                                    <Text style={{ fontSize: 16, marginLeft: 5 }}>Ward :</Text>
                                    <Picker
                                        onItemChange={this.handlePicker}
                                        style={styles.picker}
                                        items={this.data}
                                        placeholder="Select Ward"
                                        placeholderStyle={{ color: '#000' }}
                                        item={this.state.pickedWard}
                                    // backdropAnimation={{ opactity: 0 }}
                                    //androidPickerMode="dropdown"
                                    //isNullable
                                    //disable
                                    />
                                    <Image source={Images.dropDown} style={styles.dropdown} />
                                </View>


                               

                                <View style={{ flexDirection: 'row', marginTop: '10%' }}>
                                    <TouchableOpacity
                                        activeOpacity={1}
                                        style={{ flex: 1, borderRadius: 20, width: '60%', justifyContent: 'center', alignItems: 'center', marginBottom: -45, marginLeft: 15 }}
                                        onPress={() => this.props.navigation.goBack()}>
                                        <LinearGradient
                                            colors={['#00cfcf', '#00cfef']}
                                            start={{ x: 0.3, y: 0.1 }}
                                            end={{ x: 0.3, y: 1.9 }}
                                            locations={[0.1, 0.6]}
                                            style={styles.boxBtn2}>
                                            <Text
                                                style={styles.btnText}>
                                                Skip </Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        activeOpacity={1}
                                        style={{ flex: 1, borderRadius: 20, width: '60%', justifyContent: 'center', alignItems: 'center', marginBottom: -45, marginRight: 15 }}
                                        onPress={() =>
                                            this.submitWard()
                                        }>
                                        <LinearGradient
                                            colors={['#00cfcf', '#00cfef']}
                                            start={{ x: 0.3, y: 0.1 }}
                                            end={{ x: 0.3, y: 1.9 }}
                                            locations={[0.1, 0.6]}
                                            style={styles.boxBtn2}>
                                            <Text
                                                style={styles.btnText}>
                                                {this.state.buttonText}
                                            </Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                </View>
                                <AwesomeAlert
                                    show={this.state.showAlert}
                                    showProgress={false}
                                    title={"NETAHUB"}
                                    titleStyle={{ fontSize: 14, color: '#000', fontWeight: "bold" }}
                                    message="Adress Changed Succesfully"
                                    closeOnTouchOutside={true}
                                    closeOnHardwareBackPress={false}
                                    //showCancelButton={true}
                                    showConfirmButton={true}
                                    confirmText="Okay"
                                    confirmButtonColor="#00cced"
                                    onConfirmPressed={() => {
                                        this.hideAlert();
                                    }}
                                />
                            </View>
                        </ScrollView>
                    </KeyboardAvoidingView>
                </View>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    // console.log(state)
    return {
        editWard1: state.editWard.editWard,
        editWard_error: state.editWard.editWard_error,
        userward1: state.userward.userward,
        userward_error: state.userward.userward_error,
        wardlist1: state.wardlist.wardlist,
        wardlist_error: state.wardlist.wardlist_error,
    };
};
export default connect(mapStateToProps, {
    editWard, userward, wardlist
})(EditWard);
