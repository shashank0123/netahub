import React, { Component, setState } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Dimensions,
  Image,
  FlatList,
  I18nManager,
  RefreshControl,
} from 'react-native';
const { width, height } = Dimensions.get('window');
import DateTimePicker from '@react-native-community/datetimepicker';
import AwesomeAlert from 'react-native-awesome-alerts';
import InputField from '../../Commons/input';
import { Images } from '../../utils';
import styles from './style';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { profile } from '../../redux/actions/profile';
import { occupationlist } from '../../redux/actions/occupationlist';
import { qualificationlist } from '../../redux/actions/qualificationlist';
import { saveProfile } from '../../redux/actions/saveProfile';
import { profilepic } from '../../redux/actions/profilepic';
import { Picker } from '@react-native-community/picker';
import ImagePicker from 'react-native-image-picker';
import * as RNLocalize from 'react-native-localize';
import i18n from 'i18n-js';
import memoize from 'lodash.memoize';
import Header from '../../Commons/header';
import { ImageBackground } from 'react-native';
const translationGetters = {
  // lazy requires (metro bundler does not support symlinks)
  en: () => require('../../assets/en-US.json'),
  hi: () => require('../../assets/hi.json'),
};
const translate = memoize(
  (key, config) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key),
);
const Gender = [
  {
    id: 'M',
    name: 'Male',
  },
  {
    id: 'F',
    name: 'Female',
  },
  {
    id: 'O',
    name: 'Others',
  },
];
class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      option: 0,
      date: new Date(),
      dateofbirth: this.formatDate(new Date),
      display_name: '',
      show_date: false,
      date_placeholder: true,
      selectedValue: '',
      selectedValue1: '',
      selectedValue2: '',
      selectedValue3: '',
      imageuri: '',
      email: '',
      profile: {},
      showAlert: false,
      occupationlist: [],
      qualificationlist: [],
      district_id: 0,
      message: "Profile Updated Succesfully",

      // avatarSource: 0
    };
    this._getStorageValue();
  }
  async _getStorageValue() {
    var value = await AsyncStorage.getItem('token');
    var username = await AsyncStorage.getItem('username');
    if (value == null) {
      // console.log(value)
      this.props.navigation.replace('Login');
    } else
      this.props.profile(value, username).then(async () => {
        if (this.props.profile_error == 'Invalid Token') {
          await AsyncStorage.setItem('token', '');
          this.props.navigation.replace('Login');
        }
        this.setState({ profile: this.props.profile1 });
        if (this.props.profile1.gender) {
          this.setState({ selectedValue1: this.props.profile1.gender });
          this.setState({ selectedValue2: this.props.profile1.occupation });
          this.setState({ selectedValue3: this.props.profile1.qualification });
          this.setState({ email: this.props.profile1.email_id });
          this.setState({ display_name: this.props.profile1.display_name });
          this.setState({ district_id: this.props.profile1.district_id });
          this.setState({ dateofbirth: this.props.profile1.dateofbirth });
          this.setState({
            avatarSource: 'https://netahub.com/images/user/' +
              this.props.profile1.photo_path,
          });
          console.log(this.state.selectedValue1);
        }
        this.props.occupationlist(this.state).then(() => {
          this.setState({ occupationlist: this.props.occupationlist1 });
        });
        this.props.qualificationlist(this.state).then(res => {
          console.log(this.props.occupationlist1);
          this.setState({ qualificationlist: this.props.qualificationlist1 });
        });
      });
    this.state.token = value;
    return value;
  }
  componentDidUpdate() {


  }
  showDatepicker = () => {
    this.setState({ show_date: true });
  };
  formatDate = date => {
    if (date != "0000-00-00") {
      date = new Date(date)
      var month = ('0' + (date.getMonth() + 1)).slice(-2);
      var day1 = ('0' + date.getDate()).slice(-2);
      if (typeof date.getFullYear() == 'number')
        return `${date.getFullYear()}-${month}-${day1}`;
      else
        return this.formatDate(new Date())
    }
    else
      return this.formatDate(new Date())
    // {date.getFullYear()}-${month}-${day1}
  };


  onChangeDate = (event, selectedDate) => {
    console.log(selectedDate)
    const currentDate = selectedDate || date;
    if (Platform.OS === 'ios') this.setState({ show_date: true });
    else this.setState({ show_date: false });
    this.setState({
      dateofbirth: this.formatDate(currentDate),
      date: currentDate,
      date_placeholder: false,
    });
  };


  handleSubmit() {
    console.log(this.state.selectedValue1, 'dinesh sir')
    if (this.state.selectedValue1 == '' || this.state.selectedValue1.toString() == 'undefined') {
      this.setState({ message: 'Please select gender' })
    }
    if (this.state.display_name == '') {
      this.setState({ message: "Please Enter Name " })
    }

    if (this.state.display_name != '' && (this.state.selectedValue1 != '' && this.state.selectedValue1.toString() != 'undefined')) {
      console.log(this.state, 'satyam');
      this.props.saveProfile(this.state).then(async () => {
        if (this.props.saveProfile_error == 'Invalid Token') {
          await AsyncStorage.setItem('token', '');
          this.props.navigation.replace('Login');
        }

        // console.log(this.props.profile1)
        // this.setState({ profile: this.props.profile1 })
      });
      this.setState({ message: "Profile Updated Successfully" })
      this.showAlert();
    }
    else {


      this.showAlert();

    }


  }
  showAlert = () => {
    this.setState({
      showAlert: true,
    });
  };
  hideAlert = () => {
    this.setState({
      showAlert: false,
    });
    this.props.navigation.replace('Profile');
  };
  state = {
    avatarSource: null,
  };
  selectImage = async () => {
    let options = {
      title: 'You can choose one image',
      maxWidth: 256,
      maxHeight: 256,
      quality: 0.5,
      base64: true,
      storageOptions: {
        skipBackup: true,
      },
    };
    ImagePicker.showImagePicker(options, response => {
      response.token = this.state.token;
      // this.state.image = response.data

      this.props.profilepic(response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
        // console.log(response)
        this.setState({
          avatarSource: response.uri,
        });
      }
    });
  };
  render() {
    // console.log(this.state.profile.dateofbirth)
    const { showAlert } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.upperBar}>
          <View style={styles.backIconContainer}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>

              <Image source={Images.backIcon} style={styles.barMenuIcon} />
            </TouchableOpacity>
          </View>
          <View style={styles.headingContainer}>
            <Text style={styles.barText}>{translate('my_profile')}</Text>
          </View>
          <View>
            <TouchableOpacity onPress={() =>
              this.props.navigation.navigate('Profile')}>
              <Image source={(this.state.profile.photo_path != '') ? { uri: 'https://netahub.com/images/user/' + this.state.profile.photo_path } : Images.profilePic} style={styles.profileIcon} />
            </TouchableOpacity>
          </View>
        </View>

        <KeyboardAvoidingView behavior="padding">
          <ScrollView
            contentContainerStyle={styles.scrollWrapper}
            showsVerticalScrollIndicator={false}>
            {/* <View style={styles.wrapper}>
            </View> */}
            <View style={styles.wrapper}>
              <LinearGradient
                colors={['#00cccc', '#00cced']}
                start={{ x: 0.16, y: 0.1 }}
                end={{ x: 1.1, y: 1.1 }}
                locations={[0.16, 50]}
                style={styles.upperCont}>
                {/* <View style={styles.upperWrapper}>
                                </View> */}
              </LinearGradient>
              <View
                style={{
                  width: '100%',
                  // justifyContent: 'center',
                  // alignItems: 'center',
                  marginTop: '10%',
                }}>
                <TouchableOpacity onPress={this.selectImage}
                  style={{ justifyContent: 'center', alignItems: 'center' }}>

                  {/* <View style={styles.profileImgWrapper}> */}
                  <Image
                    // source={this.state.avatarSource ? { uri: this.state.avatarSource } : { uri: 'https://netahub.com/images/user/' + this.state.profile.photo_path }}
                    //  source={this.state.avatarSource ? { uri: this.state.avatarSource } : Images.profilePic} style={{ height: 90, width: 90, borderRadius: 50, marginTop: -70, }} 
                    source={{ uri: this.state.avatarSource }}
                    style={{
                      height: 90,
                      width: 90,
                      borderRadius: 50,
                      // marginTop: -120,
                    }}
                  />
                  {/* </View> */}
                </TouchableOpacity>
                {/* <Text style={{ fontSize: 20 }}>{profile.user_name}</Text> */}
                <View style={{ justifyContent: 'center', alignItems: 'flex-end', marginRight: 20 }}>
                  {/* <View style={styles.boxBtn}> */}

                  {/* <Image style={{ width: 16, height: 18,marginLeft:10 }} source={Images.star} /> */}
                  <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <ImageBackground resizeMode={'stretch'} // or cover
                      style={{ width: 56, height: 56, marginLeft: 10, justifyContent: 'center', alignItems: 'center' }} // must be passed from the parent, the number may vary depending upon your screen size
                      source={Images.star}>
                      <Text style={{ fontSize: 12, fontWeight: 'bold', }}>{this.state.profile.user_point}</Text>
                    </ImageBackground>
                    <Text style={{ fontSize: 14, fontWeight: 'bold', marginLeft: 10 }}>Points </Text>
                    {/* </View> */}
                  </View>
                </View>
              </View>
              <View style={{ width: '100%', marginTop: '5%' }}>
                <View
                  style={{
                    flexDirection: 'row',
                    marginLeft: '5%',
                    marginRight: '5%',
                    borderBottomColor: '#808080',
                    borderBottomWidth: 1,
                  }}>
                  <Text style={{ fontSize: 16, color: '#5e6063', marginTop: 13 }}>
                    Name:
                  </Text>
                  <TextInput
                    style={styles.nameInput}
                    underlineColorAndroid="transparent"
                    // placeholder={this.state.profile.display_name}
                    value={this.state.display_name}
                    onChangeText={display_name => this.setState({ display_name })}
                    placeholderTextColor="#808080"
                  />
                </View>



                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginLeft: '5%',
                    marginRight: '5%',
                    borderBottomColor: '#808080',
                    borderBottomWidth: 1,
                    marginTop: 25,
                  }}>
                  <Text style={{ fontSize: 16, color: '#5e6063', marginTop: 13 }}>
                    Gender:
                  </Text>
                  <Picker
                    selectedValue={this.state.selectedValue1}
                    style={styles.textinput}
                    onValueChange={(itemValue, itemIndex) =>
                      this.setState({ selectedValue1: itemValue })
                    }>
                    <Picker.Item value='' label="Select Gender" />
                    {Gender.map(nom => (

                      <Picker.Item
                        key={nom.id}
                        label={nom.name}
                        value={nom.id}
                      />
                    ))}
                  </Picker>
                </View>



                <View
                  style={{
                    flexDirection: 'row',
                    marginLeft: '5%',
                    marginRight: '5%',
                    borderBottomColor: '#808080',
                    borderBottomWidth: 1,
                    marginTop: 25,
                  }}>
                  <Text
                    style={{
                      fontSize: 16,
                      color: '#5e6063',
                      marginTop: 10,
                      paddingBottom: 15,
                    }}>
                    Age:
                  </Text>





                  <TouchableOpacity
                    style={{ marginTop: '3%', marginLeft: '27%', width: '50%' }}
                    onPress={this.showDatepicker}>

                    <Text style={styles.placeholderText}>
                      {(this.state.dateofbirth && (this.state.dateofbirth != ''))
                        ? this.formatDate(this.state.dateofbirth)
                        : this.formatDate(this.state.date)}
                    </Text>
                    {this.state.show_date && (
                      <DateTimePicker
                        onPress={this.showDatepicker}
                        testID="dateTimePicker"
                        // value={this.state.profile.dateofbirth}
                        value={this.state.date}
                        mode="date"
                        is24Hour={true}
                        display="default"
                        onChange={this.onChangeDate}
                      />
                    )}
                  </TouchableOpacity>






                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    // justifyContent: 'space-between',
                    marginLeft: '5%',
                    marginRight: '5%',
                    borderBottomColor: '#808080',
                    borderBottomWidth: 1,
                    marginTop: 25,
                  }}>
                  <Text style={{ fontSize: 16, color: '#5e6063', marginTop: 10 }}>
                    Email:
                  </Text>
                  <TextInput
                    style={styles.nameInput2}
                    underlineColorAndroid="transparent"
                    placeholder={this.state.profile.email_id}
                    value={this.state.email}
                    onChangeText={email => this.setState({ email })}
                    placeholderTextColor="#808080"
                  />
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    marginLeft: '5%',
                    marginRight: '5%',
                    borderBottomColor: '#808080',
                    borderBottomWidth: 1,
                    marginTop: 25,
                  }}>
                  <Text style={{ fontSize: 16, color: '#5e6063', marginTop: 5 }}>
                    Mobile No:
                  </Text>
                  <Text
                    style={{
                      fontSize: 16,
                      color: '#5e6063',
                      marginTop: 5,
                      marginBottom: 15,
                      marginLeft: '14%',
                    }}>
                    {this.state.profile.mobile_no}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginLeft: '5%',
                    marginRight: '5%',
                    borderBottomColor: '#808080',
                    borderBottomWidth: 1,
                    marginTop: 25,
                  }}>
                  <Text style={{ fontSize: 16, color: '#5e6063', marginTop: 8 }}>
                    Address:
                  </Text>
                  <Text
                    style={{
                      fontSize: 16,
                      color: '#5e6063',
                      marginTop: 5,
                      marginBottom: 15,
                      paddingLeft: '13%',
                      width: '70%',
                    }}>
                    {this.state.profile.village_name}
                  </Text>
                  <TouchableOpacity
                    activeOpacity={1}
                    onPress={() =>
                      this.props.navigation.navigate('EditPincode', {
                        pincode: this.state.profile.pincode,
                      })
                    }>
                    <Image
                      source={Images.pencil}
                      style={{ height: 18, width: 18, marginTop: 10 }}
                    />
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginLeft: '5%',
                    marginRight: '5%',
                    borderBottomColor: '#808080',
                    borderBottomWidth: 1,
                    marginTop: 25,
                  }}>
                  <Text style={{ fontSize: 16, color: '#5e6063', marginTop: 8 }}>
                    Society:
                  </Text>
                  <Text
                    style={{
                      fontSize: 16,
                      color: '#5e6063',
                      marginTop: 5,
                      marginBottom: 15,
                      paddingLeft: '13%',
                      width: '70%',
                    }}>
                    {this.state.profile.society_name}
                  </Text>
                  <TouchableOpacity
                    activeOpacity={1}
                    onPress={() =>
                      this.props.navigation.navigate('EditSociety', {
                        district_id: this.state.district_id,
                      })
                    }>
                    <Image
                      source={Images.pencil}
                      style={{ height: 18, width: 18, marginTop: 10 }}
                    />
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginLeft: '5%',
                    marginRight: '5%',
                    borderBottomColor: '#808080',
                    borderBottomWidth: 1,
                    marginTop: 25,
                  }}>
                  <Text style={{ fontSize: 16, color: '#5e6063', marginTop: 10 }}>
                    Qualif.:
                  </Text>
                  <Picker
                    selectedValue={this.state.selectedValue3}
                    style={styles.textinput}
                    onValueChange={(itemValue, itemIndex) =>
                      this.setState({ selectedValue3: itemValue })
                    }>
                    <Picker.Item label="Select Degree" value="Select Degree" />
                    {this.state.qualificationlist.map(nom => (
                      <Picker.Item
                        key={nom.qualification_id}
                        label={nom.qualification_desc}
                        value={nom.qualification_id}
                      />
                    ))}
                  </Picker>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginLeft: '5%',
                    marginRight: '5%',
                    borderBottomColor: '#808080',
                    borderBottomWidth: 1,
                    marginTop: 25,
                  }}>
                  <Text style={{ fontSize: 16, color: '#5e6063', marginTop: 10 }}>
                    Occup:
                  </Text>
                  <Picker
                    selectedValue={this.state.selectedValue2}
                    style={styles.textinput}
                    onValueChange={(itemValue, itemIndex) =>
                      this.setState({ selectedValue2: itemValue })
                    }>
                    <Picker.Item
                      label="Select Occupation"
                      value="Select Occupation"
                    />
                    {this.state.occupationlist.map(nom => (
                      <Picker.Item
                        key={nom.occupation_id}
                        label={nom.occupation_desc}
                        value={nom.occupation_id}
                      />
                    ))}
                  </Picker>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginLeft: '5%',
                    marginRight: '5%',
                    borderBottomColor: '#808080',
                    borderBottomWidth: 1,
                    marginTop: 25,
                  }}>
                  <Text style={{ fontSize: 16, color: '#5e6063', marginTop: 10 }}>
                    College:
                  </Text>
                  <Text
                    style={{
                      fontSize: 16,
                      color: '#5e6063',
                      marginTop: 5,
                      marginBottom: 15,
                      paddingLeft: '16%',
                      width: '70%',
                    }}>
                    {this.state.profile.college_name}
                  </Text>
                  <TouchableOpacity
                    activeOpacity={1}
                    onPress={() =>
                      this.props.navigation.navigate('EditCollege')
                    }>
                    <Image
                      source={Images.pencil}
                      style={{ height: 18, width: 18, marginTop: 10 }}
                    />
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    marginTop: '10%',
                    marginBottom: 45,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <LinearGradient
                    colors={['#00cfcf', '#00cfef']}
                    start={{ x: 0.3, y: 0.1 }}
                    end={{ x: 0.3, y: 1.9 }}
                    locations={[0.1, 0.6]}
                    style={{
                      width: '50%',
                      borderRadius: 20,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <TouchableOpacity
                      activeOpacity={1}
                      style={styles.boxBtn2}
                      onPress={() => this.handleSubmit()}>
                      <Text style={styles.btnText}>Submit</Text>
                    </TouchableOpacity>
                  </LinearGradient>
                </View>
              </View>
            </View>
            <AwesomeAlert
              show={this.state.showAlert}
              showProgress={false}
              title={'NETAHUB'}
              titleStyle={{ fontSize: 14, color: '#000', fontWeight: 'bold' }}
              message={this.state.message}
              closeOnTouchOutside={true}
              closeOnHardwareBackPress={false}
              //showCancelButton={true}
              showConfirmButton={true}
              confirmText="Okay"
              confirmButtonColor="#00cced"
              onConfirmPressed={() => {
                this.hideAlert();
              }}
            />
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    );
  }
}
const mapStateToProps = state => {
  //console.log(state)
  return {
    profile1: state.profile.profile[0],
    profile_error: state.profile.profile_error,
    saveProfile1: state.saveProfile.saveProfile,
    saveProfile_error: state.saveProfile.saveProfile_error,
    profilepic1: state.profilepic.profilepic,
    profilepic_error: state.profilepic.profilepic_error,
    occupationlist1: state.occupationlist.occupationlist,
    occupationlist_error: state.occupationlist.occupationlist_error,
    qualificationlist1: state.qualificationlist.qualificationlist,
    qualificationlist_error: state.qualificationlist.qualificationlist_error,
  };
};
export default connect(
  mapStateToProps,
  {
    profile,
    saveProfile,
    profilepic,
    occupationlist,
    qualificationlist,
  },
)(Profile);
