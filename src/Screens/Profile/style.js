import { StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
export default StyleSheet.create({
  container: {
    width,
    height,
    display: 'flex',
    justifyContent: 'flex-start',
  },
  upperBar: {
    width,
    height: height * 0.075,
    backgroundColor: '#00b8e6',
    flexDirection: 'row',
    alignItems: 'center',
  },
  backIconContainer: {
  },
  headingContainer: {
    width: '60%',
    marginLeft: '10%',
    alignItems: 'center',
  },
  barText: {
    color: "#000000",
    fontSize: height * 0.03,
    fontWeight: 'bold'
  },
  barMenuIcon: {
    height: height * 0.04,
    width: height * 0.04,
    marginLeft: width * 0.04,
  },
  scrollWrapper: {
    width,
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingBottom: '30%'
    // backgroundColor: 'pink',
  },
  wrapper: {
    width: width * 1,
    // height:height*1,
    // backgroundColor:'#00cccc',
    alignItems: 'center'
  },
  profileIcon: {
    height: height * 0.045,
    width: height * 0.045,
    borderRadius: 50,
    marginLeft: width * 0.03,
  },
  // bellIcon: {
  //     height: height * 0.045,
  //     width: height * 0.045,
  // },
  upperCont: {
    position: 'absolute',
    left: '0%',
    right: '0%',
    top: '-6.04%',
    bottom: '84.26%',
    borderBottomLeftRadius: 190,
    borderBottomRightRadius: 190,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  upperWrapper: {
    width: '100%',
    height: height * 0.23,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  profileImgWrapper: {
    height: height * 0.10,
    width: height * 0.15,
    // marginTop:height*0.02, 
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '5%',
    backgroundColor:'#000'
  },
  textinput: {
    fontSize: 16,
    width: '67%',
    textAlign: 'left'
  },
  emailInput: {
    fontSize: 16,
    width: '80%',
    textAlign: 'right'

  },
  nameInput: {
    fontSize: 16,
    width: '80%',
    marginLeft: '22.5%'


  },
  nameInput2: {
    fontSize: 16,
    width: '80%',
    marginLeft: '21.5%'


  },
  textinput1: {
    fontSize: 16,
    width: '65%',
    textAlign: 'right',
    marginLeft: '20%',
  },
  loginArea: {
    paddingHorizontal: 3,
    // paddingVertical:20,
    borderWidth: 1,
    marginHorizontal: 20,
    borderRadius: 25,
    marginTop: "15%",
    borderColor: 'white',
    elevation: 3,
    backgroundColor: "#ffffff",
    marginBottom: 10,
    paddingBottom: 15
  },
  boxBtn2: {
    width: width * 0.6,
    height: height * 0.07,
    display: 'flex',
    // backgroundColor: '#00cced',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
  },
  btnText: {
    fontWeight: 'bold',
    textShadowColor: '#ffffff',
    fontSize: 20,
    color: '#ffffff'
  },
  placeholderText: {
    color: '#5e6063',
  },
  picker1: {
    // borderWidth:1,
    width: "50%",
    height: '100%',
    marginLeft: width * 0.1,
  },
  boxBtn: {
    width: width * 0.35,
    height: height * 0.035,
    display: 'flex',
    backgroundColor: '#00cfef',
    justifyContent:'flex-start',
    alignItems: 'center',
    borderRadius: 5,
    flexDirection: 'row',
    marginTop: '2%',
    marginLeft:'15%'
},
});