import { StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
export default StyleSheet.create({
    container: {
        width,
        height,
        display: 'flex',
    },
    upperBar: {
        width,
        height: height * 0.075,
        backgroundColor: '#00b8e6',
        flexDirection: 'row',
        alignItems: 'center',
    },
    backIconContainer: {
    },
    headingContainer: {
        width: '50%',
        marginLeft: '10%',
        alignItems: 'center',
    },
    barText: {
        color: "#000000",
        fontSize: height * 0.03,
        fontWeight: 'bold'
    },
    barMenuIcon: {
        height: height * 0.04,
        width: height * 0.04,
        marginLeft: width * 0.04,
    },
    scrollWrapper: {
        width,
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingBottom: 35
        // backgroundColor: 'pink',
    },
   
    docSpecsWrapper: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
    },
    docdetailImg: {
        width: 85,
        height: 85,
        borderRadius: 50,
        borderWidth: 1,
        borderColor: '#ffffff',
        shadowColor: '#000000',
        shadowOffset: { width: 2, height: 2 },
        shadowOpacity: 1,
        shadowRadius: 1,
        elevation: 5,
        marginBottom: 4
    },
    docdetailImg1: {
        width: 85,
        height: 85,
        borderRadius: 50,
        // borderWidth: 1,
        borderColor: '#ffffff',
        shadowColor: '#000000',
        shadowOffset: { width: 2, height: 2 },
        shadowOpacity: 1,
        shadowRadius: 1,
        // elevation: 5,
        marginBottom: 4
    },
    docNameWrapper: {
        width: '90%',
        marginLeft: 25,
        display: 'flex',
        marginTop: 3,
        flexDirection: 'column',
    },
    docNameText: {
        fontFamily: 'Helvetica Neue',
        color: 'green',
        fontSize: width * 0.05,
        fontWeight: 'bold',
        paddingBottom: 1,
    },
    shabhaNameText: {
        fontFamily: 'Helvetica Neue',
        color: 'green',
        fontSize: width * 0.05,
        fontWeight: 'bold',
        paddingBottom: 1,
    },
    docSubNameText: {
        color: 'green',
        fontSize: width * 0.04,
        fontWeight: 'normal',
        fontFamily: 'Helvetica Neue',
        fontStyle: 'normal',
    },
    docSubNameText1: {
        color: 'green',
        fontSize: width * 0.035,
        fontWeight: 'normal',
        fontFamily: 'Helvetica Neue',
        fontStyle: 'normal',
        marginTop:'10%'
        
    },
    totalText: {
        color: 'green',
        fontSize: width * 0.04,
        fontWeight: 'bold',
        fontFamily: 'Helvetica Neue',
       
        marginTop:10,
    },
    middleWrapper: {
       paddingRight:'10%',
       paddingLeft:'10%',
        display: 'flex',
        flexDirection: 'row',
        // justifyContent: 'space-between',
        // alignItems: 'center',
        // justifyContent: 'center',
        marginTop: height * 0.015,
    },
    docDetailedWrapper2: {
        width: width * 0.9,
        height: height * 0.33,
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#ffffff',
        marginTop: -90,
        borderRadius: 15,
        shadowColor: 'rgba(1, 1, 1, 1)',
        elevation: 4,
        // marginLeft: 18,
    },
    docDetailedWrapper: {
        width: width * 0.9,
        height: height * 0.33,
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#ffffff',
        marginTop: 20,
        borderRadius: 15,
        shadowColor: 'rgba(1, 1, 1, 1)',
        elevation: 4,
        // marginLeft: 18,
    },
    docDetailedWrapper1: {
        width: width * 0.9,
        // height: height * 0.45,
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#fff',
        marginTop: 20,
        borderRadius: 15,
        shadowColor: 'rgba(1, 1, 1, 1)',
        elevation: 4,
       
        // marginLeft: 18,
    },
    boxShare: {
        width: width * 0.13,
        height: height * 0.05,
        // marginRight:30,
        display: 'flex',
        backgroundColor: '#00cced',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
    },
    boxShare1: {
        width: width * 0.10,
        height: height * 0.052,
        
        display: 'flex',
        backgroundColor: '#00cced',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        position:'absolute',
        left:'86%'
    },
    boxNominate: {
        width: width * 0.33,
        height: height * 0.05,
        display: 'flex',
        backgroundColor: '#e6a895',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        marginLeft: 16
    },
    boxNominate1: {
        width: width * 0.33,
        height: height * 0.05,
        display: 'flex',
        backgroundColor: '#00cced',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        marginLeft: 16
    },
    boxNominate2: {
        width: width * 0.33,
        height: height * 0.05,
        display: 'flex',
        backgroundColor: '#96e6a9',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        marginLeft: 16
    },
    boxCast: {
        width: width * 0.28,
        height: height * 0.05,
        display: 'flex',
        backgroundColor: '#00cced',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        marginLeft: 5
    },
    profileIcon: {
        height: height * 0.045,
        width: height * 0.045,
        borderRadius: 50,
        marginLeft: width * 0.02,
    },
    bellIcon: {
        height: height * 0.038,
        width: height * 0.0426,
    },
    bellIcon1: {
        height: height * 0.038,
        width: height * 0.035,
    },
    wrapper: {
        width: width * 1,
        height: height * 0.3,
        backgroundColor: '#00cccc',
        alignItems: 'center',
        position: 'absolute'
    },
    boxBtn: {
        width: width * 0.25,
        height: height * 0.035,
        display: 'flex',
        backgroundColor: '#00cfef',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        flexDirection: 'row',
        marginTop: '2%',
        marginLeft:'15%'
    },
});