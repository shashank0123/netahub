import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image, Picker, FlatList, I18nManager, RefreshControl,
} from "react-native";
const { width, height } = Dimensions.get('window');
const screenWidth = Dimensions.get("window").width;
import InputField from '../../Commons/input';
import { Images } from '../../utils'
import Share from "react-native-share";
import styles from './style';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { dashboard } from '../../redux/actions/dashboard';
import {
    PieChart,
} from "react-native-chart-kit";
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
import Header from "../../Commons/header";
const translationGetters = {
    // lazy requires (metro bundler does not support symlinks)
    en: () => require("../../assets/en-US.json"),
    hi: () => require("../../assets/hi.json")
};
const translate = memoize(
    (key, config) => i18n.t(key, config),
    (key, config) => (config ? key + JSON.stringify(config) : key)
);
const chartConfig = {
    backgroundGradientFrom: "#1E2923",
    backgroundGradientFromOpacity: 0,
    backgroundGradientTo: "#08130D",
    backgroundGradientToOpacity: 0.5,
    color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
    strokeWidth: 2, // optional, default 3
    barPercentage: 0.5,
    useShadowColorFromDataset: false // optional
};
const myCustomShare = async (message) => {
    const shareOptions = {
        message: message,
        // urls:''  //for sharing multiple Image files...//
    }
    try {
        const ShareResponse = await Share.open(shareOptions);
        console.log(JSON.stringify(ShareResponse)) // to check which platform is used to share...//
    } catch (error) {
        console.log('Error =>', error);
    }
}
class Dashboard extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dashboarddata: [],
            profile_pic: '',
            refreshing: false,
        }
        this._getStorageValue();
    }
    _onRefresh() {
        this.setState({ refreshing: true });
        this._getStorageValue().then(() => {
            this.setState({ refreshing: false });
        });
    }
    getData(item) {
        var colors2 = ['#FB5607', '#6ECAF1', '#FFBE0B', '#3A86FF', '#8338EC', '#6BEED8', '#FF006F', '#F4E47F', '#FF9281', '#006600',];
        var colors = [];



        if (item.nominee_name) {
            var arrayofnominee = item.nominee_name.split(',');

        }
        else
            var arrayofnominee = [];
        if (item.vote_cnt)
            var votearray = item.vote_cnt.split(',').map(Number);
        else
            var votearray = [];
        var data = [];
        console.log(arrayofnominee, arrayofnominee.length)
        var i;
        for (i = 0; i < arrayofnominee.length; i++) {
            colors.push(colors2[i])
            var temp = {
                name: arrayofnominee[i],
                color: colors[i],
                population: votearray[i],
                legendFontColor: colors[i],
                legendFontSize: 12
            }
            data.push(temp);
            console.log(data)
            console.log(colors)
        }
        return data;
    }
    async _getStorageValue() {
        var value = await AsyncStorage.getItem('token')
        var profile_pic = await AsyncStorage.getItem('profile_pic')
        this.state.profile_pic = profile_pic
        if (value == null) {

            this.props.navigation.replace('Login')
        }
        else {
            this.setState({ token: value })
            console.log('value======================>>>', value)
            this.props.dashboard(value).then(async () => {
                console.log(this.props.dashboard_error)
                if (this.props.dashboard_error == "Invalid Token") {
                    await AsyncStorage.setItem('token', "")
                    this.props.navigation.replace('Login')
                }
                if (this.props.dashboard1.length > 0 && this.props.dashboard1[0].total_vote_cnt != undefined)
                    this.setState({ dashboarddata: this.props.dashboard1 })
                console.log(this.state)
            })
        }
        return value
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.upperBar}>
                    <View style={styles.backIconContainer}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image source={Images.backIcon} style={styles.barMenuIcon} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.headingContainer}>
                        <Text style={styles.barText}>Dashboard</Text>
                    </View>
                    <View style={{ marginLeft: -10 }}>
                        <TouchableOpacity onPress={() =>
                            this.props.navigation.navigate('NotificationScreen')}>
                            <Image source={Images.bell} style={styles.bellIcon1} />
                        </TouchableOpacity>
                    </View>
                    <View >
                        <TouchableOpacity onPress={() =>
                            this.props.navigation.navigate('Profile')}>
                            <Image source={{ uri: 'https://netahub.com/images/user/' + this.state.profile_pic }} style={styles.profileIcon} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ marginLeft: 8 }}>
                        <TouchableOpacity onPress={() =>
                            this.props.navigation.navigate('ReferAndEarn')}>
                            <Image source={Images.refer} style={styles.bellIcon} />
                        </TouchableOpacity>
                    </View>
                </View>
                {/* <Header title="dashboard"
                     profile_pic={this.state.profile_pic}
                     onPressBack={() => this.props.navigation.navigate('Settings')}
                     refer={true}
                     notification={true}
                     onPressProfile={() => this.props.navigation.navigate('Profile')}
                   /> */}
                <KeyboardAvoidingView behavior="padding">
                    <LinearGradient
                        colors={['#00cfcf', '#00cfef']}
                        start={{ x: 0.3, y: 0.1 }}
                        end={{ x: 0.3, y: 1.9 }}
                        locations={[0.1, 0.6]}
                        style={styles.wrapper}>
                    </LinearGradient>
                    <View style={{ alignItems: 'center', marginTop: '1%', marginBottom: '5%' }}>
                        {this.state.dashboarddata.length > 0 ?
                            <FlatList
                                // style=
                                // {{ marginBottom: '14%' }}
                                showsVerticalScrollIndicator={false}
                                contentContainerStyle={{ marginTop: '20%', paddingBottom: '55%' }}
                                data={this.state.dashboarddata}
                                refreshControl={
                                    <RefreshControl
                                        refreshing={this.state.refreshing}
                                        onRefresh={this._onRefresh.bind(this)}
                                    />
                                }
                                renderItem={({ item }) => (

                                    <>
                                        <View style={styles.docDetailedWrapper1}>
                                            <View
                                                style={[styles.docSpecsWrapper, { marginTop: 20 }]}>
                                                <View style={styles.docNameWrapper}>
                                                    <View style={{ marginTop: 3, flexDirection: 'row', }}>
                                                        <Text numberOfLines={1} style={{ position: 'absolute', fontSize: width * 0.05, color: 'green', paddingBottom: 1, fontWeight: 'bold', width: '80%', }}>{item.survey_name}</Text>
                                                    </View>
                                                    <View style={{ marginTop: '10%', flexDirection: 'row' }}>
                                                        <Text style={{ position: 'absolute', fontSize: width * 0.035, color: 'green', paddingBottom: 1, top: '15%' }}>Constituency: </Text>
                                                        <Text numberOfLines={1} style={{ position: 'absolute', left: "40%", fontSize: width * 0.035, color: 'green', paddingBottom: 1, top: '15%', fontWeight: 'bold', }} >{item.cons_name}</Text>
                                                    </View>
                                                    <View style={{ marginTop: '8%', flexDirection: 'row' }}>
                                                        <Text style={{ position: 'absolute', fontSize: width * 0.035, color: 'green' }} >Period: </Text>
                                                        <Text style={{ position: 'absolute', left: "40%", fontSize: width * 0.035, fontWeight: 'bold', color: 'green' }} >{item.from_date}  to  {item.to_date}</Text>
                                                    </View>
                                                    <View style={{ marginTop: '8%', flexDirection: 'row' }}>
                                                        <Text style={styles.totalText}>
                                                            Total vote :
                                                        </Text>
                                                        {/* <Text numberOfLines={1} style={{ fontSize: width * 0.05, fontWeight: 'bold' }}>   {item.total_vote_cnt} </Text> */}
                                                        <View style={styles.boxBtn}>

                                                            <Text numberOfLines={1} style={{ fontSize: width * 0.04, fontWeight: 'bold' }}>
                                                                {item.total_vote_cnt}

                                                            </Text>
                                                        </View>


                                                    </View>
                                                </View>
                                                <TouchableOpacity
                                                    activeOpacity={1}
                                                    style={styles.boxShare1}
                                                    onPress={() => myCustomShare(item.message)}>

                                                    <Image style={{ width: 16.5, height: 18 }} source={Images.share} />
                                                </TouchableOpacity>
                                            </View>
                                            <View style={styles.middleWrapper}>
                                                <PieChart
                                                    data={this.getData(item)}
                                                    width={screenWidth - 50}
                                                    height={150}
                                                    chartConfig={chartConfig}
                                                    accessor="population"
                                                    backgroundColor="transparent"
                                                    paddingLeft={-20}
                                                // absolute
                                                />
                                            </View>
                                        </View>
                                    </>
                                )}
                                keyExtractor={item => item.id}
                            />
                            : <></>}
                    </View>
                </KeyboardAvoidingView>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    // console.log(state)
    return {
        dashboard1: state.dashboard.dashboard,
        dashboard_error: state.dashboard.dashboard_error
    };
};
export default connect(mapStateToProps, {
    dashboard
})(Dashboard);
