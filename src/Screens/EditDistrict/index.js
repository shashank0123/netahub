import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image, FlatList, I18nManager, ActivityIndicator
} from "react-native";
const { width, height } = Dimensions.get('window');
import InputField from '../../Commons/input';
import { Images } from '../../utils'
import styles from './style';
// import { Picker } from '@react-native-community/picker';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { editDistrict } from '../../redux/actions/editDistrict';
import { districtlist } from '../../redux/actions/districtlist';
import { userdistrict } from '../../redux/actions/userdistrict';
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
import { Picker } from 'react-native-woodpicker';
const LoadingIcon = ({ loadingApp }) => <ActivityIndicator
    style={{ zIndex: 5, position: 'absolute', alignItems: 'center', top: '63%', left: '45%', }} size="large" color="#00b8e6" animating={loadingApp} ></ActivityIndicator>;
const translationGetters = {
    // lazy requires (metro bundler does not support symlinks)
    en: () => require("../../assets/en-US.json"),
    hi: () => require("../../assets/hi.json")
};
const translate = memoize(
    (key, config) => i18n.t(key, config),
    (key, config) => (config ? key + JSON.stringify(config) : key)
);
class EditDistrict extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedValue: '',
            selectedValue1: '',
            buttonText: 'Next',
            profile_pic: '',
            state_id: this.props.route.params.state_id,
            districtlist: [],
            district_name: '',
            loadingApp: false,
            pickedDistrict: '',
        }
        this._getStorageValue()
    }
    data = [
        { label: "Select District ", value: 1 },
    ];
    handlePicker = data => {
        this.setState({ pickedDistrict: data });
        console.log(data)
        if (this.props.userdistrict1[0].district_id != data.district_id) {
            this.setState({ buttonText: 'Continue' })
        }
        else
            this.setState({ buttonText: 'Next' })
        this.setState({ district_id: data.value })
    };
    async _getStorageValue() {
        var value = await AsyncStorage.getItem('token')
        var profile_pic = await AsyncStorage.getItem('profile_pic')
        this.state.profile_pic = profile_pic
        if (value == null) {
            this.props.navigation.replace('Login')
        }
        else {
            this.setState({ 'token': value })
            this.props.districtlist(this.state).then(async () => {
                if (this.props.profile_error == "Invalid Token") {
                    await AsyncStorage.setItem('token', "")
                    this.props.navigation.replace('Login')
                }

                var districtlist = []
                districtlist = this.props.districtlist1.map((obj) => {
                    var obj1 = {
                        label: obj.district_name,
                        value: obj.district_id
                    }
                    return obj1
                    // console.log(obj1)
                })
                this.data = districtlist




                this.setState({ districtlist: this.props.districtlist1 })
            })
            this.props.userdistrict(value).then(async () => {
                if (this.props.profile_error == "Invalid Token") {
                    await AsyncStorage.setItem('token', "")
                    this.props.navigation.replace('Login')
                }
                this.setState({ userdistrict: this.props.userdistrict1[0].district_id, selectedValue1: this.props.userdistrict1[0].district_id, district_id: this.props.userdistrict1.district_id, 
                                pickedDistrict : 
                        { 
                            "label": this.props.userdistrict1[0].district_name, 
                            "value": this.props.userdistrict1[0].district_id 
                        }
                            })
            })
        }
        return value
    }
    renderForm() {
        if (this.state.selectedValue1 == '' || this.state.selectedValue1 == 0)
            return (<>
                <View style={{ marginTop: 15 }}>
                    <InputField
                        name={'District Name'}
                        alertMessage={'Please Enter Your District Name'}
                        //   errorMessage={this.state.loginMsg}
                        placeholder={'Enter District'}
                        onChangeText={
                            district_name => {
                                this.setState({ district_name })
                            }}
                        value={this.state.district_name}
                    />
                </View>
            </>)
        else {
        }
    }
    changeDistrict(itemValue) {
        this.setState({ selectedValue1: itemValue, district_id: itemValue })
        console.log(this.props.userdistrict1[0].district_id, itemValue)
        if (this.props.userdistrict1[0].district_id != itemValue) {
            this.setState({ buttonText: 'Continue' })
        }
        else
            this.setState({ buttonText: 'Next' })
    }
    submitDistrict() {
        this.setState({ loadingApp: true })
        if (this.state.buttonText == 'Next') {
            this.props.navigation.replace('EditTehsil', { district_id: this.state.pickedDistrict.value });
        }
        else
            this.props.editDistrict(this.state).then(async () => {
                if (this.props.editDistrict_error == "Invalid Token") {
                    await AsyncStorage.setItem('token', "")
                    this.props.navigation.replace('Login')
                }
                else
                    this.props.navigation.replace('EditTehsil', { district_id: this.state.pickedDistrict.value });
            })
    }
    loadingRender() {
        if (this.state.loadingApp) {
            return (
                <LoadingIcon />
            )
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={{ zIndex: 5, }}>
                    {this.loadingRender()}
                    <View style={styles.upperBar}>
                        <View style={styles.backIconContainer}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                                <Image source={Images.backIcon} style={styles.barMenuIcon} />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.headingContainer}>
                            <Text style={styles.barText}>District</Text>
                        </View>
                        <View>
                            <TouchableOpacity onPress={() =>
                                this.props.navigation.navigate('Profile')}>
                                <Image source={{ uri: 'https://netahub.com/images/user/' + this.state.profile_pic }} style={styles.profileIcon} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <KeyboardAvoidingView behavior="padding">
                        <ScrollView
                            contentContainerStyle={styles.scrollWrapper}
                            showsVerticalScrollIndicator={false}>
                            <View style={styles.wrapper}>
                                <View style={styles.header}>
                                    <Image source={Images.netaHubLogo} style={{ width: width * 0.35, height: height * 0.19, borderRadius: 111 }} />
                                </View>
                            </View>
                            <View style={styles.loginArea}>
                                {/* <View style={styles.formShortBox2}>
                                    <Picker
                                        selectedValue={this.state.selectedValue1}
                                        style={styles.picker1}
                                        onValueChange={(itemValue, itemIndex) => this.changeDistrict(itemValue)}>
                                        <Picker.Item label="Add New" value="0" />
                                        {this.state.districtlist.map(nom => <Picker.Item key={nom.district_id} label={nom.district_name} value={nom.district_id} />)}
                                    </Picker>
                                </View> */}
                                 <View style={styles.pickerBox}>
                                    <Text style={{ fontSize: 16, marginLeft: 5 }}>District :</Text>
                                    <Picker
                                        onItemChange={this.handlePicker}
                                        style={styles.picker}
                                        items={this.data}
                                        placeholder="Add New"
                                        placeholderStyle={{ color: '#000' }}
                                        item={this.state.pickedDistrict}
                                    // backdropAnimation={{ opactity: 0 }}
                                    //androidPickerMode="dropdown"
                                    //isNullable
                                    //disable
                                    />
                                    <Image source={Images.dropDown} style={styles.dropdown} />
                                </View>
                                {this.renderForm()}
                                
                                <View style={{ flexDirection: 'row', marginTop: '5%' }}>
                                    <TouchableOpacity
                                        activeOpacity={1}
                                        style={{ flex: 1, borderRadius: 20, width: '60%', justifyContent: 'center', alignItems: 'center', marginBottom: -45,marginLeft:15 }}
                                        onPress={() => this.props.navigation.goBack()}>
                                        <LinearGradient
                                            colors={['#00cfcf', '#00cfef']}
                                            start={{ x: 0.3, y: 0.1 }}
                                            end={{ x: 0.3, y: 1.9 }}
                                            locations={[0.1, 0.6]}
                                            style={styles.boxBtn2}>
                                            <Text
                                                style={styles.btnText}>
                                                Skip </Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        activeOpacity={1}
                                        style={{ flex: 1, borderRadius: 20, width: '60%', justifyContent: 'center', alignItems: 'center', marginBottom: -45,marginRight:15 }}
                                        onPress={() =>
                                            this.submitDistrict()
                                        }>
                                        <LinearGradient
                                            colors={['#00cfcf', '#00cfef']}
                                            start={{ x: 0.3, y: 0.1 }}
                                            end={{ x: 0.3, y: 1.9 }}
                                            locations={[0.1, 0.6]}
                                            style={styles.boxBtn2}>
                                            <Text
                                                style={styles.btnText}>
                                                {this.state.buttonText}
                                            </Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </ScrollView>
                    </KeyboardAvoidingView>
                </View>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        editDistrict1: state.editDistrict.editDistrict,
        editDistrict_error: state.editDistrict.editDistrict_error,
        districtlist1: state.districtlist.districtlist,
        districtlist_error: state.districtlist.districtlist_error,
        userdistrict1: state.userdistrict.userdistrict,
        userdistrict_error: state.userdistrict.userdistrict_error
    };
};
export default connect(mapStateToProps, {
    editDistrict, districtlist, userdistrict
})(EditDistrict);
