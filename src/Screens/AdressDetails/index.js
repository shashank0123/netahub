import React, { Component, useState } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image, I18nManager,
    Platform, PermissionsAndroid
} from "react-native";
import axios from 'axios'
import Geolocation from '@react-native-community/geolocation';
import Geocoder from 'react-native-geocoding';
import urlObj from '../../redux/url'
const { width, height } = Dimensions.get('window');
import InputField from '../../Commons/input';
import { Images } from '../../utils';
import LinearGradient from 'react-native-linear-gradient';
import { Picker } from '@react-native-community/picker';
import styles from './style';
import i18n from "i18n-js";
import memoize from "lodash.memoize";
import { connect } from 'react-redux';
import { country } from '../../redux/actions/country';
import { statelist } from '../../redux/actions/statelist';
import { districtlist } from '../../redux/actions/districtlist';
import { tehsillist } from '../../redux/actions/tehsillist';
import { blocklist } from '../../redux/actions/blocklist';
import { villagelist } from '../../redux/actions/villagelist';
import { wardlist } from '../../redux/actions/wardlist';
const translationGetters = {
    // lazy requires (metro bundler does not support symlinks)
    en: () => require("../../assets/en-US.json"),
    hi: () => require("../../assets/hi.json")
};
const translate = memoize(
    (key, config) => i18n.t(key, config),
    (key, config) => (config ? key + JSON.stringify(config) : key)
);
const CountryName = [
    {
        id: 1,
        name: "India",
    },
    {
        id: 2,
        name: "Japan",
    },
    {
        id: 3,
        name: 'Australia',
    },
]
const StateName = [
    {
        id: 1,
        name: "Up",
    },
    {
        id: 2,
        name: "Delhi",
    },
    {
        id: 3,
        name: 'J&K',
    },
]
const DistrictName = [
    {
        id: 1,
        name: "Up",
    },
    {
        id: 2,
        name: "Delhi",
    },
    {
        id: 3,
        name: 'J&K',
    },
]
class AdressDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedValue1: '',
            selectedValue2: '',
            selectedValue3: '',
            selectedValue4: '',
            selectedValue5: '',
            selectedValue6: '',
            selectedValue7: '',
            pincode:'',
            token : urlObj.temptoken,
            countrylist:[],
            statelist:[],
            districtlist:[],
            tehsillist:[],
            blocklist:[],
            villagelist:[],
            wardlist:[],
        }
        this.props.country(this.state.token).then(() => {
            this.setState({countrylist: this.props.country1})
        })
        this.requestPermissions()
    }
    async getLocation(){
    console.log('hi')
    Geolocation.getCurrentPosition(
            async (position) => {
              
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                });
                var url = 'https://api.opencagedata.com/geocode/v1/json?key=4f2cbc850b6c4253be149f62129d45ab&q='+position.coords.latitude+'%2C+'+position.coords.longitude+'&pretty=1&no_annotations=1'
                // console.log(url)
                await axios
                  .get(url)
                  .then((response) => {
                    
                    // console
                    console.log(response.data.results[0].components);
                    this.setState({
                      country:response.data.results[0].components.country,
                      county:response.data.results[0].components.county,
                      state:response.data.results[0].components.state,
                      district:response.data.results[0].components.county,
                      building:response.data.results[0].components.building,
                      village:response.data.results[0].components.village,
                      pincode:response.data.results[0].components.postcode,
                    })
                    // console.log(this.state)
                  })
                  .catch((error) => {
                    console.log(error);
                  });

                
            },
            (error) => {
                // See error code charts below.
                this.setState({
                        error: error.message
                    }),
                    console.log(error.code, error.message);
            },
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
     );
  }

  async componentDidMount() {
        // this.getLocation()
    }

    async requestPermissions() {
  if (Platform.OS === 'ios') {
    Geolocation.requestAuthorization();
    Geolocation.setRNConfiguration({
      skipPermissionRequests: false,
     authorizationLevel: 'whenInUse',
   });
  }

  if (Platform.OS === 'android') {
    await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );
  }
  this.getLocation()
}
    render() {
        console.log(this.state)
        return (
            <View style={styles.container}>
                {/* <KeyboardAvoidingView behavior="padding"> */}
                <ScrollView
                    contentContainerStyle={styles.scrollWrapper}
                    showsVerticalScrollIndicator={false}
                >
                    <LinearGradient
                        colors={['#00cccc', '#00cced']}
                        start={{ x: 0.3, y: 0.1 }}
                        end={{ x: 0.3, y: 1.9 }}
                        locations={[0.1, 0.6]}
                        style={styles.wrapper}>
                        <View style={styles.header}>
                            <Image source={Images.netaHubLogo} style={{ width: width * 0.35, height: height * 0.19, borderRadius: 111 }} />
                        </View>
                    </LinearGradient>
                    <View style={styles.loginArea}>
                        <View style={{ marginTop: 40 }}>
                            <InputField
                                name={'Pincode'}
                                onChangeText={(pincode) => {
                                    this.setState({ pincode })}}
                                value={this.state.pincode}
                                placeholder={this.state.pincode}
                            />
                        </View>
                        <View style={styles.formShortBox2}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', paddingLeft: 8 }}>Country:</Text>
                            <Picker
                                selectedValue={this.state.selectedValue1}
                                style={styles.picker1}
                                onValueChange={(itemValue, itemIndex) => this.setState({ selectedValue1: itemValue })}>
                                <Picker.Item label="Country" value="Select Country" />
                                {this.state.countrylist.map(nom => <Picker.Item key={nom.country_id} label={nom.country_name} value={nom.country_id} />)}
                            </Picker>
                        </View>
                        <View style={styles.formShortBox2}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', paddingLeft: 8 }}>State:</Text>
                            <Picker
                                selectedValue={this.state.selectedValue2}
                                style={styles.picker1}
                                onValueChange={(itemValue, itemIndex) => this.setState({ selectedValue2: itemValue })}>
                                <Picker.Item label="Select State" value="Select State" />
                                {StateName.map(nom => <Picker.Item key={nom.id} label={nom.name} value={nom.name} />)}
                            </Picker>
                        </View>
                        <View style={styles.formShortBox2}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', paddingLeft: 8 }}>District:</Text>
                            <Picker
                                selectedValue={this.state.selectedValue3}
                                style={styles.picker1}
                                onValueChange={(itemValue, itemIndex) => this.setState({ selectedValue3: itemValue })}>
                                <Picker.Item label="Select District" value="Select District" />
                                {DistrictName.map(nom => <Picker.Item key={nom.id} label={nom.name} value={nom.name} />)}
                            </Picker>
                        </View>
                        <View style={styles.formShortBox2}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', paddingLeft: 8 }}>Tehsil:</Text>
                            <Picker
                                selectedValue={this.state.selectedValue4}
                                style={styles.picker1}
                                onValueChange={(itemValue, itemIndex) => this.setState({ selectedValue4: itemValue })}>
                                <Picker.Item label="Select Tehsil" value="Select Tehsil" />
                                {CountryName.map(nom => <Picker.Item key={nom.id} label={nom.name} value={nom.name} />)}
                            </Picker>
                        </View>
                        <View style={styles.formShortBox2}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', paddingLeft: 8 }}>Block:</Text>
                            <Picker
                                selectedValue={this.state.selectedValue5}
                                style={styles.picker1}
                                onValueChange={(itemValue, itemIndex) => this.setState({ selectedValue5: itemValue })}>
                                <Picker.Item label="Select Block" value="Select Block" />
                                {CountryName.map(nom => <Picker.Item key={nom.id} label={nom.name} value={nom.name} />)}
                            </Picker>
                        </View>
                        <View style={styles.formShortBox2}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', paddingLeft: 8 }}>Village:</Text>
                            <Picker
                                selectedValue={this.state.selectedValue6}
                                style={styles.picker1}
                                onValueChange={(itemValue, itemIndex) => this.setState({ selectedValue6: itemValue })}>
                                <Picker.Item label="Select Village" value="Select Village" />
                                {CountryName.map(nom => <Picker.Item key={nom.id} label={nom.name} value={nom.name} />)}
                            </Picker>
                        </View>
                        <View style={styles.formShortBox}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', paddingLeft: 8 }}>Ward:</Text>
                            <Picker
                                selectedValue={this.state.selectedValue7}
                                style={styles.picker2}
                                onValueChange={(itemValue, itemIndex) => this.setState({ selectedValue7: itemValue })}>
                                <Picker.Item label="Select Ward" value="Select Ward" />
                                {CountryName.map(nom => <Picker.Item key={nom.id} label={nom.name} value={nom.name} />)}
                            </Picker>
                        </View>
                        <View style={{ position: 'absolute', left: '23%', top: '96.5%' }}>
                            <LinearGradient
                                colors={['#00cfcf', '#00cfef']}
                                start={{ x: 0.3, y: 0.1 }}
                                end={{ x: 0.3, y: 1.9 }}
                                locations={[0.1, 0.6]}
                                style={{ borderRadius: 20, }}>
                                <TouchableOpacity
                                    activeOpacity={1}
                                    style={styles.boxBtn2}
                                    onPress={() =>
                                        this.props.navigation.navigate('OtpRagistration')
                                    }>
                                    <Text
                                        style={styles.btnText}>
                                        Next
                      </Text>
                                </TouchableOpacity>
                            </LinearGradient>
                        </View>
                    </View>
                </ScrollView>
                {/* </KeyboardAvoidingView> */}
            </View>
        );
    }
}
const mapStateToProps = (state) => {
                // console.log(state)
                return {
                  
                  country1: state.country.country,
                  country_error: state.country.country_error,
                  statelist1: state.statelist.statelist,
                  statelist_error: state.statelist.statelist_error,
                  districtlist1: state.districtlist.districtlist,
                  districtlist_error: state.districtlist.districtlist_error,
                  tehsillist1: state.tehsillist.tehsillist,
                  tehsillist_error: state.tehsillist.tehsillist_error,
                  blocklist1: state.blocklist.blocklist,
                  blocklist_error: state.blocklist.blocklist_error,
                  villagelist1: state.villagelist.villagelist,
                  villagelist_error: state.villagelist.villagelist_error,
                  wardlist1: state.wardlist.wardlist,
                  wardlist_error: state.wardlist.wardlist_error,
                  
                };
              };
              export default connect(mapStateToProps, {
                country,statelist,districtlist,tehsillist,blocklist,villagelist,wardlist
              })(AdressDetails);

