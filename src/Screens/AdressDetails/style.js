import { StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
export default StyleSheet.create({
    container: {
        width,
        // height,
        // display: 'flex',
        // justifyContent: 'flex-start',
    },
    scrollWrapper: {
        width,
        height: height * 1.5,
        display: 'flex',
        // alignItems: 'center',
        // backgroundColor: 'pink',
    },
    wrapper: {
        width: width * 1,
        height: height * 0.5,
        backgroundColor: '#00cced',
        alignItems: 'center'
    },
    headerStyle: {
        fontSize: 44,
        fontWeight: 'bold',
        color: '#FFFFFF'
    },
    headerStyle1: {
        fontSize: 44,
        fontWeight: 'normal',
        color: '#FFFFFF'
    },
    header: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '8%'
    },
    loginArea: {
        paddingHorizontal: 10,
        // paddingVertical:15,
        borderWidth: 1,
        marginHorizontal: 20,
        borderRadius: 25,
        marginTop: -150,
        borderColor: 'white',
        elevation: 3,
        backgroundColor: "#ffffff",
        // height,
    },
    loginTitle: {
        paddingLeft: 15,
    },
    boxBtn2: {
        width: width * 0.5,
        height: height * 0.07,
        display: 'flex',
        // backgroundColor: '#00cced',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
    },
    btnText: {
        fontWeight: 'bold',
        textShadowColor: '#ffffff',
        fontSize: 20,
        color: '#ffffff'
    },
    // formShortBox: {
    //     alignItems:'center',
    //     // borderWidth:1,
    //     width:"100%",
    //     height:'100%',
    //     flexDirection:'row',
    //     borderRadius:width*0.02,
    //     // borderWidth:2,
    //     // marginTop:height*0.02,
    //     // marginRight:'2%',
    // },
    searchIcon: {
        height: height * 0.03,
        width: height * 0.03,
        marginLeft: width * 0.02,
    },
    placeholderText: {
        fontSize: width * 0.035,
        marginLeft: width * 0.02,
        fontWeight: 'bold',
        color: '#888888'
    },
    formshortInput: {
        width: "90%",
        height: '75%',
        marginLeft: width * 0.01,
        fontSize: width * 0.04,
        alignSelf: 'flex-end',
    },
    formRow: {
        height: '3%',
        width: '100%',
        justifyContent: "center",
        // alignItems:'center',
        marginTop: 18,
    },
    container1: {
        //    width:'100%',
        borderBottomWidth: 1,
        borderBottomColor: '#888888',
        // marginLeft:15,
        marginBottom: 20,
        marginLeft: 15,
        marginRight: 15
    },
    inputLabel: {
        paddingLeft: 6,
        fontWeight: 'bold'
    },
    picker: {
        borderWidth: 1,
        width: "100%",
        height: '100%',
        marginLeft: width * 0.01,
    },
    checkboxContainer: {
        flexDirection: "row",
        marginBottom: 20,
    },
    checkbox: {
        alignSelf: "center",
    },
    label: {
        margin: 8,
    },
    formShortBox2: {
        justifyContent: 'center',
        // alignItems: 'flex-start',
        // borderWidth:1,
        width: "97%",
        paddingLeft: 10,
        // height: '10%',
        // flexDirection: 'row',
        // borderRadius:width*0.02,
        // borderWidth:2,
        // backgroundColor: '#E8E8E8',
        borderBottomWidth: 1,
        borderBottomColor: '#E8E8E8',
        marginTop: '3%',
        // paddingLeft:10
    },
    formShortBox: {
        justifyContent: 'center',
        // alignItems: 'flex-start',
        // borderWidth:1,
        width: "97%",
        paddingLeft: 10,
        // height: '10%',
        // flexDirection: 'row',
        // borderRadius:width*0.02,
        // borderWidth:2,
        // backgroundColor: '#E8E8E8',
        borderBottomWidth: 1,
        borderBottomColor: '#E8E8E8',
        marginTop: '3%',
        // paddingLeft:10
        marginBottom: 80
       
    },
    picker1: {
        // borderWidth:1,
        width: "99%",
        // height: '70%',
        // marginLeft: width * 0.1,
    },
    picker2: {
        // borderWidth:1,
        width: "99%",
        // height: '70%',
    },
});