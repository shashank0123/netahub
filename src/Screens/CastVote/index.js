import React, { Component, setState } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image, Picker, FlatList, I18nManager
} from "react-native";
const { width, height } = Dimensions.get('window');
import InputField from '../../Commons/input';
import { Images } from '../../utils'
import styles from './style';
import Share from "react-native-share";
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { CastVote } from '../../redux/actions/CastVote';
import { castVoteSave } from '../../redux/actions/castVoteSave';
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
import Header from "../../Commons/header";
const translationGetters = {
    // lazy requires (metro bundler does not support symlinks)
    en: () => require("../../assets/en-US.json"),
    hi: () => require("../../assets/hi.json")
};
const translate = memoize(
    (key, config) => i18n.t(key, config),
    (key, config) => (config ? key + JSON.stringify(config) : key)
);
class CastVotes extends Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedValue: '',
            selectedValue1: '',
            cons_id: this.props.route.params.cons_id,
            survey_id: this.props.route.params.survey_id,
            token: '',
            shareMessage: '',
            nominees: [],
            voted : '',
            profile_pic: ''
        }
        this._getStorageValue();
        console.log(this.state)
    }
    componentDidMount(){
        this.focusListener = this.props.navigation.addListener('focus', () => {
            this._getStorageValue();
            //Put your Data loading function here instead of my this.loadData()
        });
    }
    myCustomShare = async () => {
        const shareOptions = {
            message: this.state.shareMessage,
            // urls:''  //for sharing multiple Image files...//
        }
        try {
            const ShareResponse = await Share.open(shareOptions);
            console.log(JSON.stringify(ShareResponse)) // to check which platform is used to share...//
        } catch (error) {
            console.log('Error =>', error);
        }
    }
    async _getStorageValue() {
        var value = await AsyncStorage.getItem('token')
        var profile_pic = await AsyncStorage.getItem('profile_pic')
        this.state.profile_pic = profile_pic
        if (value == null) {
            console.log(value)
            this.props.navigation.replace('Login')
        }
        else {
            this.setState({ token: value })
            this.props.CastVote(this.state).then(async () => {
                console.log(this.props.CastVote_error)
                if (this.props.CastVote_error == "Invalid Token") {
                    await AsyncStorage.setItem('token', "")
                    this.props.navigation.replace('Login')
                }
                if (this.props.CastVote1 && this.props.CastVote1[0] && this.props.CastVote1[0].nominee_name)
                this.setState({ nominees: this.props.CastVote1 })
                console.log(this.state)
            })
        }
        return value
    }
    submitVote(item) {
        item.token = this.state.token
        item.cons_id = this.state.cons_id
        this.props.castVoteSave(item).then(async () => {
            console.log(this.props.castVoteSave_error)
            if (this.props.castVoteSave_error == "Invalid Token") {
                await AsyncStorage.setItem('token', "")
                this.props.navigation.replace('Login')
            }
            this.props.navigation.replace('CastVotes', { election_id: item.election_id, survey_id: item.survey_id, cons_id: item.cons_id, })
            // this.props.navigation.replace('AppHome')
        })
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.upperBar}>
                    <View style={styles.backIconContainer}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image source={Images.backIcon} style={styles.barMenuIcon} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.headingContainer}>
                        <Text style={styles.barText}>All Nominies</Text>
                    </View>
                    <View>
                        <TouchableOpacity onPress={() =>
                            this.props.navigation.navigate('Profile')}>
                            <Image source={{ uri: 'https://netahub.com/images/user/' + this.state.profile_pic }} style={styles.profileIcon} />
                        </TouchableOpacity>
                    </View>
                </View>
                 
                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={this.state.nominees}
                    renderItem={({ item }) => (
                        <>
                            <View style={styles.docDetailedWrapper2}>
                                <View
                                    style={[styles.docSpecsWrapper, { marginTop: 20 }]}>
                                    <TouchableOpacity onPress={() => {
                                        item.cons_id = this.state.cons_id
                                        this.props.navigation.navigate('NomineeDetails', { nominee: item })
                                        this.state.nominees = []
                                    }}>
                                        <Image
                                          
                                            // source={require('../../Assets/profile_default.png')}
                                            // source={{ uri: 'https://netahub.com/images/user/' + item.photo_path }}
                                           source={(item.photo_path != '') ? { uri: 'https://netahub.com/images/user/' + item.photo_path } : Images.palceholderImage}   style={styles.docdetailImg}
                                        />
                                    </TouchableOpacity>
                                    <View style={styles.docNameWrapper}>
                                        <Text numberOfLines={1} style={styles.docNameText}>
                                            {item.nominee_name}
                                        </Text>
                                        <Text numberOfLines={2} style={styles.docSubNameText}>
                                            {item.party_name}
                                        </Text>
                                    </View>
                                    <TouchableOpacity
                                        activeOpacity={1}
                                        style={styles.boxShare1}
                                        onPress={() => {
                                            this.state.shareMessage = item.message
                                            this.myCustomShare()
                                        }
                                        }
                                    >
                                        <Image style={{ width: 16.5, height: 18, }} source={Images.share} />
                                    </TouchableOpacity>
                                </View>
                                {
                                    (item.voted == 0 )? <TouchableOpacity
                                        activeOpacity={1}
                                        style={styles.boxNominate1}
                                        onPress={() =>
                                            this.submitVote(item)}
                                    >
                                        <LinearGradient
                                            colors={['#00cfcf', '#00cfef']}
                                            start={{ x: 0.3, y: 0.1 }}
                                            end={{ x: 0.3, y: 1.9 }}
                                            locations={[0.1, 0.6]}
                                            style={styles.boxNominate2}
                                        >
                                            <Text style={{ fontWeight: "bold", fontSize: 18 }}>Vote</Text>
                                        </LinearGradient>
                                    </TouchableOpacity> : <TouchableOpacity
                                        activeOpacity={1}
                                        style={[styles.boxNominate1, { backgroundColor: 'green' }]}
                                    >
                                            <Text style={{ fontWeight: "bold", fontSize: 18 }}>Voted</Text>
                                        </TouchableOpacity>}
                            </View>
                        </>
                    )}
                    keyExtractor={item => item.id}
                />
            </View>
            // </View>
        );
    }
}
const mapStateToProps = (state) => {
    // console.log(state)
    return {
        CastVote1: state.CastVote.CastVote,
        CastVote_error: state.CastVote.CastVote_error,
        castVoteSave1: state.castVoteSave.castVoteSave,
        castVoteSave_error: state.castVoteSave.castVoteSave_error
    };
};
export default connect(mapStateToProps, {
    CastVote, castVoteSave
})(CastVotes);
