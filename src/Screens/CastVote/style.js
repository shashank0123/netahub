import { StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
export default StyleSheet.create({
    container: {
        width,
        height,
        display: 'flex',
        justifyContent: 'flex-start',
    },
    upperBar: {
        width,
        height: height * 0.075,
        backgroundColor: '#00b8e6',
        flexDirection: 'row',
        alignItems: 'center',
    },
    backIconContainer: {
    },
    headingContainer: {
        width: '60%',
        marginLeft: '10%',
        alignItems: 'center',
    },
    barText: {
        color: "#000000",
        fontSize: height * 0.03,
        fontWeight: 'bold'
    },
    barMenuIcon: {
        height: height * 0.04,
        width: height * 0.04,
        marginLeft: width * 0.04,
    },
    profileIcon: {
        height: height * 0.045,
        width: height * 0.045,
        borderRadius: 50,
        marginLeft: width * 0.03,
    },
    // bellIcon: {
    //     height: height * 0.045,
    //     width: height * 0.045,
    // },
    wrapper: {
        width: width * 1,
        height: height * 0.1,
        backgroundColor: '#00cccc',
        alignItems: 'center'
    },
    docDetailedWrapper2: {
        width: width * 0.9,
        height: height * 0.15,
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#ffffff',
        marginTop: 20,
        borderRadius: 15,
        shadowColor: 'rgba(1, 1, 1, 1)',
        elevation: 4,
        marginLeft: 18,
    },
    docSpecsWrapper: {
        width: '90%',
        display: 'flex',
        flexDirection: 'row',
    },
    docdetailImg: {
        width: 70,
        height: 70,
        borderRadius: 50,
        borderWidth: 1,
        borderColor: '#ffffff',
        shadowColor: '#000000',
        shadowOffset: { width: 2, height: 2 },
        shadowOpacity: 1,
        shadowRadius: 1,
        elevation: 5,
        // marginBottom: 4
    },
    docNameWrapper: {
        width: '45%',
        marginLeft: 15,
        display: 'flex',
        marginTop: 3,
        flexDirection: 'column',
    },
    docNameWrapper1: {
        // width: '60%',
        // marginLeft:15,
        display: 'flex',
        marginTop: 3,
        flexDirection: 'column',
    },
    docNameText: {
        fontFamily: 'Helvetica Neue',
        color: '#000000',
        fontSize: width * 0.05,
        fontWeight: 'bold',
        paddingBottom: 1,
    },
    docSubNameText: {
        color: '#000000',
        fontSize: width * 0.045,
        fontWeight: 'normal',
        fontFamily: 'Helvetica Neue',
        fontStyle: 'normal',
        marginTop: 8
    },
    boxShare1: {
        width: width * 0.10,
        height: height * 0.052,
        display: 'flex',
        backgroundColor: '#00cced',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        position: 'absolute',
        left: '88%',
        top: -10
    },
    middleWrapper: {
        width: '80%',
        display: 'flex',
        // flexDirection: 'row',
        // justifyContent: 'space-between',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: height * 0.03,
    },
    boxNominate1: {
        width: width * 0.25,
        height: height * 0.045,
        display: 'flex',
        // backgroundColor: '#00cced',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        // marginLeft: 16,
        position: 'absolute',
        left: '70%',
        top: "47%"
    },
    boxNominate2: {
        width: width * 0.25,
        height: height * 0.045,
        display: 'flex',
        // backgroundColor: '#00cced',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        // marginLeft: 16,
        // position:'absolute',
        // left:'70%',
        // top:"50%"
    },
});