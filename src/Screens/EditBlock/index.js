import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image, FlatList,I18nManager,ActivityIndicator
} from "react-native";
const { width, height } = Dimensions.get('window');
import InputField from '../../Commons/input';
import { Images } from '../../utils'
import styles from './style';
// import { Picker } from '@react-native-community/picker';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { editBlock } from '../../redux/actions/editBlock';
import { blocklist } from '../../redux/actions/blocklist';
import { userblock } from '../../redux/actions/userblock';
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
import { Picker } from 'react-native-woodpicker';
const LoadingIcon = ({ loadingApp }) => <ActivityIndicator
    style={{ zIndex: 5, position: 'absolute', alignItems: 'center', top: '63%', left: '45%', }} size="large" color="#00b8e6" animating={loadingApp} ></ActivityIndicator>;
const translationGetters = {
  // lazy requires (metro bundler does not support symlinks)
  en: () => require("../../assets/en-US.json"),
  hi: () => require("../../assets/hi.json")
};

const translate = memoize(
  (key, config) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key)
);

class EditBlock extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedValue: '',
            selectedValue1: '',
            profile_pic:'',
            buttonText:'Next',
            blocklist: [],
            tehsil_id : this.props.route.params.tehsil_id,
            loadingApp: false,
            pickedBlock: '',
        }
        this._getStorageValue()
    }
    data = [
        { label: "Select Block ", value: 1 },
    ];
    handlePicker = data => {
        this.setState({ pickedBlock: data });
        if (this.props.userblock1[0].block_id != data.block_id) {
            this.setState({ buttonText: 'Continue' })
        }
        else
            this.setState({ buttonText: 'Next' })
        this.setState({ block_id: data.value })
    };
    async _getStorageValue(){
        
    var value = await AsyncStorage.getItem('token')
    var profile_pic = await AsyncStorage.getItem('profile_pic')
    this.state.profile_pic = profile_pic
    if (value == null){
      this.props.navigation.replace('Login')
    }
    else{
        this.setState({'token':value})
        this.props.blocklist(this.state).then(async () => {
                if (this.props.profile_error == "Invalid Token") {
                    await AsyncStorage.setItem('token', "")
                    this.props.navigation.replace('Login')
                }
                var blocklist = []
                blocklist = this.props.blocklist1.map((obj) => {
                    var obj1 = {
                        label: obj.block_name,
                        value: obj.block_id
                    }
                    return obj1
                    // console.log(obj1)
                })
                this.data = blocklist


                this.setState({ blocklist: this.props.blocklist1 })
            })
        this.props.userblock(value).then(async () => {
                if (this.props.profile_error == "Invalid Token") {
                    await AsyncStorage.setItem('token', "")
                    this.props.navigation.replace('Login')
                }
                this.setState({ userblock: this.props.userblock1[0].block_id, selectedValue1: this.props.userblock1[0].block_id, block_id: this.props.userblock1.block_id, 
                                pickedBlock : 
                        { 
                            "label": this.props.userblock1[0].block_name, 
                            "value": this.props.userblock1[0].block_id 
                        } })
            })
        
      }
    return value
  }
  renderForm(){

    if (this.state.selectedValue1 == ''  || this.state.selectedValue1== 0)
      return (<>
      <View style={{marginTop:15}}>
        <InputField
            name={'Block Name'}
            alertMessage={'Please Enter Your Block Name'}
            //   errorMessage={this.state.loginMsg}
            placeholder={'Enter Block'}
            onChangeText={
                block_name => {
                    
                    this.setState({ block_name })
                }}
            value={this.state.block_name}
        />
        </View>
        </>)
    else{

    }
  }
  changeBlock(itemValue){
      this.setState({ selectedValue1: itemValue, block_id: itemValue })

      if (this.props.userblock1[0].block_id != itemValue){
          this.setState({buttonText:'Continue'})
      }
      else
          this.setState({buttonText:'Next'})
  }

  submitBlock(){
    this.setState({ loadingApp: true })
      if (this.state.buttonText == 'Next'){
          this.props.navigation.replace('EditVillage', {block_id:this.state.pickedBlock.value});
      }
      else
      this.props.editBlock(this.state).then(async () => {
        if (this.props.editBlock_error == "Invalid Token") {
          await AsyncStorage.setItem('token', "")
          this.props.navigation.replace('Login')
        }
        else 
          this.props.navigation.replace('EditVillage', {block_id:this.state.pickedBlock.value});        
      })
  }
  loadingRender() {
    if (this.state.loadingApp) {
        return (
            <LoadingIcon />
        )
    }
}
    render() {
        return (
            <View style={styles.container}>
                 <View style={{ zIndex: 5, }}>
                    {this.loadingRender()}
                <View style={styles.upperBar}>
                    <View style={styles.backIconContainer}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image source={Images.backIcon} style={styles.barMenuIcon} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.headingContainer}>
                        <Text style={styles.barText}>Block</Text>
                    </View>
                    <View>
                        <TouchableOpacity onPress={() =>
                            this.props.navigation.navigate('Profile')}>
                            <Image source={{ uri: 'https://netahub.com/images/user/' + this.state.profile_pic }} style={styles.profileIcon} />
                        </TouchableOpacity>
                    </View>
                </View>
                <KeyboardAvoidingView behavior="padding">
                    <ScrollView
                        contentContainerStyle={styles.scrollWrapper}
                        showsVerticalScrollIndicator={false}>
                        <View style={styles.wrapper}>
                            <View style={styles.header}>
                            <Image source={Images.netaHubLogo} style={{ width: width *0.35, height: height * 0.19,borderRadius:111 }} />
                            </View>
                        </View>
                        <View style={styles.loginArea}>
                            {/* <InputField
                                name={'Feedback'}
                                //   errorMessage={this.state.loginMsg}
                                placeholder={'Write your Feedback'}
                            /> */}

                            {/* <View style={styles.formShortBox2}>
                                
                                <Picker
                                    selectedValue={this.state.selectedValue1}
                                    style={styles.picker1}
                                    onValueChange={(itemValue, itemIndex) => this.changeBlock(itemValue)}>
                                    <Picker.Item label="Add New" value="0" />
                                    {this.state.blocklist.map(nom => <Picker.Item key={nom.block_id} label={nom.block_name} value={nom.block_id} />)}
                                </Picker>
                            </View> */}
                            <View style={styles.pickerBox}>
                                    <Text style={{ fontSize: 16, marginLeft: 5 }}>Block :</Text>
                                    <Picker
                                        onItemChange={this.handlePicker}
                                        style={styles.picker1}
                                        items={this.data}
                                        placeholder="Select Block"
                                        placeholderStyle={{ color: '#000' }}
                                        item={this.state.pickedBlock}
                                    // backdropAnimation={{ opactity: 0 }}
                                    //androidPickerMode="dropdown"
                                    //isNullable
                                    //disable
                                    />
                                    <Image source={Images.dropDown} style={styles.dropdown} />
                                </View>
                            {this.renderForm()}
                           
                            <View style={{ flexDirection: 'row', marginTop: '5%' }}>
                                    <TouchableOpacity
                                        activeOpacity={1}
                                        style={{ flex: 1, borderRadius: 20, width: '60%', justifyContent: 'center', alignItems: 'center', marginBottom: -45,marginLeft:15 }}
                                        onPress={() => this.props.navigation.goBack()}>
                                        <LinearGradient
                                            colors={['#00cfcf', '#00cfef']}
                                            start={{ x: 0.3, y: 0.1 }}
                                            end={{ x: 0.3, y: 1.9 }}
                                            locations={[0.1, 0.6]}
                                            style={styles.boxBtn2}>
                                            <Text
                                                style={styles.btnText}>
                                                Skip </Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        activeOpacity={1}
                                        style={{ flex: 1, borderRadius: 20, width: '60%', justifyContent: 'center', alignItems: 'center', marginBottom: -45,marginRight:15 }}
                                        onPress={() =>
                                            this.submitBlock()
                                        }>
                                        <LinearGradient
                                            colors={['#00cfcf', '#00cfef']}
                                            start={{ x: 0.3, y: 0.1 }}
                                            end={{ x: 0.3, y: 1.9 }}
                                            locations={[0.1, 0.6]}
                                            style={styles.boxBtn2}>
                                            <Text
                                                style={styles.btnText}>
                                                {this.state.buttonText}
                                            </Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                </View>
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
                </View>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
                // console.log(state)
                return {
                  editBlock1: state.editBlock.editBlock,
                  editBlock_error: state.editBlock.editBlock_error,
                  blocklist1: state.blocklist.blocklist,
                  blocklist_error: state.blocklist.blocklist_error,
                  userblock1: state.userblock.userblock,
                  userblock_error: state.userblock.userblock_error,
                };
              };
              export default connect(mapStateToProps, {
                editBlock, blocklist, userblock
              })(EditBlock);
