import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image, FlatList, I18nManager
} from "react-native";
const { width, height } = Dimensions.get('window');
import InputField from '../../Commons/input';
import { Images } from '../../utils'
import styles from './style';
// import { Picker } from '@react-native-community/picker';
import LinearGradient from 'react-native-linear-gradient';
import ImagePicker from 'react-native-image-picker';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { selfNominate } from '../../redux/actions/selfNominate';
import { party } from '../../redux/actions/party';
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
import AwesomeAlert from 'react-native-awesome-alerts';
import { Picker } from 'react-native-woodpicker';
const translationGetters = {
    // lazy requires (metro bundler does not support symlinks)
    en: () => require("../../assets/en-US.json"),
    hi: () => require("../../assets/hi.json")
};
const translate = memoize(
    (key, config) => i18n.t(key, config),
    (key, config) => (config ? key + JSON.stringify(config) : key)
);
class AddNominee extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedValue: '',
            selectedValue1: '',
            nominee_name: '',
            mobile_no: '',
            nominee_desc: '',
            party: [],
            election_id: this.props.route.params.election_id,
            token: '',
            survey_id: this.props.route.params.survey_id,
            cons_id: this.props.route.params.cons_id,
            filedata: '',
            photo_path: '',
            profile_pic: '',
            showAlert: false,
            pickedParty: '',
            party_id: '',
            avatarSource: 0
        }
        // get > party post > addNominee
        this._getStorageValue();
    }
    data = [
        { label: "Select Party ", value: 1 },
    ];
    handlePicker = data => {
        this.setState({ pickedParty: data });
        console.log(data)
        this.setState({ party_id: data.value })
    };
    async _getStorageValue() {
        console.log(this.props.route.params.election_id)
        // this.setState({})
        var value = await AsyncStorage.getItem('token')
        var profile_pic = await AsyncStorage.getItem('profile_pic')
        this.state.profile_pic = profile_pic
        if (value == null) {
            this.props.navigation.replace('Login')
        }
        else {
            this.setState({ token: value });
            this.props.party(value).then(async () => {
                var party = []
                if (this.props.party1[0]) {
                    party = this.props.party1.map((obj) => {
                        var obj1 = {
                            label: obj.party_name,
                            value: obj.party_id
                        }
                        return obj1
                    }
                    )
                    this.data = party
                }
                this.setState({ party: this.props.party1 })
            })
        }
        return value
    }
    state = {
        avatarSource: null,
    }
    selectImage = async () => {
        let options = {
            title: 'You can choose one image',
            maxWidth: 256,
            maxHeight: 256,
            quality: 0.5,
            base64: true,
            storageOptions: {
                skipBackup: true
            }
        };
        ImagePicker.showImagePicker(options, (response) => {
            this.state.image = response.data
            console.log('Response = ', response);
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                this.setState({
                    avatarSource: response.uri,
                });
            }
        });
    }
    submitButton() {
        this.props.selfNominate(this.state).then(async () => {
            if (this.props.survey_error == "Invalid Token") {
                await AsyncStorage.setItem('token', "")
                this.props.navigation.replace('Login')
            }
            // this.props.navigation.replace('AppHome')
        })
        this.showAlert()
    }
    showAlert = () => {
        this.setState({
            showAlert: true
        });
    };
    hideAlert = () => {
        this.setState({
            showAlert: false
        });
        this.props.navigation.replace('AppHome')
    };
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.upperBar}>
                    <View style={styles.backIconContainer}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image source={Images.backIcon} style={styles.barMenuIcon} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.headingContainer}>
                        <Text style={styles.barText}>{translate('add_nominee')}</Text>
                    </View>
                    <View>
                        <TouchableOpacity onPress={() =>
                            this.props.navigation.navigate('Profile')}>
                            <Image source={{ uri: 'https://netahub.com/images/user/' + this.state.profile_pic }} style={styles.profileIcon} />
                        </TouchableOpacity>
                    </View>
                </View>
                <KeyboardAvoidingView>
                    <ScrollView
                        contentContainerStyle={styles.scrollWrapper}
                        showsVerticalScrollIndicator={false}>
                        <View style={styles.wrapper}>
                        </View>
                        <View style={styles.loginArea}>
                            <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', marginTop: -35 }}>
                                <TouchableOpacity onPress={this.selectImage}>
                                    <View style={styles.profileImgWrapper}>
                                        <Image source={this.state.avatarSource ? { uri: this.state.avatarSource } : Images.profilePic} style={{ height: 90, width: 90, borderRadius: 50, marginTop: -70, }} />
                                        {/* <Image source={{uri:this.state.avatarSource}} style={{ height: 90, width: 90, borderRadius: 50,marginTop:-70,}} /> */}
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <InputField
                                name={'Name'}
                                alertMessage={'Please Enter Name'}
                                //   errorMessage={this.state.loginMsg}
                                placeholder={' Name Here'}
                                onChangeText={nominee_name => this.setState({ nominee_name })}
                                value={this.state.nominee_name}
                            />
                            <InputField
                                name={'Mobile Number'}
                                alertMessage={'Please Enter  Mobile Number'}
                                placeholder={' Phone Number Here'}
                                onChangeText={mobile_no => this.setState({ mobile_no })}
                                value={this.state.mobile_no}
                            //   errorMessage={this.state.loginMsg}
                            />
                            <InputField
                                name={'Description'}
                                alertMessage={'Please Enter Discription'}
                                placeholder={' Desc Here'}
                                onChangeText={nominee_desc => this.setState({ nominee_desc })}
                                value={this.state.nominee_desc}
                            //   errorMessage={this.state.loginMsg}
                            />
                            {/* <View style={styles.formShortBox}>
                                <Text style={{ fontSize: 14, fontWeight: 'bold' }}> {translate('party_name')}:</Text>
                                <Picker
                                    selectedValue={this.state.selectedValue}
                                    style={styles.picker}
                                    onValueChange={(itemValue, itemIndex) => this.setState({ selectedValue: itemValue, party_id: itemValue })}>
                                    <Picker.Item label="Party" value="Select Party" />
                                    {this.state.party.map(gen => <Picker.Item key={gen.party_id} label={gen.party_name} value={gen.party_id} />)}
                                </Picker>
                            </View> */}
                            {/* <View style={styles.pickerBox}> */}
                            <View style={styles.formShortBox}>
                                <Text style={{ fontSize: 16, }}>Party :</Text>
                                <Picker
                                    onItemChange={this.handlePicker}
                                    style={styles.picker}
                                    items={this.data}
                                    placeholder="Select Party"
                                    placeholderStyle={{ color: '#000' }}
                                    item={this.state.pickedParty}
                                // backdropAnimation={{ opactity: 0 }}
                                //androidPickerMode="dropdown"
                                //isNullable
                                //disable
                                />
                                <Image source={Images.dropDown} style={styles.dropdown} />
                            </View>
                            <TouchableOpacity
                                activeOpacity={1}
                                // style={{ flex: 1, borderRadius: 20, width: '60%', justifyContent: 'center', alignItems: 'center',  marginTop: '18%' }}
                              
                                onPress={() =>
                                    this.submitButton()
                                }>
                                <LinearGradient
                                    colors={['#00cfcf', '#00cfef']}
                                    start={{ x: 0.3, y: 0.1 }}
                                    end={{ x: 0.3, y: 1.9 }}
                                    locations={[0.1, 0.6]}
                                    style={styles.boxBtn2}>
                                    <Text
                                        style={styles.btnText}>
                                        {translate('submit')}
                                       
                                    </Text>
                                </LinearGradient>
                            </TouchableOpacity>
                            <AwesomeAlert
                                show={this.state.showAlert}
                                showProgress={false}
                                title={"NETAHUB"}
                                titleStyle={{ fontSize: 14, color: '#000', fontWeight: "bold" }}
                                message="Nominee Created Succesfully"
                                closeOnTouchOutside={true}
                                closeOnHardwareBackPress={false}
                                //showCancelButton={true}
                                showConfirmButton={true}
                                confirmText="Okay"
                                confirmButtonColor="#00cced"
                                onConfirmPressed={() => {
                                    this.hideAlert();
                                }}
                            />
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    // console.log(state)
    return {
        selfNominate1: state.selfNominate.selfNominate,
        selfNominate_error: state.selfNominate.selfNominate_error,
        party1: state.party.party,
        party_error: state.party.party_error
    };
};
export default connect(mapStateToProps, {
    selfNominate, party
})(AddNominee);
