import React, { Component, setState } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Dimensions,
  Image,
  Picker,
  FlatList,
  Modal,
  I18nManager,
  RefreshControl,
  fetchData,
} from 'react-native';
const { width, height } = Dimensions.get('window');
import { Images } from '../../utils';
import styles from './style';
import Share from 'react-native-share';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { elections } from '../../redux/actions/elections';
import { profile } from '../../redux/actions/profile';
import { contribute } from '../../redux/actions/contribute';
import * as RNLocalize from 'react-native-localize';
import i18n from 'i18n-js';
import memoize from 'lodash.memoize';
import AwesomeAlert from 'react-native-awesome-alerts';
import Header from '../../Commons/header';
const translationGetters = {
  // lazy requires (metro bundler does not support symlinks)
  en: () => require('../../assets/en-US.json'),
  hi: () => require('../../assets/hi.json'),
};
const translate = memoize(
  (key, config) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key),
);
class Elections extends Component {
  state = {
    assembly: [],
    sharemessage: 'Custom text',
    modeltitle: '',
    modeldesc: '',
    refreshing: false,
  };
  constructor(props) {
    super(props);
    this._getStorageValue();
    this.state = {
      show: false,
      profile: {},
      profile_pic: 'user.png',
      updatestatus: '1',
    };
  }
  _onRefresh() {
    this.setState({ refreshing: true });
    this._getStorageValue().then(() => {
      this.setState({ refreshing: false });
    });
  }
  myCustomShare = async message => {
    const shareOptions = {
      message: message,
      // urls:''  //for sharing multiple Image files...//
    };
    try {
      const ShareResponse = await Share.open(shareOptions);
      // console.log(JSON.stringify(ShareResponse)) // to check which platform is used to share...//
    } catch (error) {
      // console.log('Error =>', error);
    }
  };
  showAlert = () => {
    this.setState({
      showAlert: true,
    });
  };
  hideAlert = async () => {
    this.setState({
      showAlert: false,
    });
    this.setState({ updatestatus: '5' });
    await AsyncStorage.setItem('updatestatus', '5');
    console.log(this.state.profile, 'satyam yahi hai ')
    if (
      this.state.profile.popup_param != '' &&
      this.state.profile.popup_param != 'NA' &&
      this.state.profile.popup_param != undefined
    ) {
      var name = 'Edit' + this.state.profile.popup_param
      this.props.navigation.navigate(name, {
        district_id: this.state.profile.district_id,
        state_id: this.state.profile.state_id,
        block_id: this.state.profile.block_id,
        country_id: this.state.profile.country_id,
        tehsil_id: this.state.profile.tehsil_id,
        village_id: this.state.profile.village_id,
        ward_id: this.state.profile.ward_id,
      });
    }
  };
  cancelAlert = async () => {
    this.setState({
      showAlert: false,
    });
    this.setState({ updatestatus: '5' });
    await AsyncStorage.setItem('updatestatus', '5');
  };
  async _getStorageValue() {
    var value = await AsyncStorage.getItem('token');
    var username = await AsyncStorage.getItem('username');
    var updatestatus = await AsyncStorage.getItem('updatestatus');
    this.setState({ updatestatus });
    if (value == null) {
      this.props.navigation.replace('Login');
    } else {
      this.props.profile(value, username).then(async () => {
        this.state.profile_pic = this.props.profile1[0].photo_path;
        await AsyncStorage.setItem(
          'profile_pic',
          this.props.profile1[0].photo_path,
        );
        this.setState({ profile: this.props.profile1[0] });
        // .then(()=> {
        if (
          this.props.profile1[0].popup_param != '' &&
          this.props.profile1[0].popup_param != 'NA' &&
          this.props.profile1[0].popup_param != undefined &&
          this.state.updatestatus != 5
        ) {
          this.showAlert();
        }
        else {
          this.setState({ showAlert: false });
        }
        // });
      });
      this.state.token = value;
      this.props.elections(value).then(async () => {
        if (this.props.elections_error == 'Invalid Token') {
          await AsyncStorage.setItem('token', '');
          this.props.navigation.replace('Login');
        }
        this.setState({ assembly: this.props.elections1 });
      });
    }
    return value;
  }
  callContribute(item) {
    // console.log('contribute')
    item.token = this.state.token;
    this.props.contribute(item).then(async () => {
      if (this.props.survey_error == 'Invalid Token') {
        await AsyncStorage.setItem('token', '');
        this.props.navigation.replace('Login');
      }
      this.props.navigation.replace('AppHome');
    });
  }
  renderButton(item) {
    return (
      <>
        {item.nomination_status == 1 ? (
          <TouchableOpacity
            activeOpacity={1}
            style={styles.btnSelfNominate}
            onPress={() =>
              this.props.navigation.navigate('SelfNominate', {
                election_id: item.election_id,
                survey_id: item.survey_id,
                cons_id: item.cons_id,
              })
            }>
            <Text>{translate('self_nominated')}</Text>
          </TouchableOpacity>
        ) : (
          <></>
        )}
        {item.is_enable == 1 ? (
          <TouchableOpacity
            activeOpacity={1}
            style={styles.btnContribute}
            onPress={() => this.callContribute(item)}>
            <Text>{translate('contribute')}</Text>
          </TouchableOpacity>
        ) : (
          <></>
        )}
        {item.nomination_status == 2 ? (
          <TouchableOpacity
            activeOpacity={1}
            style={styles.btnNominated}
          // onPress={() =>
          //   this.props.navigation.navigate('SelfNominate', {election_id : item.election_id,survey_id : item.survey_id,cons_id : item.cons_id,})
          // }
          >
            <Text>{translate('nominated')}</Text>
          </TouchableOpacity>
        ) : (
          <></>
        )}
        {item.nomination_status == 3 ? (
          <TouchableOpacity
            activeOpacity={1}
            style={styles.btnNominate}
            onPress={() =>
              this.props.navigation.navigate('AddNominee', {
                election_id: item.election_id,
                survey_id: item.survey_id,
                cons_id: item.cons_id,
              })
            }>
            <Text>{translate('nominate')}</Text>
          </TouchableOpacity>
        ) : (
          <></>
        )}
        {item.is_eligible == 1 ? (
          <TouchableOpacity
            activeOpacity={1}
            style={styles.btnCastVote}
            onPress={() =>
              this.props.navigation.navigate('CastVotes', {
                election_id: item.election_id,
                survey_id: item.survey_id,
                cons_id: item.cons_id,
              })
            }>
            <Text>{translate('cast_vote')}</Text>
          </TouchableOpacity>
        ) : (
          <></>
        )}
        {item.to_create_survey == 1 && (
          <TouchableOpacity
            activeOpacity={1}
            style={styles.btnCreateSurvey}
            onPress={() =>
              this.props.navigation.navigate('SurveyName', {
                election_id: item.election_id,
              })
            }>
            <Text>{translate('create_survey')}</Text>
          </TouchableOpacity>
        )}
      </>
    );
  }
  renderCard(item) {
    return (
      <View
        style={
          item.is_active == 1
            ? styles.docDetailedWrapper
            : styles.docDetailedWrapperdark
        }>
        <View style={[styles.docSpecsWrapper, { marginTop: 20 }]}>
          <Image
            style={styles.docdetailImg}
            source={{ uri: 'https://netahub.com/images/' + item.election_image }}
          />
          <View style={styles.docNameWrapper}>
            <Text
              numberOfLines={1}
              style={
                item.is_active == 1 ? styles.docNameText : styles.docNameText2
              }>
              {item.election_name}
            </Text>
            <Text numberOfLines={1} style={styles.docSubNameText}>
              {item.cons_name}
            </Text>
            <TouchableOpacity
              onPress={() => {
                this.setState({
                  show: true,
                  modeltitle: item.election_name,
                  modeldesc: item.election_description,
                });
              }}>
              <Text style={styles.docSubNameText1} numberOfLines={3}>
                {item.election_description.substring(0, 100)}
              </Text>
            </TouchableOpacity>
            {/* <Text style={styles.docSubNameText1}>
                        {item.election_description.substring(0, 100)}
                      </Text> */}
          </View>
          {item.is_active == 1 && item.message != null ? (
            <TouchableOpacity
              activeOpacity={1}
              style={styles.boxShare1}
              onPress={() => {
                this.myCustomShare(item.message);
              }}>
              <Image style={{ width: 16.5, height: 18 }} source={Images.share} />
            </TouchableOpacity>
          ) : (
            <></>
          )}
        </View>
        <View style={styles.middleWrapper}>{this.renderButton(item)}</View>
      </View>
    );
  }
  render() {
    // console.log(this.state.profile)
    return (
      <View style={styles.container}>
        <View style={styles.upperBar}>
          <View style={styles.backIconContainer}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Settings')}>
              <Image source={Images.backIcon} style={styles.barMenuIcon} />
            </TouchableOpacity>
          </View>
          <View style={styles.headingContainer}>
            <Text style={styles.barText}>{translate('election')}</Text>
          </View>
          <View style={{ marginLeft: -10 }}>
            <TouchableOpacity onPress={() =>
              this.props.navigation.navigate('NotificationScreen')}>
              <Image source={Images.bell} style={styles.bellIcon1} />
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity onPress={() =>
              this.props.navigation.navigate('Profile')}>
              <Image source={{ uri: 'https://netahub.com/images/user/' + this.state.profile_pic }} style={styles.profileIcon} />
            </TouchableOpacity>
          </View>
          <View style={{ marginLeft: 8 }}>
            <TouchableOpacity onPress={() =>
              this.props.navigation.navigate('ReferAndEarn')}>
              <Image source={Images.refer} style={styles.bellIcon} />
            </TouchableOpacity>
          </View>
        </View>
        {/* <Header
          title="election"
          profile_pic={this.state.profile_pic}
          onPressBack={() => this.props.navigation.navigate('Settings')}
          refer={true}
          notification={true}
          onPressProfile={() => this.props.navigation.navigate('Profile')}
        /> */}
        <KeyboardAvoidingView behavior="padding">
          <LinearGradient
            colors={['#00cfcf', '#00cfef']}
            start={{ x: 0.3, y: 0.1 }}
            end={{ x: 0.3, y: 1.9 }}
            locations={[0.1, 0.6]}
            style={styles.wrapper}
          />
          <View
            style={{ alignItems: 'center', marginTop: '1%', marginBottom: '5%' }}>
            <FlatList
              showsVerticalScrollIndicator={false}
              contentContainerStyle={{ marginTop: '20%', paddingBottom: '55%' }}
              data={this.state.assembly}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh.bind(this)}
                />
              }
              renderItem={({ item }) => this.renderCard(item)}
              keyExtractor={item => item.id}
            />
          </View>
          {/*Modal Begin For Discription  */}
          <Modal
            transparent={true}
            animationType="slide"
            visible={this.state.show}>
            <View style={{ backgroundColor: '#000000aa', height: height * 1 }}>
              <TouchableOpacity
                style={styles.testModel}
                onPress={() => {
                  this.setState({ show: false });
                }}>
                <View style={styles.middleWrapper1}>
                  <View
                    style={{
                      width: '100%',
                      height: '20%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text style={styles.nameWrapper}>
                      {this.state.modeltitle}
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    margin: 20,
                  }}>
                  <Text style={{ textAlign: 'left', fontSize: 18 }}>
                    {this.state.modeldesc}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </Modal>
          {/* Modal END */}
        </KeyboardAvoidingView>
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={false}
          title={'NETAHUB'}
          titleStyle={{ fontSize: 14, color: '#000', fontWeight: 'bold' }}
          message="Do you want to update your profile"
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          showConfirmButton={true}
          confirmText="Okay"
          confirmButtonColor="#00cced"
          onConfirmPressed={() => {
            this.hideAlert();
          }}
          onCancelPressed={() => {
            this.cancelAlert();

          }}
        />
      </View>
    );
  }
}
const mapStateToProps = state => {
  // console.log(state)
  return {
    elections1: state.elections.elections,
    elections_error: state.elections.elections_error,
    contribute1: state.contribute.contribute,
    contribute_error: state.contribute.contribute_error,
    profile1: state.profile.profile,
    profile_error: state.profile.profile_error,
  };
};
export default connect(
  mapStateToProps,
  {
    elections,
    contribute,
    profile,
  },
)(Elections);
