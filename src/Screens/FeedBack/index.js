import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Dimensions,
  Image, Picker, FlatList, I18nManager
} from "react-native";
const { width, height } = Dimensions.get('window');
import InputField from '../../Commons/input';
import { Images } from '../../utils'
import styles from './style';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { feedback } from '../../redux/actions/feedback';
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
import AwesomeAlert from 'react-native-awesome-alerts';
import Header from "../../Commons/header";
const translationGetters = {
  // lazy requires (metro bundler does not support symlinks)
  en: () => require("../../assets/en-US.json"),
  hi: () => require("../../assets/hi.json")
};

const translate = memoize(
  (key, config) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key)
);

class FeedBack extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: '',
      feedback_desc: '',
      profile_pic: '',
      showAlert: false
    }
    this._getStorageValue()
  }

  async _getStorageValue() {
    var value = await AsyncStorage.getItem('token')
    var profile_pic = await AsyncStorage.getItem('profile_pic')
    this.state.profile_pic = profile_pic
    if (value == null) {
      console.log(value)
      this.props.navigation.replace('Login')
    }
    else {
      this.state.token = value

    }
    return value
  }
  onSubmit() {
    console.log('hi')
    console.log(this.state)
    this.props.feedback(this.state).then(async () => {
      if (this.props.feedback_error == "Invalid Token") {
        await AsyncStorage.setItem('token', "")
        this.props.navigation.replace('Login')
      }
      else
        this.props.navigation.replace('AppHome');

      // this.state.feedbacklist = this.props.feedback1
      // console.log(this.state.feedbacklist)
    })
    this.showAlert()

  }
  showAlert = () => {
    this.setState({
      showAlert: true
    });
  };

  hideAlert = () => {
    this.setState({
      showAlert: false
    });
    this.props.navigation.replace('AppHome')
  };
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.upperBar}>
          <View style={styles.backIconContainer}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Image source={Images.backIcon} style={styles.barMenuIcon} />
            </TouchableOpacity>
          </View>
          <View style={styles.headingContainer}>
            <Text style={styles.barText}>Feedback</Text>
          </View>
          <View>
            <TouchableOpacity onPress={() =>
              this.props.navigation.navigate('Profile')}>
              <Image source={{ uri: 'https://netahub.com/images/user/' + this.state.profile_pic }} style={styles.profileIcon} />
            </TouchableOpacity>
          </View>
        </View>
        {/* <Header title="feedback"
          // bellIcon={Images.bell}
          // refer={Images.refer}
          profile_pic={this.state.profile_pic}
          onPressBack={() => this.props.navigation.navigate('Settings')}
          onPressNotification={() => this.props.navigation.navigate('NotificationScreen')}
          onPressProfile={() => this.props.navigation.navigate('Profile')}
          onPressRefer={() => this.props.navigation.navigate('ReferAndEarn')} /> */}
        <KeyboardAvoidingView behavior="padding">
          <ScrollView
            contentContainerStyle={styles.scrollWrapper}
            showsVerticalScrollIndicator={false}>
            <View style={styles.wrapper}>
              <View style={styles.header}>
                <Image source={Images.netaHubLogo} style={{ width: width * 0.35, height: height * 0.19, borderRadius: 111 }} />
              </View>
            </View>
            <View style={styles.loginArea}>
              {/* <InputField
                name={'Feedback'}
                alertMessage={'Please Enter Your Feedback Here'}
                //   errorMessage={this.state.loginMsg}
                placeholder={'Write your Feedback'}
                onChangeText={feedback_desc => this.setState({ feedback_desc })}
                value={this.state.feedback_desc}
              /> */}
              <View style={{ marginLeft: 20, marginRight: 10, }}>
                <Text style={styles.textStyle}>Feedback</Text>
                <TextInput

                  placeholderTextColor="#000"
                  autoCapitalize='none'
                  multiline={true}
                  textAlignVertical='top'
                  style={styles.input1}
                  placeholder="Write your Feedback"
                />
              </View>
              <View style={{ justifyContent: 'center', alignItems: 'center', position: 'absolute', left: 57, top: '85%' }}>
                <LinearGradient
                  colors={['#00cfcf', '#00cfef']}
                  start={{ x: 0.3, y: 0.1 }}
                  end={{ x: 0.3, y: 1.9 }}
                  locations={[0.1, 0.6]}
                  style={{ borderRadius: 20 }}>
                  <TouchableOpacity
                    activeOpacity={1}
                    style={styles.boxBtn2}
                    onPress={() =>
                      this.onSubmit()
                    }>
                    <Text
                      style={styles.btnText}>
                      Submit
                     </Text>
                  </TouchableOpacity>
                </LinearGradient>
              </View>
              <AwesomeAlert

                show={this.state.showAlert}
                showProgress={false}
                title={"NETAHUB"}
                titleStyle={{ fontSize: 14, color: '#000', fontWeight: "bold" }}
                message="Feedback Saved Succesfully"
                closeOnTouchOutside={true}
                closeOnHardwareBackPress={false}
                //showCancelButton={true}
                showConfirmButton={true}
                confirmText="Okay"
                confirmButtonColor="#00cced"
                onConfirmPressed={() => {
                  this.hideAlert();
                }}
              />
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  // console.log(state)
  return {
    feedback1: state.feedback.feedback,
    feedback_error: state.feedback.feedback_error
  };
};
export default connect(mapStateToProps, {
  feedback
})(FeedBack);
