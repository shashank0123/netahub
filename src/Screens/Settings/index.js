import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image, Picker, FlatList, I18nManager
} from "react-native";
import LinearGradient from 'react-native-linear-gradient';
const { width, height } = Dimensions.get('window');
import InputField from '../../Commons/input';
import { Images } from '../../utils'
import styles from './style';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { setting } from '../../redux/actions/setting';
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
import Header from "../../Commons/header";
const translationGetters = {
    // lazy requires (metro bundler does not support symlinks)
    en: () => require("../../assets/en-US.json"),
    hi: () => require("../../assets/hi.json")
};
const translate = memoize(
    (key, config) => i18n.t(key, config),
    (key, config) => (config ? key + JSON.stringify(config) : key)
);
const assembly = [
    {
        id: '1',
        name: 'My Profile',
        image: Images.prof,
        screenName: 'Profile'
    },
    {
        id: '2',
        name: 'My Constituency',
        image: Images.constituency,
        screenName: 'MyConstituency'
    },
    {
        id: '3',
        name: 'Feedback',
        image: Images.feedback,
        screenName: 'FeedBack'
    },
    {
        id: '4',
        name: 'FAQs',
        image: Images.faq,
        screenName: 'FAQs'
    },
];
class Settings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            profile_pic: ''
        };
        this._getStorageValue();
    }
    async _getStorageValue() {
        var value = await AsyncStorage.getItem('token')
        var username = await AsyncStorage.getItem('username')
        var profile_pic = await AsyncStorage.getItem('profile_pic')
        this.state.profile_pic = profile_pic
        if (value == null) {
            console.log(value)
            this.props.navigation.replace('Login')
        }
        else
            return value
    }
    async logout() {
        await AsyncStorage.setItem('token', "")
        this.props.navigation.replace('Login')
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.upperBar}>
                    <View style={styles.backIconContainer}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image source={Images.backIcon} style={styles.barMenuIcon} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.headingContainer}>
                        <Text style={styles.barText}>More</Text>
                    </View>
                    <View style={{ marginLeft: -10 }}>
                        <TouchableOpacity onPress={() =>
                            this.props.navigation.navigate('NotificationScreen')}>
                            <Image source={Images.bell} style={styles.bellIcon1} />
                        </TouchableOpacity>
                    </View>
                    <View >
                        <TouchableOpacity onPress={() =>
                            this.props.navigation.navigate('Profile')}>
                            <Image source={{ uri: 'https://netahub.com/images/user/' + this.state.profile_pic }} style={styles.profileIcon} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ marginLeft: 8 }}>
            <TouchableOpacity onPress={() =>
              this.props.navigation.navigate('ReferAndEarn')}>
              <Image source={Images.refer} style={styles.bellIcon} />
            </TouchableOpacity>
          </View>
                </View>
               
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        data={assembly}
                        numColumns={2}
                        renderItem={({ item }) => (
                            <>
                                <TouchableOpacity onPress={() =>
                                    this.props.navigation.navigate(item.screenName)}>
                                    <LinearGradient
                                        colors={['#79efef', '#cfe7ea']}
                                        start={{ x: 0.1, y: 0.1 }}
                                        end={{ x: 0.1, y: 0.8 }}
                                        locations={[0.2, 0.8]}
                                        style={styles.docDetailedWrapper1}>
                                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                            <View style={{ marginTop: 10, justifyContent: 'center', alignItems: 'center', width: "100%" }} >
                                                <Image source={item.image} style={{ width: 50, height: 50 }} />
                                                <Text style={{ fontWeight: '900', fontSize: 16, marginTop: 15 }}>{item.name}</Text>
                                            </View>
                                        </View>
                                    </LinearGradient>
                                </TouchableOpacity>
                            </>
                        )}
                        keyExtractor={item => item.id}
                    />
                </View>
                <TouchableOpacity
                    activeOpacity={1}
                    style={{ justifyContent: 'center', alignItems: 'center', marginTop: 10 }}
                    onPress={() =>
                        this.logout()
                    }>
                    <LinearGradient
                        colors={['#79efef', '#cfe7ea']}
                        start={{ x: 0.1, y: 0.1 }}
                        end={{ x: 0.1, y: 0.8 }}
                        locations={[0.2, 0.8]}
                        style={{
                            width: width * 0.43,
                            height: height * 0.15, borderRadius: 10
                        }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            {/* <View style={styles.docDetailedWrapper1}> */}
                            <View style={{ marginTop: 10, justifyContent: 'center', alignItems: 'center', width: "100%" }} >
                                <Image source={Images.logout} style={{ width: 50, height: 50 }} />
                                <Text style={{ fontWeight: '900', fontSize: 16, marginTop: 15 }}>Logout</Text>
                            </View>
                        </View>
                        {/* </View> */}
                    </LinearGradient>
                </TouchableOpacity>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    // console.log(state)
    return {
        setting1: state.setting.setting,
        setting_error: state.setting.setting_error
    };
};
export default connect(mapStateToProps, {
    setting
})(Settings);
