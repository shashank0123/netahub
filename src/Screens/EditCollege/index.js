import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Dimensions,
    Image, FlatList, I18nManager, ActivityIndicator
} from "react-native";
const { width, height } = Dimensions.get('window');
import InputField from '../../Commons/input';
import { Images } from '../../utils'
import styles from './style';
// import { Picker } from '@react-native-community/picker';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { editCollege } from '../../redux/actions/editCollege';
import { collegelist } from '../../redux/actions/collegelist';
import { usercollege } from '../../redux/actions/usercollege';
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
import AwesomeAlert from 'react-native-awesome-alerts';
import { Picker } from 'react-native-woodpicker';
const LoadingIcon = ({ loadingApp }) => <ActivityIndicator
    style={{ zIndex: 5, position: 'absolute', alignItems: 'center', top: '63%', left: '45%', }} size="large" color="#00b8e6" animating={loadingApp} ></ActivityIndicator>;
const translationGetters = {
    // lazy requires (metro bundler does not support symlinks)
    en: () => require("../../assets/en-US.json"),
    hi: () => require("../../assets/hi.json")
};
const translate = memoize(
    (key, config) => i18n.t(key, config),
    (key, config) => (config ? key + JSON.stringify(config) : key)
);
class EditCollege extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedValue: '',
            selectedValue1: '',
            showAlert: false,
            collegelist: [],
            college_id: 0,
            user_college: '',
            buttonText: 'Next',
            loadingApp: false,
            pickedCollege: '',
        }
        this._getStorageValue()
    }
    data = [
        { label: "Select College ", value: 1 },
    ];
    handlePicker = data => {
        this.setState({ pickedCollege: data });
        if (this.props.usercollege1[0].college_id != data.college_id) {
            this.setState({ buttonText: 'Continue' })
        }
        else
            this.setState({ buttonText: 'Next' })
        this.setState({ college_id: data.value })
    };
    async _getStorageValue() {
        var value = await AsyncStorage.getItem('token')
        var profile_pic = await AsyncStorage.getItem('profile_pic')
        this.state.profile_pic = profile_pic
        if (value == null) {
            console.log(value)
            this.props.navigation.replace('Login')
        }
        else {
            this.setState({ token: value })
            this.props.collegelist(this.state).then(async () => {
                if (this.props.profile_error == "Invalid Token") {
                    await AsyncStorage.setItem('token', "")
                    this.props.navigation.replace('Login')
                }
                var collegelist = []
                collegelist = this.props.collegelist1.map((obj) => {
                    var obj1 = {
                        label: obj.college_name,
                        value: obj.college_id
                    }
                    return obj1
                    // console.log(obj1)
                })
                this.data = collegelist
                this.setState({ collegelist: this.props.collegelist1 })
            })
            this.props.usercollege(value).then(async () => {
                if (this.props.profile_error == "Invalid Token") {
                    await AsyncStorage.setItem('token', "")
                    this.props.navigation.replace('Login')
                }
                this.setState({
                    usercollege: this.props.usercollege1[0].college_id, selectedValue1: this.props.usercollege1[0].college_id, college_id: this.props.usercollege1.college_id,
                    pickedCollege:
                    {
                        "label": this.props.usercollege1[0].college_name,
                        "value": this.props.usercollege1[0].college_id
                    }
                })
            })
            this.setState({ token: value })
        }
        return value
    }

    submitCollege() {
        this.setState({ loadingApp: true })
        if (this.state.buttonText == 'Next') {

            this.props.navigation.replace('Profile');
        }
        else
            this.props.editCollege(this.state).then(async () => {
                if (this.props.editCollege_error == "Invalid Token") {
                    await AsyncStorage.setItem('token', "")
                    this.props.navigation.replace('Login')
                }
                else
                    this.props.navigation.replace('Profile');
            })
    }
    loadingRender() {
        if (this.state.loadingApp) {
            return (
                <LoadingIcon />
            )
        }
    }
    // submitCollege() {
    //     this.showAlert()
    // }
    showAlert = () => {
        this.setState({
            showAlert: true
        });
    };
    hideAlert = () => {
        this.setState({
            showAlert: false
        });
        this.props.navigation.replace('Profile')
    };
    renderForm() {
        if (this.state.selectedValue1 == '' || this.state.selectedValue1 == 0)
            return (<>
                <View style={{ marginTop: 15 }}>
                    <InputField
                        name={'College Name'}
                        alertMessage={'Please Enter Your College Name'}
                        //   errorMessage={this.state.loginMsg}
                        placeholder={'Enter College'}
                        onChangeText={
                            college_name => {
                                this.setState({ college_name })
                            }}
                        value={this.state.college_name}
                    />
                </View>
            </>)
        else {
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={{ zIndex: 5, }}>
                    {this.loadingRender()}
                    <View style={styles.upperBar}>
                        <View style={styles.backIconContainer}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                                <Image source={Images.backIcon} style={styles.barMenuIcon} />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.headingContainer}>
                            <Text style={styles.barText}>College</Text>
                        </View>
                        <View>
                            <TouchableOpacity onPress={() =>
                                this.props.navigation.navigate('Profile')}>
                                <Image source={{ uri: 'https://netahub.com/images/user/' + this.state.profile_pic }} style={styles.profileIcon} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <KeyboardAvoidingView behavior="padding">
                        <ScrollView
                            contentContainerStyle={styles.scrollWrapper}
                            showsVerticalScrollIndicator={false}>
                            <View style={styles.wrapper}>
                                <View style={styles.header}>
                                    <Image source={Images.netaHubLogo} style={{ width: width * 0.35, height: height * 0.19, borderRadius: 111 }} />
                                </View>
                            </View>
                            <View style={styles.loginArea}>
                                {/* <View style={styles.formShortBox2}>
                                   
                                    <Picker
                                        selectedValue={this.state.selectedValue1}
                                        style={styles.picker1}
                                        onValueChange={(itemValue, itemIndex) => this.changeCollege(itemValue)}>
                                        <Picker.Item label="Add New" value="0" />
                                        {this.state.collegelist.map(nom => <Picker.Item key={nom.college_id} label={nom.college_name} value={nom.college_id} />)}
                                    </Picker>
                                </View> */}
                                <View style={styles.pickerBox}>
                                    <Text style={{ fontSize: 16, marginLeft: 5 }}>College :</Text>
                                    {this.data.length > 0 ?
                                        <Picker
                                            onItemChange={this.handlePicker}
                                            style={styles.picker}
                                            items={this.data}
                                            placeholder="Select College"
                                            placeholderStyle={{ color: '#000' }}
                                            item={this.state.pickedCollege}
                                        // backdropAnimation={{ opactity: 0 }}
                                        //androidPickerMode="dropdown"
                                        //isNullable
                                        //disable
                                        /> : <></>}
                                    <Image source={Images.dropDown} style={styles.dropdown} />
                                </View>
                                {this.renderForm()}
                                {/* <TouchableOpacity
                                activeOpacity={1}
                                style={{ flex: 1, borderRadius: 20, width: '60%', justifyContent: 'center', alignItems: 'center', marginTop: 30, marginBottom: -70 }}
                                onPress={() =>
                                    this.submitCollege()
                                }>
                                <LinearGradient
                                    colors={['#00cfcf', '#00cfef']}
                                    start={{ x: 0.3, y: 0.1 }}
                                    end={{ x: 0.3, y: 1.9 }}
                                    locations={[0.1, 0.6]}
                                    style={styles.boxBtn2}>
                                    <Text
                                        style={styles.btnText}>
                                        Submit
                      </Text>
                                </LinearGradient>
                            </TouchableOpacity> */}
                                <View style={{ flexDirection: 'row', marginTop: '5%' }}>
                                    <TouchableOpacity
                                        activeOpacity={1}
                                        style={{ flex: 1, borderRadius: 20, width: '60%', justifyContent: 'center', alignItems: 'center', marginBottom: -45, marginLeft: 15 }}
                                        onPress={() => this.props.navigation.goBack()}>
                                        <LinearGradient
                                            colors={['#00cfcf', '#00cfef']}
                                            start={{ x: 0.3, y: 0.1 }}
                                            end={{ x: 0.3, y: 1.9 }}
                                            locations={[0.1, 0.6]}
                                            style={styles.boxBtn2}>
                                            <Text
                                                style={styles.btnText}>
                                                Skip </Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        activeOpacity={1}
                                        style={{ flex: 1, borderRadius: 20, width: '60%', justifyContent: 'center', alignItems: 'center', marginBottom: -45, marginRight: 15 }}
                                        onPress={() =>
                                            this.submitCollege()
                                        }>
                                        <LinearGradient
                                            colors={['#00cfcf', '#00cfef']}
                                            start={{ x: 0.3, y: 0.1 }}
                                            end={{ x: 0.3, y: 1.9 }}
                                            locations={[0.1, 0.6]}
                                            style={styles.boxBtn2}>
                                            <Text
                                                style={styles.btnText}>
                                                {this.state.buttonText}
                                            </Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                </View>
                                <AwesomeAlert
                                    show={this.state.showAlert}
                                    showProgress={false}
                                    title={"NETAHUB"}
                                    titleStyle={{ fontSize: 14, color: '#000', fontWeight: "bold" }}
                                    message="College Changed Succesfully"
                                    closeOnTouchOutside={true}
                                    closeOnHardwareBackPress={false}
                                    //showCancelButton={true}
                                    showConfirmButton={true}
                                    confirmText="Okay"
                                    confirmButtonColor="#00cced"
                                    onConfirmPressed={() => {
                                        this.hideAlert();
                                    }}
                                />
                            </View>
                        </ScrollView>
                    </KeyboardAvoidingView>
                </View>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    // console.log(state.collegelist)
    return {
        editCollege1: state.editCollege.editCollege,
        editCollege_error: state.editCollege.editCollege_error,
        usercollege1: state.usercollege.usercollege,
        usercollege_error: state.usercollege.usercollege_error,
        collegelist1: state.collegelist.collegelist,
        collegelist_error: state.collegelist.collegelist_error
    };
};
export default connect(mapStateToProps, {
    editCollege, collegelist, usercollege
})(EditCollege);
