import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Dimensions,
  Image, Picker, FlatList,I18nManager
} from "react-native";
const { width, height } = Dimensions.get('window');
import LinearGradient from 'react-native-linear-gradient';
import InputField from '../../Commons/input';
import { Images, widthToDp, heightToDp } from '../../utils'
import styles from './style';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { report } from '../../redux/actions/report';
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
import Header from "../../Commons/header";
const translationGetters = {
  // lazy requires (metro bundler does not support symlinks)
  en: () => require("../../assets/en-US.json"),
  hi: () => require("../../assets/hi.json")
};

const translate = memoize(
  (key, config) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key)
);

const assembly = [
  {
    id: '1',
    name: 'Age Analysis',
    image: Images.age,
    screenName: 'AgeAnalysis',
    passvalue: 'age'
  },
  {
    id: '2',
    name: 'Profession Analysis',
    image: Images.home,
    screenName: 'ProfessionAnalysis',
    passvalue: 'Professional'
  },
  {
    id: '3',
    name: 'Tehsil',
    image: Images.tehsil,
    screenName: 'TehsilAnalysis',
    passvalue: 'tehsil'
  },
  {
    id: '4',
    name: 'Village',
    image: Images.village,
    screenName: 'TehsilAnalysis',
    passvalue: 'village'
  },
  {
    id: '5',
    name: 'Ward',
    image: Images.ward,
    screenName: 'TehsilAnalysis',
    passvalue: 'ward'
  },
  {
    id: '6',
    name: 'Block',
    image: Images.block,
    screenName: 'TehsilAnalysis',
    passvalue: 'block'
  },
  {
    id: '7',
    name: 'Nagar Panchayat',
    image: Images.nagar,
    screenName: 'TehsilAnalysis',
    passvalue: 'nagarpanchayat'
  },
  {
    id: '8',
    name: 'Gender Analysis',
    image: Images.gender,
    screenName: 'GenderAnalysis',
    passvalue: 'gender'
  },
  {
    id: '9',
    name: 'District',
    image: Images.district,
    screenName: 'TehsilAnalysis',
    passvalue: 'district'
  },
  {
    id: '10',
    name: 'State',
    image: Images.state,
    screenName: 'TehsilAnalysis',
    passvalue: 'state'
  },
  {
    id: '11',
    name: 'Country',
    image: Images.country,
    screenName: 'TehsilAnalysis',
    passvalue: 'country'
  },
];
class Reports extends Component {
  constructor(props) {
        super(props);
        this.state = {
            profile_pic:''
        };
        this._getStorageValue();
    }
    async _getStorageValue() {
        var value = await AsyncStorage.getItem('token')
        var username = await AsyncStorage.getItem('username')
        var profile_pic = await AsyncStorage.getItem('profile_pic')
        this.state.profile_pic = profile_pic
        if (value == null) {
            console.log(value)
            this.props.navigation.replace('Login')
        }
        else
            
        return value
    }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.upperBar}>
          <View style={styles.backIconContainer}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Image source={Images.backIcon} style={styles.barMenuIcon} />
            </TouchableOpacity>
          </View>
          <View style={styles.headingContainer}>
            <Text style={styles.barText}>Reports</Text>
          </View>
          <View style={{ marginLeft: -10 }}>
            <TouchableOpacity onPress={() =>
              this.props.navigation.navigate('NotificationScreen')}>
              <Image source={Images.bell} style={styles.bellIcon1} />
            </TouchableOpacity>
          </View>
          <View >
            <TouchableOpacity onPress={() =>
              this.props.navigation.navigate('Profile')}>
              <Image source={{ uri: 'https://netahub.com/images/user/' + this.state.profile_pic }} style={styles.profileIcon} />
            </TouchableOpacity>
          </View>
          <View style={{ marginLeft: 8 }}>
            <TouchableOpacity onPress={() =>
              this.props.navigation.navigate('ReferAndEarn')}>
              <Image source={Images.refer} style={styles.bellIcon} />
            </TouchableOpacity>
          </View>
        </View>
        {/* <Header title="reports"
                    profile_pic={this.state.profile_pic}
                    onPressBack={() => this.props.navigation.goBack()}
                    onPressNotification={() => this.props.navigation.navigate('NotificationScreen')}
                    onPressProfile={() => this.props.navigation.navigate('Profile')}
                    onPressRefer={() => this.props.navigation.navigate('ReferAndEarn')} /> */}
        <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: height * 0.15 }}>
          <FlatList
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{alignItems: 'center'}}
            data={assembly}
            numColumns={2}
            renderItem={({ item }) => (
              <>
                <TouchableOpacity onPress={() =>
                  this.props.navigation.navigate(item.screenName, { location_type: item.passvalue })}>
                  <LinearGradient
                    colors={['#79efef', '#cfe7ea']}
                    start={{ x: 0.1, y: 0.1 }}
                    end={{ x: 0.1, y: 0.8 }}
                    locations={[0.2, 0.8]}
                    style={styles.docDetailedWrapper1}>
                    <View style={{ marginTop: 10, justifyContent: 'center', alignItems: 'center', width: "100%" }} >
                      <Image source={item.image} style={{ width: 50, height: 50 }} />
                      <Text style={{ fontWeight: '900', fontSize: 16, marginTop: 15 }}>{item.name}</Text>
                    </View>
                  </LinearGradient>
                </TouchableOpacity>
              </>
            )}
            keyExtractor={item => item.id}
          />
        </View>
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  // console.log(state)
  return {
    report1: state.report.report,
    report_error: state.report.report_error
  };
};
export default connect(mapStateToProps, {
  report
})(Reports);
