import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Dimensions,
  Image, FlatList, I18nManager
} from "react-native";
const { width, height } = Dimensions.get('window');
import InputField from '../../Commons/input';
import { Images } from '../../utils'
import styles from './style';
// import { Picker } from '@react-native-community/picker';
import { Picker } from 'react-native-woodpicker';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { tehsilAnalysis } from '../../redux/actions/tehsilAnalysis';
import { reportSurvey } from '../../redux/actions/reportSurvey';
import { reportSurveyNominee } from '../../redux/actions/reportSurveyNominee';
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
import Header from "../../Commons/header";
const translationGetters = {
  // lazy requires (metro bundler does not support symlinks)
  en: () => require("../../assets/en-US.json"),
  hi: () => require("../../assets/hi.json")
};

const translate = memoize(
  (key, config) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key)
);

class TehsilAnalysis extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedValue: '',
      selectedValue1: '',
      surveyList: [],
      survey_id: '',
      NomineeList: [],
      tehsilAnalysis: [],
      location_type: this.props.route.params.location_type,
      profile_pic: '',
      pickedSurvey: '',
      pickedNominee: ''
    }
    this._getStorageValue();
  }
  data = [
    { label: "Select Survey ", value: 0 },

  ];
  data1 = [
    { label: "Select Nominee ", value: 0 },

  ];
  handlePicker = data => {
    console.log(typeof data, 'yahi aaya hai ')
    this.setState({ pickedSurvey: data });
    console.log(data)
    if (typeof data != 'undefined')
      this.setState({ survey_id: data.value })
    else
      this.setState({ pickedSurvey: { 'label': 0, 'value': 'select' } })
    this.getNominee(data)
  };
  handlePicker1 = data => {
    this.setState({ pickedNominee: data });
    if (typeof data != 'undefined')
      this.setState({ nominee_id: data.value });
    else
      this.setState({ pickedNominee: { 'label': 0, 'value': 'select' } })
    if (typeof data != 'undefined')
      this.state.nominee_id = data.value
    this.gettehsilAnalysis()
  };
  async _getStorageValue() {
    var value = await AsyncStorage.getItem('token')
    var profile_pic = await AsyncStorage.getItem('profile_pic')
    this.state.profile_pic = profile_pic
    if (value == null) {
      console.log(value)
      this.props.navigation.replace('Login')
    }
    else {
      this.setState({ token: value });
      this.props.reportSurvey(this.state).then(async () => {
        console.log(this.props.reportSurvey1, 'jai kanahai laal ki')
        if (this.props.reportSurvey_error == "Invalid Token") {
          await AsyncStorage.setItem('token', "")
          this.props.navigation.replace('Login')
        }
        var surveylist = []
        surveylist = this.props.reportSurvey1.map((obj) => {
          var obj1 = {
            label: obj.survey_name,
            value: obj.survey_id
          }
          return obj1
          // console.log(obj1)
        })
        // console.log(surveylist)
        this.data = surveylist
        this.setState({ surveyList: this.props.reportSurvey1 })
      })
    }
    return value
  }
  getNominee(data) {
    if (typeof data != 'undefined')
      this.state.survey_id = data.value
    this.props.reportSurveyNominee(this.state).then(async () => {
      if (this.props.reportSurveyNominee_error == "Invalid Token") {
        await AsyncStorage.setItem('token', "")
        this.props.navigation.replace('Login')
      }
      var NomineeList = []
      if (this.props.reportSurveyNominee1[0]) {
        NomineeList = this.props.reportSurveyNominee1.map((obj) => {
          var obj1 = {
            label: obj.nominee_name,
            value: obj.nominee_id
          }
          return obj1
          // console.log(obj1)
        })
        // console.log(surveylist)
        this.data1 = NomineeList
      }
      this.setState({ NomineeList: this.props.reportSurveyNominee1 })
    })
  }
  gettehsilAnalysis() {
    this.props.tehsilAnalysis(this.state).then(async () => {
      if (this.props.tehsilAnalysis_error == "Invalid Token") {
        await AsyncStorage.setItem('token', "")
        this.props.navigation.replace('Login')
      }
      this.setState({ tehsilAnalysis: this.props.tehsilAnalysis1 })
      console.log(this.state.tehsilAnalysis)
    })
  }
  renderCard(item) {
    return (
      <View style={styles.docDetailedWrapper2}>
        <View style={styles.middleWrapper}>
          <View style={{ width: '100%', height: '28%', justifyContent: 'center', alignItems: 'center' }}>
            <Text style={styles.nameWrapper} >{item.location_type}: {item.location_name}</Text>
          </View>
        </View>
        <View style={{
          flexDirection: 'row', borderBottomColor: '#808080',
          marginTop: 20, width: '90%', justifyContent: 'space-between'
        }}>
          <Text style={{ fontSize: 16, color: '#5e6063', marginTop: 5, }}>Group Vote:</Text>
          <Text style={{ fontSize: 16, color: '#5e6063', marginTop: 5, }}>{item.occ_vote}</Text>
        </View>
        <View style={{
          flexDirection: 'row', borderBottomColor: '#808080',
          marginTop: 10, width: '90%', justifyContent: 'space-between'
        }}>
          <Text style={{ fontSize: 16, color: '#5e6063', marginTop: 5, }}>Nominee Vote:</Text>
          <Text style={{ fontSize: 16, color: '#5e6063', marginTop: 5, }}>{item.nominee_vote}</Text>
        </View>
        <View style={{
          flexDirection: 'row', borderBottomColor: '#808080',
          marginTop: 10, width: '90%', justifyContent: 'space-between'
        }}>
          <Text style={{ fontSize: 16, color: '#5e6063', marginTop: 5, }}>Nominee Vote %:</Text>
          <Text style={{ fontSize: 16, color: '#5e6063', marginTop: 5, }}>{item.nominee_vote_per}%</Text>
        </View>
        <View style={{
          flexDirection: 'row', borderBottomColor: '#808080',
          marginTop: 10, width: '90%', justifyContent: 'space-between'
        }}>
          <Text style={{ fontSize: 16, color: '#5e6063', marginTop: 5, }}>Survey Vote:</Text>
          <Text style={{ fontSize: 16, color: '#5e6063', marginTop: 5, }}>{item.survey_vote}</Text>
        </View>
        <View style={{
          flexDirection: 'row', borderBottomColor: '#808080',
          marginTop: 10, width: '90%', justifyContent: 'space-between'
        }}>
          <Text style={{ fontSize: 16, color: '#5e6063', marginTop: 5, }}>Survey Vote %:</Text>
          <Text style={{ fontSize: 16, color: '#5e6063', marginTop: 5, }}>{item.survey_vote_per}%</Text>
        </View>
        <View style={{
          flexDirection: 'row', borderBottomColor: '#808080',
          marginTop: 10, width: '90%', justifyContent: 'space-between'
        }}>
          <Text style={{ fontSize: 16, color: '#5e6063', marginTop: 5, }}>Consistuency Name:</Text>
          <Text style={{ fontSize: 16, color: '#5e6063', marginTop: 5, }}>{item.cons_name}</Text>
        </View>
      </View>
    )
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.upperBar}>
          <View style={styles.backIconContainer}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Image source={Images.backIcon} style={styles.barMenuIcon} />
            </TouchableOpacity>
          </View>
          <View style={styles.headingContainer}>
            <Text numberOfLines={1} style={styles.barText}>{this.state.location_type} Analysis</Text>
          </View>
          <View>
            <TouchableOpacity onPress={() =>
              this.props.navigation.navigate('Profile')}>
              <Image source={{ uri: 'https://netahub.com/images/user/' + this.state.profile_pic }} style={styles.profileIcon} />
            </TouchableOpacity>
          </View>
        </View>
        {/* <Header title={[this.state.location_type+ 'analysis']}
                  
                  profile_pic={this.state.profile_pic}
                  onPressBack={() => this.props.navigation.goBack()}
                  // onPressNotification={() => this.props.navigation.navigate('NotificationScreen')}
                  onPressProfile={() => this.props.navigation.navigate('Profile')}
                  // onPressRefer={() => this.props.navigation.navigate('ReferAndEarn')}
                   /> */}
        <KeyboardAvoidingView behavior="padding">
          <View style={styles.wrapper}></View>
          <View style={{ alignItems: 'center', marginTop: '15%' }}>
            {/* <View style={styles.formShortBox}>
              <Text style={{ fontSize: 16 }}>Survey</Text>
              <Picker
                selectedValue={this.state.selectedValue}
                style={styles.picker}
                onValueChange={(itemValue, itemIndex) => { this.setState({ selectedValue: itemValue }); this.state.survey_id = itemValue; this.getNominee() }}>
                <Picker.Item label="Select Survey" value="Select Survey" />
                {this.state.surveyList.map(gen => <Picker.Item key={gen.survey_id} label={gen.survey_name} value={gen.survey_id} />)}
              </Picker>
            </View> */}
            {/* <View style={styles.formShortBox2}>
              <Text style={{ fontSize: 16 }}>Nominee</Text>
              <Picker
                selectedValue={this.state.selectedValue1}
                style={styles.picker1}
                onValueChange={(itemValue, itemIndex) => { this.setState({ selectedValue1: itemValue }); this.state.nominee_id = itemValue; this.gettehsilAnalysis() }}>
                <Picker.Item label="Select Nominee" value="Select Gender" />
                {this.state.NomineeList.map(nom => <Picker.Item key={nom.nominee_id} label={nom.nominee_name} value={nom.nominee_id} />)}
              </Picker>
            </View> */}






            <View style={styles.pickerBox}>
              <Text style={{ fontSize: 16, marginLeft: 5 }}>Survey :</Text>
              <Picker
                onItemChange={this.handlePicker}
                style={styles.picker}
                items={this.data}
                placeholder="Select Survey"

                placeholderStyle={{ color: '#000' }}
                item={this.state.pickedSurvey}
              // backdropAnimation={{ opactity: 0 }}
              //androidPickerMode="dropdown"
              //isNullable
              //disable
              />
              <Image source={Images.dropDown} style={styles.dropdown} />
            </View>
            <View style={styles.pickerBox}>
              <Text style={{ fontSize: 16, marginLeft: 5 }}>Nominee :</Text>
              <Picker
                onItemChange={this.handlePicker1}
                style={styles.picker1}
                items={this.data1}
                placeholder="Select Nominee"

                placeholderStyle={{ color: '#000' }}
                item={this.state.pickedNominee}
              // backdropAnimation={{ opactity: 0 }}
              //androidPickerMode="dropdown"
              //isNullable
              //disable
              />
              <Image source={Images.dropDown} style={styles.dropdown} />
            </View>

          </View>
          <View style={{ marginTop: '5%', alignItems: 'center', height: height, marginTop: -110 }}>
            {(this.state.tehsilAnalysis.length > 0 && typeof this.state.tehsilAnalysis[0].nominee_vote != 'undefined') ?
              <FlatList
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ marginTop: '10%', paddingBottom: '100%' }}
                data={this.state.tehsilAnalysis}
                renderItem={({ item }) => (
                  this.renderCard(item)
                )}
                keyExtractor={item => item.id}
              />
              :
              <></>
            }
          </View>
        </KeyboardAvoidingView>
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  // console.log(state)
  return {
    tehsilAnalysis1: state.tehsilAnalysis.tehsilAnalysis,
    tehsilAnalysis_error: state.tehsilAnalysis.tehsilAnalysis_error,
    reportSurvey1: state.reportSurvey.reportSurvey,
    reportSurvey_error: state.reportSurvey.reportSurvey_error,
    reportSurveyNominee1: state.reportSurveyNominee.reportSurveyNominee,
    reportSurveyNominee_error: state.reportSurveyNominee.reportSurveyNominee_error
  };
};
export default connect(mapStateToProps, {
  tehsilAnalysis, reportSurvey, reportSurveyNominee
})(TehsilAnalysis);
