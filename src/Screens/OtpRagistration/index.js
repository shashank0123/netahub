import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image, Picker, I18nManager
} from "react-native";
const { width, height } = Dimensions.get('window');
import InputField from '../../Commons/input';
import { Input } from 'react-native-elements';
import { Images } from '../../utils'
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './style';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { otpScreen } from '../../redux/actions/otpScreen';
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
const translationGetters = {
    // lazy requires (metro bundler does not support symlinks)
    en: () => require("../../assets/en-US.json"),
    hi: () => require("../../assets/hi.json")
};
const translate = memoize(
    (key, config) => i18n.t(key, config),
    (key, config) => (config ? key + JSON.stringify(config) : key)
);
class OtpRagistration extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            option: 0,
            otp: '',
            new_password: '',
            conf_password: ''
        };
    }
    onSubmit() {
        console.log('hi')
        console.log(this.state)
    }
    render() {
        return (
            <View style={styles.container}>
                <KeyboardAvoidingView behavior="padding">
                    <ScrollView
                        contentContainerStyle={styles.scrollWrapper}
                        showsVerticalScrollIndicator={false}>
                        <LinearGradient
                            colors={['#00cccc', '#00cced']}
                            start={{ x: 0.3, y: 0.1 }}
                            end={{ x: 0.3, y: 1.9 }}
                            locations={[0.1, 0.6]}
                            style={styles.wrapper}>
                            {/* <View style={styles.wrapper}> */}
                            <View style={styles.header}>
                                <Image source={Images.netaHubLogo} style={{ width: width * 0.35, height: height * 0.19, borderRadius: 111 }} />
                            </View>
                            {/* </View> */}
                        </LinearGradient>
                        <View style={styles.loginArea}>
                            <InputField
                                name={'OTP'}
                                alertMessage={'Please Enter OTP you Recieved'}
                                errorMessage={this.state.loginMsg}
                                placeholder={' Enter OTP'}
                                onChangeText={otp => this.setState({ otp })}
                                value={this.state.otp}
                            />
                            <View style={{ marginBottom: -70, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                                <LinearGradient
                                    colors={['#00cfcf', '#00cfef']}
                                    start={{ x: 0.3, y: 0.1 }}
                                    end={{ x: 0.3, y: 1.9 }}
                                    locations={[0.1, 0.6]}
                                    style={{ borderRadius: 20 }}>
                                    <TouchableOpacity
                                        activeOpacity={1}
                                        style={styles.boxBtn2}
                                        onPress={() =>
                                            this.onSubmit()
                                        }>
                                        <Text
                                            style={styles.btnText}>
                                            SignUp
                      </Text>
                                    </TouchableOpacity>
                                </LinearGradient>
                            </View>
                        </View>
                        <View style={{ marginTop: 50 }}>
                            <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}
                                onPress={() =>
                                    this.props.navigation.navigate('Ragistration')
                                }>
                                <Text>
                                    <Text> Dont Have an Account?</Text>
                                    <Text style={{ fontWeight: 'bold' }}> Create Now</Text>
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}
                                onPress={() =>
                                    this.props.navigation.navigate('ForgetPassword')
                                }>
                                <Text style={styles.forgetText}>
                                    <Text > Login</Text>
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
            </View>
        );
    }
}
export default OtpRagistration;
