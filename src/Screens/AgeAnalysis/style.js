import { StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
export default StyleSheet.create({
    container: {
        width,
        height,
        display: 'flex',
    },
    upperBar: {
        width,
        height: height * 0.075,
        backgroundColor: '#00b8e6',
        flexDirection: 'row',
        alignItems: 'center',
    },
    backIconContainer: {
    },
    headingContainer: {
        width: '60%',
        marginLeft: '10%',
        alignItems: 'center',
    },
    barText: {
        color: "#000000",
        fontSize: height * 0.03,
        fontWeight: 'bold'
    },
    barMenuIcon: {
        height: height * 0.04,
        width: height * 0.04,
        marginLeft: width * 0.04,
    },
    scrollWrapper: {
        width,
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingBottom: 35
        // backgroundColor: 'pink',
    },
    wrapper: {
        width: width * 1,
        height: height * 0.5,
        backgroundColor: '#00cccc',
        alignItems: 'center',
        position: 'absolute'
    },
    profileIcon: {
        height: height * 0.045,
        width: height * 0.045,
        borderRadius: 50,
        marginLeft: width * 0.03,
    },
    // bellIcon: {
    //     height: height * 0.045,
    //     width: height * 0.045,
    // },
    formShortBox: {
        justifyContent: 'center',
        alignItems: 'center',
        // borderWidth:1,
        width: "90%",
        // height: '22%',
        flexDirection: 'row',
        borderRadius: width * 0.02,
        // borderWidth:2,
        backgroundColor: '#ffffff',
        marginTop: '20%'
    },
    formShortBox2: {
        justifyContent: 'center',
        alignItems: 'center',
        // borderWidth:1,
        width: "90%",
        // height: '14%',
        flexDirection: 'row',
        borderRadius: width * 0.02,
        // borderWidth:2,
        backgroundColor: '#ffffff',
        marginTop: '4%'
    },
    // picker: {
    //     // borderWidth:1,
    //     width: "60%",
    //     // height: '20%',
    //     marginLeft: width * 0.15,
    // },
    // picker1: {
    //     // borderWidth:1,
    //     width: "60%",
    //     // height: '100%',
    //     marginLeft: width * 0.1,
    // },
    recent: {
        marginTop: 40,
        width: "95%",
        height: '35%',
        backgroundColor: '#ffffff',
        borderRadius: 10,
        shadowColor: '#000000',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.1,
        shadowRadius: 20,
        elevation: Platform.OS === 'ios' ? 10 : 5,
    },
    docDetailedWrapper2: {
        width: width * 0.9,
        height: height * 0.45,
        display: 'flex',
        // justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#ffffff',
        marginTop: 10,
        borderRadius: 15,
        shadowColor: 'rgba(1, 1, 1, 1)',
        elevation: 4,
        borderWidth: 1,
        borderColor: '#00cccc',
        // marginLeft: 18,
    },
    middleWrapper: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        backgroundColor: '#00cced',
        paddingLeft: 15
    },
    nameWrapper: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#ffffff'
    },
    closeIcon: {
        height: height * 0.04,
        width: height * 0.04,
        marginLeft: width * 0.04,
    },
    pickerBox: {
        justifyContent:'flex-start',
        alignItems: 'center',
        // borderWidth: 1,
        width: "80%",
        height: '15%',
        flexDirection: 'row',
        backgroundColor: '#fff',
        borderRadius: width * 0.02,
        marginTop: height * 0.03,
        paddingLeft:10
    },
    picker: {
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor:'#fff',
        width: "80%",
        height: '100%',
        flexDirection: 'row',
        backgroundColor: '#fff',
        borderRadius: width * 0.02,
        marginLeft:19
    },
    picker1: {
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor:'#fff',
        width: "94.5%",
        height: '100%',
        flexDirection: 'row',
        backgroundColor: '#fff',
        borderRadius: width * 0.02,
        marginLeft:10
    },
    dropdown: {
        height: height * 0.03,
        width: height * 0.03,
        position: "absolute",
        right: '3%',
    },
});