import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Dimensions,
  Image,
  Picker,
  FlatList,
  I18nManager,
  ToastAndroid,
  AlertIOS,
} from 'react-native';
const {width, height} = Dimensions.get('window');
const screenWidth = Dimensions.get('window').width;
import LinearGradient from 'react-native-linear-gradient';
import {Images} from '../../utils';
import Share from 'react-native-share';
import styles from './style';
import i18n from 'i18n-js';
import memoize from 'lodash.memoize';
import Clipboard from '@react-native-community/clipboard';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import {profile} from '../../redux/actions/profile';
import {paramcontents} from '../../redux/actions/paramcontents';

const translationGetters = {
  // lazy requires (metro bundler does not support symlinks)
  en: () => require('../../assets/en-US.json'),
  hi: () => require('../../assets/hi.json'),
};
const translate = memoize(
  (key, config) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key),
);
const myCustomShare = async (content_desc) => {
  const shareOptions = {
    message: content_desc,
    // urls:''  //for sharing multiple Image files...//
  };
  try {
    const ShareResponse = await Share.open(shareOptions);
    console.log(JSON.stringify(ShareResponse)); // to check which platform is used to share...//
  } catch (error) {
    console.log('Error =>', error);
  }
};

class ReferAndEarn extends Component {
  constructor(props) {
    super(props);
    this._getStorageValue();
    console.log("This is working everytime")
    this.state = {
      notifications: [],
      profile_pic: '',
      refreshing: false,
      profile: {},
      paramcontents: [],
      visible: false,
    };
    
  }
  componentDidMount() {
    this._getStorageValue();
  }

  async _getStorageValue() {
    var value = await AsyncStorage.getItem('token');
    console.log('shashank', value);
    var username = await AsyncStorage.getItem('username');
    if (value == null) {
      // console.log(value)
      this.props.navigation.replace('Login');
    } else
      this.props.profile(value, username).then(async () => {
        if (this.props.profile_error == 'Invalid Token') {
          await AsyncStorage.setItem('token', '');
          this.props.navigation.replace('Login');
        }
        this.setState({profile: this.props.profile1});
      });

      this.props.paramcontents(value, this.state.profile.user_id).then(async () => {
        console.log(this.props.paramcontents1)
        this.setState({paramcontents: this.props.paramcontents1});
      });
    this.state.token = value;
    return value;
  }
  showToast = () => {
    ToastAndroid.show('Refferal code copied !', ToastAndroid.SHORT);
  };

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={{flex: 1}}>
          <View style={styles.upperBar}>
            <View style={styles.backIconContainer}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Elections')}>
                <Image source={Images.backIcon} style={styles.barMenuIcon} />
              </TouchableOpacity>
            </View>
            <View style={styles.headingContainer}>
              <Text style={styles.barText}>{translate('refer')}</Text>
            </View>
          </View>
          <KeyboardAvoidingView behavior="padding">
            <LinearGradient
              colors={['#00cfcf', '#00cfef']}
              start={{x: 0.3, y: 0.1}}
              end={{x: 0.3, y: 1.9}}
              locations={[0.1, 0.6]}
              style={styles.wrapper}>
              <View
                style={{width: '100%', alignItems: 'center', marginTop: '5%'}}>
                <Text style={styles.topheading}>Invite Your Friend</Text>
                <View
                  style={{
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text style={styles.topheading1}>
                    And win more App Features
                  </Text>
                </View>
              </View>
              <View
                style={{
                  width: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: 10,
                }}>
                <Image
                  source={Images.invitation}
                  style={{height: height * 0.22, width: height * 0.3}}
                />
              </View>
            </LinearGradient>
            <View style={styles.docDetailedWrapper2}>
              <View style={{marginLeft: 20}}>
                <Image source={Images.netaHubLogo} style={styles.bellIcon1} />
              </View>
              <View style={styles.docNameWrapper}>
                <Text style={styles.docNameText}>
                  Share your refferal link via SMS/Email/Whatapp. You will earn
                  more App Points.
                </Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                marginTop: '5%',
                justifyContent: 'space-evenly',
              }}>
              <View
                style={{
                  flexDirection: 'column',
                  width: '18%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Image
                    source={Images.reffrSignup}
                    style={{height: height * 0.04, width: height * 0.045}}
                  />
                </View>
                <Text
                  style={{textAlign: 'center', fontSize: 10, marginTop: 15}}>
                  Invite Your Friends To Signup
                </Text>
              </View>

              <View
                style={{
                  flexDirection: 'column',
                  width: '18%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: '100%',
                    height: 15,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Image
                    source={Images.reffrRegistr}
                    style={{height: height * 0.05, width: height * 0.035}}
                  />
                </View>
                <Text
                  style={{textAlign: 'center', fontSize: 10, marginTop: 20}}>
                  Your Friend get Registered
                </Text>
              </View>

              <View
                style={{
                  flexDirection: 'column',
                  width: '18%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: '100%',
                    height: 23,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop: 8,
                  }}>
                  <Image
                    source={Images.refferReward}
                    style={{height: height * 0.05, width: height * 0.045}}
                  />
                </View>
                <Text
                  style={{textAlign: 'center', fontSize: 10, marginTop: 15}}>
                  You will be Rewarded with more Features
                </Text>
              </View>
              {/* <Text style={{ width: '18%', textAlign: 'center', fontSize: 10 }}>Your Friend get Product from US</Text> */}
              {/* <Text style={{ width: '18%', textAlign: 'center', fontSize: 10 }}>You and Your Friend Rewarded</Text> */}
            </View>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: '5%',
              }}>
              <Text
                style={{
                  width: '50%',
                  textAlign: 'center',
                  fontSize: 14,
                  fontWeight: 'bold',
                }}>
                YOUR REFFERAL CODE
              </Text>
            </View>
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <View style={styles.boxBtn}>
                <Text style={styles.btnText}>
                  {this.state.profile.mobile_no}
                </Text>
              </View>
             
              <TouchableOpacity
                style={{marginTop: 15}}
                onPress={() => {
                  Clipboard.setString(this.state.profile.mobile_no);
                  this.showToast();
                }}>
                <View>
                  <Text style={styles.btnText1}>TAP TO COPY</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 25,
              }}>
              <TouchableOpacity
                activeOpacity={1}
                style={{
                  flex: 1,
                  borderRadius: 20,
                  width: '60%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginLeft: 15,
                }}
                onPress={() => myCustomShare(this.state.paramcontents.content_desc)}>
                <LinearGradient
                  colors={['#00cfcf', '#00cfef']}
                  start={{x: 0.3, y: 0.1}}
                  end={{x: 0.3, y: 1.9}}
                  locations={[0.1, 0.6]}
                  style={styles.boxBtn2}>
                  <Text style={{color: '#fff'}}>REFER NOW</Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              activeOpacity={1}
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                paddingBottom: 40,
                marginTop: 15,
              }}
              onPress={() =>
                this.props.navigation.navigate('TermsAndConditions')
              }>
              <Text style={styles.btnText1}>Terms & Conditions</Text>
            </TouchableOpacity>
          </KeyboardAvoidingView>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => {
  //console.log(state)
  return {
    profile1: state.profile.profile[0],
    paramcontents1: state.paramcontents.paramcontents[0],
  };
};
export default connect(
  mapStateToProps,
  {
    profile,paramcontents
  },
)(ReferAndEarn);
