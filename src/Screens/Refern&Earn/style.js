import { StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
export default StyleSheet.create({
    container: {
        width,
        height,
        display: 'flex',
    },
    upperBar: {
        width,
        height: height * 0.075,
        backgroundColor: '#00b8e6',
        flexDirection: 'row',
        alignItems: 'center',
    },
    backIconContainer: {
    },
    headingContainer: {
        width: '50%',
        marginLeft: '10%',
        alignItems: 'center',
    },
    barText: {
        color: "#000000",
        fontSize: height * 0.03,
        fontWeight: 'bold'
    },
    barMenuIcon: {
        height: height * 0.04,
        width: height * 0.04,
        marginLeft: width * 0.04,
    },
    wrapper: {
        width: width * 1,
        height: height * 0.42,
        backgroundColor: '#00cccc',
        alignItems: 'center',
        position: 'absolute'
    },
   
    docDetailedWrapper2: {
        width: width * 0.9,
        height: height * 0.08,
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#ffffff',
        marginTop: height * 0.38,
        borderRadius: 5,
        shadowColor: 'rgba(1, 1, 1, 1)',
        // elevation: 4,
        marginLeft: 18,
        borderBottomWidth: .5,
        borderBottomColor: "#C8C8C8",
        flexDirection:'row'
    },
    docNameWrapper: {
        width: '75%',
        // marginLeft: 25,
        display: 'flex',
        // marginTop: 10,
        flexDirection:'row',
        marginLeft:20
    },
    docNameText: {
        fontFamily: 'Helvetica Neue',
        color: '#000',
        fontSize: width * 0.035,
        fontWeight: 'bold',
        paddingBottom: 1,
    },
    boxBtn: {
        width: width * 0.3,
        height: height * 0.05,
        display: 'flex',
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        flexDirection: 'row',
        marginTop: '5%'
    },
    btnText: {
        // width:'100%',
        fontWeight: 'bold',
        textShadowColor: '#ffffff',
        fontSize: 14,
        color: '#00cced',
    },
    btnText1: {
        // width:'100%',
        fontWeight: 'bold',
        textShadowColor: '#ffffff',
        fontSize: 10,
        color: '#00cced',
        // marginTop: 20,
        marginLeft:20
    },
    boxBtn2: {
        width: width * 0.35,
        height: height * 0.05,
        display: 'flex',
        // backgroundColor: '#00cced',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
    },
    topheading:{
        // paddingTop: height*0.03,
        fontSize: width*0.065,
        color: '#fff',
        alignItems: 'center'
    },
    topheading1:{
        // paddingTop: height*0.03,
        fontSize: width*0.065,
        color: '#fff',
        alignItems: 'center'
    },
    bellIcon1: {
        height: height * 0.04,
        width: height * 0.04,
        borderRadius:50
        
    },
});