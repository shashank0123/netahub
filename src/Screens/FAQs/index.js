import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image, Picker, FlatList, I18nManager
} from "react-native";
const { width, height } = Dimensions.get('window');
import InputField from '../../Commons/input';
import { Images } from '../../utils'
import styles from './style';
import Share from "react-native-share";
import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
//import for the animation of Collapse and Expand
import Collapsible from 'react-native-collapsible';
//import for the collapsible/Expandable view
import Accordion from 'react-native-collapsible/Accordion';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { faq } from '../../redux/actions/faq';
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
const translationGetters = {
    // lazy requires (metro bundler does not support symlinks)
    en: () => require("../../assets/en-US.json"),
    hi: () => require("../../assets/hi.json")
};

const translate = memoize(
    (key, config) => i18n.t(key, config),
    (key, config) => (config ? key + JSON.stringify(config) : key)
);

class FAQs extends Component {
    state = {
        activeSections: [], faqlist: [],
        profile_pic: ''
    };
    setSections = sections => {
        if (sections == 'undefined') {
            this.setState({
                activeSections: [],
            });
        }
        else {
            //setting up a active section state
            this.setState({
                activeSections: sections.includes(undefined) ? [] : sections,
            });
        }
    };
    componentDidMount() {
        this._getStorageValue()
    }
    async _getStorageValue() {
        var value = await AsyncStorage.getItem('token')
        var profile_pic = await AsyncStorage.getItem('profile_pic')
        this.state.profile_pic = profile_pic
        if (value == null) {
            console.log(value)
            this.props.navigation.replace('Login')
        }
        else {
            this.state.token = value
            this.props.faq(this.state).then(async () => {
                if (this.props.faq_error == "Invalid Token") {
                    await AsyncStorage.setItem('token', "")
                    this.props.navigation.replace('Login')
                }
                if (this.props.faq1.length > 0) {
                    console.log(this.props.faq1, "this is tootal data")
                    this.setState({ faqlist: this.props.faq1 })
                }
                else {

                }
                console.log(this.state.faqlist)
            })
        }
        return value
    }
    renderHeader = (section, _, isActive) => {
        //Accordion Header view
        return (
            <Animatable.View
                // duration={400}
                // style={[styles.header, isActive ? styles.active : styles.inactive]}
                transition="backgroundColor">
                <LinearGradient
                    colors={['#79efef', '#cfe7ea']}
                    start={{ x: 0.3, y: 0.1 }}
                    end={{ x: 0.3, y: 1.5 }}
                    locations={[0.1, 0.6]}
                    style={styles.searchWrapper}>
                    <Text style={{ fontSize: 16, fontWeight: 'bold', marginLeft: '2%' }}>{section.question}</Text>
                </LinearGradient>
            </Animatable.View>
        );
    };
    renderContent(section, _, isActive) {
        //Accordion Content view
        return (
            <Animatable.View
                style={[styles.content, isActive ? styles.active : styles.inactive]}
            >
                <Animatable.Text
                    style={{ textAlign: 'left', width: width * 0.95, backgroundColor: '#FFFFFF', elevation: 4, paddingLeft: 25 }}>
                    {section.answer}
                </Animatable.Text>
            </Animatable.View>
        );
    }
    render() {
        const { multipleSelect, activeSections } = this.state;
        return (
            <View style={styles.container}>
                <View style={styles.upperBar}>
                    <View style={styles.backIconContainer}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image source={Images.backIcon} style={styles.barMenuIcon} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.headingContainer}>
                        <Text style={styles.barText}>FAQs</Text>
                    </View>
                    <View>
                        <TouchableOpacity onPress={() =>
                            this.props.navigation.navigate('Profile')}>
                            <Image source={{ uri: 'https://netahub.com/images/user/' + this.state.profile_pic }} style={styles.profileIcon} />
                        </TouchableOpacity>
                    </View>
                </View>
                <ScrollView>

                    {this.props.faq1.length > 0 ?
                        console.log(this.props.faq1.length, 'shashank23', this.props.faq1)
                        :
                        <></>}
                    {this.props.faq1.length > 0 ?
                        <Accordion
                            activeSections={activeSections}
                            sections={this.state.faqlist}
                            touchableComponent={TouchableOpacity}
                            renderHeader={this.renderHeader}
                            renderContent={this.renderContent}
                            onChange={this.setSections}
                        />
                        :
                        <></>}

                </ScrollView>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    // console.log(state)
    return {
        faq1: state.faq.faq,
        faq_error: state.faq.faq_error
    };
};
export default connect(mapStateToProps, {
    faq
})(FAQs);
