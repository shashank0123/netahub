import { StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
export default StyleSheet.create({
    container: {
        width,
        height,
        display: 'flex',
        alignItems: 'center'
    },
    upperBar: {
        width,
        height: height * 0.075,
        backgroundColor: '#00b8e6',
        flexDirection: 'row',
        alignItems: 'center',
    },
    backIconContainer: {
    },
    headingContainer: {
        width: '60%',
        marginLeft: '10%',
        alignItems: 'center',
    },
    barText: {
        color: "#000000",
        fontSize: height * 0.03,
        fontWeight: 'bold'
    },
    barMenuIcon: {
        height: height * 0.04,
        width: height * 0.04,
        marginLeft: width * 0.04,
    },
    scrollWrapper: {
        width,
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingBottom: 35
        // backgroundColor: 'pink',
    },
    profileIcon: {
        height: height * 0.045,
        width: height * 0.045,
        borderRadius: 50,
        marginLeft: width * 0.03,
    },
    // bellIcon: {
    //     height: height * 0.045,
    //     width: height * 0.045,
    // },
    searchWrapper: {
        height: height * 0.060,
        width: width * 0.95,
        // alignSelf:"center",
        backgroundColor: "#b6f2f2",
        flexDirection: 'row',
        marginTop: 20,
        alignItems: 'center',
    },
    boxNominate: {
        width: width * 0.25,
        height: height * 0.04,
        display: 'flex',
        backgroundColor: '#00cced',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        // marginLeft:16
    },
    symptomWrapper: {
        // width: '95%',
        height: height * 0.1,
        display: 'flex',
        // flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 15,
    },
    symptomBox: {
        width: width * 0.95,
        height: height * 0.1,
        display: 'flex',
        borderColor: '#29E093',
        backgroundColor: '#FFFFFF',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderRadius: 10,
        borderWidth: 1,
        paddingLeft: 10,
        elevation: 4,
        flexDirection: 'row'
    },
    symptomText: {
        fontWeight: 'normal',
        textShadowColor: '#000000',
        fontSize: 24,
        lineHeight: 32,
        color: '#29E093',
        marginLeft: 40
    },
    gradientBox: {
        width: width * 0.04,
        height: height * 0.098,
        display: 'flex',
        borderWidth: 1,
        borderColor: '#29E093',
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 0,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 0,
        marginLeft: -10
    },
});