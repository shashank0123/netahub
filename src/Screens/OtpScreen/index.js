import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image, I18nManager, Alert
} from "react-native";
const { width, height } = Dimensions.get('window');
import InputField from '../../Commons/input';
import { Input } from 'react-native-elements';
import { Images } from '../../utils'
import LinearGradient from 'react-native-linear-gradient';
import styles from './style';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { otpScreen } from '../../redux/actions/otpScreen';
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
import LoadingIcon from '../../components/LoadingIcon';
const translationGetters = {
    // lazy requires (metro bundler does not support symlinks)
    en: () => require("../../assets/en-US.json"),
    hi: () => require("../../assets/hi.json")
};
const translate = memoize(
    (key, config) => i18n.t(key, config),
    (key, config) => (config ? key + JSON.stringify(config) : key)
);
class OtpScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            loadingApp: false,
            option: 0,
            otp: '',
            new_password: '',
            conf_password: '',
            mobile_no: this.props.route.params.mobile_no
        };
    }
    onSubmit() {
        // console.log(this.state)
        this.setState({ loadingApp: true })
        if (this.state.otp != '' && this.state.new_password != '') {
            this.props.otpScreen(this.state).then(async () => {
                console.log(this.props.otpScreen1)
                this.props.navigation.replace('AppHome')
            })
        }
        else {
            Alert.alert(
                'NetaHub',
                'Please check your credentials',
            )
        }
        this.setState({ loadingApp: false })
        // this.showAlert();
    }
    loadingRender() {
        if (this.state.loadingApp) {
            return (
                <LoadingIcon />
            )
        }
    }
    render() {
        return (
            <View style={styles.container}>
                {this.loadingRender()}
                <KeyboardAvoidingView behavior="padding">
                    <ScrollView
                        contentContainerStyle={styles.scrollWrapper}
                        showsVerticalScrollIndicator={false}>
                        <LinearGradient
                            colors={['#00cccc', '#00cced']}
                            start={{ x: 0.3, y: 0.1 }}
                            end={{ x: 0.3, y: 1.9 }}
                            locations={[0.1, 0.6]}
                            style={styles.wrapper}>
                            {/* <View style={styles.wrapper}> */}
                            <View style={styles.header}>
                                <Image source={Images.netaHubLogo} style={{ width: width * 0.35, height: height * 0.19, borderRadius: 111 }} />
                            </View>
                            {/* </View> */}
                        </LinearGradient>
                        <View style={styles.loginArea}>
                            <InputField
                                name={'OTP'}
                                alertMessage={'Please Enter OTP you Recieved'}
                                errorMessage={this.state.loginMsg}
                                placeholder={' Enter OTP'}
                                onChangeText={otp => this.setState({ otp })}
                                value={this.state.otp}
                            />
                            <InputField
                                name={'New Password'}
                                placeholder={' Enter New Password'}
                                alertMessage={'Please Enter New Password'}
                                secureTextEntry={true}
                                errorMessage={this.state.loginMsg}
                                onChangeText={new_password => this.setState({ new_password })}
                                value={this.state.new_password}
                            />
                            <InputField
                                name={'Confirm Password'}
                                alertMessage={'Please Confirm Your New Password'}
                                errorMessage={this.state.loginMsg}
                                placeholder={' Confirm Password'}
                                onChangeText={conf_password => this.setState({ conf_password })}
                                value={this.state.conf_password}
                            />
                            {/* <View style={{ marginLeft: 5, }}>
                                <Text style={{ paddingLeft: 15, fontWeight: 'bold' }}>Password</Text>
                                <Input
                                    style={{ fontSize: 14, fontWeight: 'bold', width: "100%", marginTop: 8 }}
                                    placeholder="Password"
                                    secureTextEntry={true}
                                    errorStyle={{ color: 'red' }}
                                    errorMessage={this.props.errorMessage}
                                />
                            </View> */}
                            <View style={{ marginBottom: -45, justifyContent: 'center', alignItems: 'center', marginTop: 10, marginBottom: -30 }}>
                                <LinearGradient
                                    colors={['#00cfcf', '#00cfef']}
                                    start={{ x: 0.3, y: 0.1 }}
                                    end={{ x: 0.3, y: 1.9 }}
                                    locations={[0.1, 0.6]}
                                    style={{ borderRadius: 20 }}>
                                    <TouchableOpacity
                                        activeOpacity={1}
                                        style={styles.boxBtn2}
                                        onPress={() => { if (!this.state.loadingApp) this.onSubmit() }
                                        }>
                                        <Text
                                            style={styles.btnText}>
                                            Reset
                      </Text>
                                    </TouchableOpacity>
                                </LinearGradient>
                            </View>
                        </View>
                        <View style={{ marginTop: 50 }}>
                            <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}
                                onPress={() =>
                                    this.props.navigation.navigate('Ragistration')
                                }>
                                <Text>
                                    <Text> Dont Have an Account?</Text>
                                    <Text style={{ fontWeight: 'bold' }}> Create Now</Text>
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}
                                onPress={() =>
                                    this.props.navigation.navigate('ForgetPassword')
                                }>
                                <Text style={styles.forgetText}>
                                    <Text > Login</Text>
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    // console.log(state)
    return {
        otpScreen1: state.otpScreen.otpScreen,
        otpScreen_error: state.otpScreen.otpScreen_error
    };
};
export default connect(mapStateToProps, {
    otpScreen
})(OtpScreen);
