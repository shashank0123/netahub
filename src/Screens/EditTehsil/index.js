import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image, FlatList,I18nManager,ActivityIndicator
} from "react-native";
const { width, height } = Dimensions.get('window');
import InputField from '../../Commons/input';
import { Images } from '../../utils'
import styles from './style';
// import { Picker } from '@react-native-community/picker';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { editTehsil } from '../../redux/actions/editTehsil';
import { tehsillist } from '../../redux/actions/tehsillist';
import { usertehsil } from '../../redux/actions/usertehsil';
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
import { Picker } from 'react-native-woodpicker';
const LoadingIcon = ({ loadingApp }) => <ActivityIndicator
    style={{ zIndex: 5, position: 'absolute', alignItems: 'center', top: '63%', left: '45%', }} size="large" color="#00b8e6" animating={loadingApp} ></ActivityIndicator>;
const translationGetters = {
  // lazy requires (metro bundler does not support symlinks)
  en: () => require("../../assets/en-US.json"),
  hi: () => require("../../assets/hi.json")
};

const translate = memoize(
  (key, config) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key)
);


class EditTehsil extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedValue: '',
            selectedValue1: '',
            profile_pic:'',
            buttonText:'Next',
            district_id : this.props.route.params.district_id,
            tehsillist:[],
            loadingApp: false,
            pickedTehsil: '',
        }
        this._getStorageValue()
    }
    data = [
        { label: "Select Tehsil ", value: 1 },
    ];
    handlePicker = data => {
        this.setState({ pickedTehsil: data });
        if (this.props.usertehsil1[0].tehsil_id != data.tehsil_id) {
            this.setState({ buttonText: 'Continue' })
        }
        else
            this.setState({ buttonText: 'Next' })
        this.setState({ tehsil_id: data.value })
    };
    async _getStorageValue(){
        
    var value = await AsyncStorage.getItem('token')
    var profile_pic = await AsyncStorage.getItem('profile_pic')
    this.state.profile_pic = profile_pic
    if (value == null){
      this.props.navigation.replace('Login')
    }
    else{
        this.setState({'token':value})
        this.props.tehsillist(this.state).then(async () => {
                if (this.props.profile_error == "Invalid Token") {
                    await AsyncStorage.setItem('token', "")
                    this.props.navigation.replace('Login')
                }
                var tehsillist = []
                tehsillist = this.props.tehsillist1.map((obj) => {
                    var obj1 = {
                        label: obj.tehsil_name,
                        value: obj.tehsil_id
                    }
                    return obj1
                    // console.log(obj1)
                })
                this.data = tehsillist
                this.setState({ tehsillist: this.props.tehsillist1 })
            })
        this.props.usertehsil(value).then(async () => {
                if (this.props.profile_error == "Invalid Token") {
                    await AsyncStorage.setItem('token', "")
                    this.props.navigation.replace('Login')
                }
                this.setState({ usertehsil: this.props.usertehsil1[0].tehsil_id, selectedValue1: this.props.usertehsil1[0].tehsil_id, tehsil_id: this.props.usertehsil1.tehsil_id , 
                                pickedTehsil : 
                        { 
                            "label": this.props.usertehsil1[0].tehsil_name, 
                            "value": this.props.usertehsil1[0].tehsil_id 
                        }})
            })
        
      }
    return value
  }

  changeTehsil(itemValue){
      this.setState({ selectedValue1: itemValue, tehsil_id: itemValue })

      if (this.props.usertehsil1[0].tehsil_id != itemValue){
          this.setState({buttonText:'Continue'})
      }
      else
          this.setState({buttonText:'Next'})
  }

  renderForm(){

    if (this.state.selectedValue1 == ''  || this.state.selectedValue1== 0)
      return (<>
      <View style={{marginTop:15}}>
        <InputField
            name={'Tehsil Name'}
            alertMessage={'Please Enter Your Tehsil Name'}
            //   errorMessage={this.state.loginMsg}
            placeholder={'Enter Tehsil'}
            onChangeText={
                tehsil_name => {
                    
                    this.setState({ tehsil_name })
                }}
            value={this.state.tehsil_name}
        />
        </View>
        </>)
    else{

    }
  }

  submitTehsil(){
    this.setState({ loadingApp: true })

      if (this.state.buttonText == 'Next'){
          this.props.navigation.replace('EditBlock', {tehsil_id:this.state.pickedTehsil.value});
      }
      else
      this.props.editTehsil(this.state).then(async () => {
        if (this.props.editTehsil_error == "Invalid Token") {
          await AsyncStorage.setItem('token', "")
          this.props.navigation.replace('Login')
        }
        else 
          this.props.navigation.replace('EditBlock', {tehsil_id:this.state.pickedTehsil.value});        
      })
  }
  loadingRender() {
    if (this.state.loadingApp) {
        return (
            <LoadingIcon />
        )
    }
}
    render() {
        return (
            <View style={styles.container}>
                  <View style={{ zIndex: 5, }}>
                    {this.loadingRender()}
                <View style={styles.upperBar}>
                    <View style={styles.backIconContainer}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image source={Images.backIcon} style={styles.barMenuIcon} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.headingContainer}>
                        <Text style={styles.barText}>Tehsil</Text>
                    </View>
                    <View>
                        <TouchableOpacity onPress={() =>
                            this.props.navigation.navigate('Profile')}>
                            <Image source={{ uri: 'https://netahub.com/images/user/' + this.state.profile_pic }} style={styles.profileIcon} />
                        </TouchableOpacity>
                    </View>
                </View>
                <KeyboardAvoidingView behavior="padding">
                    <ScrollView
                        contentContainerStyle={styles.scrollWrapper}
                        showsVerticalScrollIndicator={false}>
                        <View style={styles.wrapper}>
                            <View style={styles.header}>
                            <Image source={Images.netaHubLogo} style={{ width: width *0.35, height: height * 0.19,borderRadius:111 }} />
                            </View>
                        </View>
                        <View style={styles.loginArea}>
                            {/* <InputField
                                name={'Feedback'}
                                //   errorMessage={this.state.loginMsg}
                                placeholder={'Write your Feedback'}
                            /> */}

                            {/* <View style={styles.formShortBox2}>
                                
                                <Picker
                                    selectedValue={this.state.selectedValue1}
                                    style={styles.picker1}
                                    onValueChange={(itemValue, itemIndex) => this.changeTehsil(itemValue)}>
                                    <Picker.Item label="Add New" value="0" />
                                    {this.state.tehsillist.map(nom => <Picker.Item key={nom.tehsil_id} label={nom.tehsil_name} value={nom.tehsil_id} />)}
                                </Picker>
                            </View> */}
                             <View style={styles.pickerBox}>
                                    <Text style={{ fontSize: 16, marginLeft: 5 }}>Tehsil :</Text>
                                    <Picker
                                        onItemChange={this.handlePicker}
                                        style={styles.picker}
                                        items={this.data}
                                        placeholder="Select Tehsil"
                                        placeholderStyle={{ color: '#000' }}
                                        item={this.state.pickedTehsil}
                                    // backdropAnimation={{ opactity: 0 }}
                                    //androidPickerMode="dropdown"
                                    //isNullable
                                    //disable
                                    />
                                    <Image source={Images.dropDown} style={styles.dropdown} />
                                </View>
                            





                            {this.renderForm()}
                            {/* <View style={{ marginTop:height * 0.045, }}>
                                <LinearGradient
                                    colors={['#00cfcf', '#00cfef']}
                                    start={{ x: 0.3, y: 0.1 }}
                                    end={{ x: 0.3, y: 1.9 }}
                                    locations={[0.1, 0.6]}
                                    style={{ width: '80%', borderRadius: 20 }}>
                                    <TouchableOpacity
                                        activeOpacity={1}
                                        style={styles.boxBtn2}
                                        onPress={() =>
                                            this.submitTehsil()
                                        }>
                                        <Text
                                            style={styles.btnText}>
                                            {this.state.buttonText}
                     </Text>
                                    </TouchableOpacity>
                                </LinearGradient>
                            </View> */}
                             <View style={{ flexDirection: 'row', marginTop: '5%' }}>
                                    <TouchableOpacity
                                        activeOpacity={1}
                                        style={{ flex: 1, borderRadius: 20, width: '60%', justifyContent: 'center', alignItems: 'center', marginBottom: -45,marginLeft:15 }}
                                        onPress={() => this.props.navigation.goBack()}>
                                        <LinearGradient
                                            colors={['#00cfcf', '#00cfef']}
                                            start={{ x: 0.3, y: 0.1 }}
                                            end={{ x: 0.3, y: 1.9 }}
                                            locations={[0.1, 0.6]}
                                            style={styles.boxBtn2}>
                                            <Text
                                                style={styles.btnText}>
                                                Skip </Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        activeOpacity={1}
                                        style={{ flex: 1, borderRadius: 20, width: '60%', justifyContent: 'center', alignItems: 'center', marginBottom: -45,marginRight:15 }}
                                        onPress={() =>
                                            this.submitTehsil()
                                        }>
                                        <LinearGradient
                                            colors={['#00cfcf', '#00cfef']}
                                            start={{ x: 0.3, y: 0.1 }}
                                            end={{ x: 0.3, y: 1.9 }}
                                            locations={[0.1, 0.6]}
                                            style={styles.boxBtn2}>
                                            <Text
                                                style={styles.btnText}>
                                                {this.state.buttonText}
                                            </Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                </View>
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
                </View>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
                // console.log(state)
                return {
                  editTehsil1: state.editTehsil.editTehsil,
                  editTehsil_error: state.editTehsil.editTehsil_error,
                  tehsillist1: state.tehsillist.tehsillist,
                  tehsillist_error: state.tehsillist.tehsillist_error,
                  usertehsil1: state.usertehsil.usertehsil,
                  usertehsil_error: state.usertehsil.usertehsil_error,
                };
              };
              export default connect(mapStateToProps, {
                editTehsil, usertehsil, tehsillist
              })(EditTehsil);
