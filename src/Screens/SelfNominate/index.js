import React, { Component, setState } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image, FlatList,I18nManager
} from "react-native";
const { width, height } = Dimensions.get('window');
import InputField from '../../Commons/input';
import { Images } from '../../utils'
import styles from './style';
// import { Picker } from '@react-native-community/picker';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { selfNominate } from '../../redux/actions/selfNominate';
import { party } from '../../redux/actions/party';
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
import AwesomeAlert from 'react-native-awesome-alerts';
import { Picker } from 'react-native-woodpicker';
import Header from "../../Commons/header";
const translationGetters = {
  // lazy requires (metro bundler does not support symlinks)
  en: () => require("../../assets/en-US.json"),
  hi: () => require("../../assets/hi.json")
};

const translate = memoize(
  (key, config) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key)
);

const Nominee = []
class SelfNominate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedValue: '',
            selectedValue1: '',
            election_id: this.props.route.params.election_id, 
            party : [],
            token:'',
            survey_id:this.props.route.params.survey_id, 
            cons_id:this.props.route.params.cons_id,
            nominee_name:'',
            mobile_no:'',
            photo_path:'',
            filedata:'',
            nominee_desc:'',
            profile_pic:'',
            showAlert: false,
            pickedParty: '',
        }
        this._getStorageValue()
        
    }
    data = [
        { label: "Select Party ", value: 1 },
    ];
    handlePicker = data => {
        this.setState({ pickedParty: data });
        console.log(data)
        this.setState({ party_id: data.value })
    };

    async _getStorageValue() {
        console.log(this.props.route.params.election_id)
        // this.setState({})
        var value = await AsyncStorage.getItem('token')
        var profile_pic = await AsyncStorage.getItem('profile_pic')
        this.state.profile_pic = profile_pic
        if (value == null) {
            this.props.navigation.replace('Login')
        }
        else {
            this.setState({ token: value })
            this.props.party(value).then(async () => {
                var party = []
                if (this.props.party1[0]) {
                    party = this.props.party1.map((obj) => {
                        var obj1 = {
                            label: obj.party_name,
                            value: obj.party_id
                        }
                        return obj1
                    }
                    )
                    this.data = party
                }
                this.setState({ party: this.props.party1 })
            })
        }
        this.props.party(value).then(data => {
            console.log(this.props.party1)
            this.setState({party: this.props.party1})
        })
        console.log(this.state)
        return value
    }


    submitButton(){
        console.log(this.state.party_id)
        this.props.selfNominate(this.state).then(async () => {
            if (this.props.survey_error == "Invalid Token") {
                await AsyncStorage.setItem('token', "")
                this.props.navigation.replace('Login')
            }
            // this.props.navigation.replace('AppHome')
        })
        console.log(this.state.party_id)
        this.showAlert()

    }
    showAlert = () => {
        this.setState({
            showAlert: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlert: false
        });
        this.props.navigation.replace('AppHome')
    };
    render() {


        // get > partylist post selfNominate

        console.log(this.state)
        return (
            <View style={styles.container}>
                <View style={styles.upperBar}>
                    <View style={styles.backIconContainer}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image source={Images.backIcon} style={styles.barMenuIcon} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.headingContainer}>
                        <Text style={styles.barText}>Self Nominate</Text>
                    </View>
                    <View>
                        <TouchableOpacity onPress={() =>
                            this.props.navigation.navigate('Profile')}>
                            <Image source={{ uri: 'https://netahub.com/images/user/' + this.state.profile_pic }} style={styles.profileIcon} />
                        </TouchableOpacity>
                    </View>
                </View>
                {/* <Header title="self_nominated"
                    profile_pic={this.state.profile_pic}
                    onPressBack={() => this.props.navigation.goBack()}
                    // onPressNotification={() => this.props.navigation.navigate('NotificationScreen')}
                    onPressProfile={() => this.props.navigation.navigate('Profile')}
                    // onPressRefer={() => this.props.navigation.navigate('ReferAndEarn')}
                     /> */}
                <KeyboardAvoidingView behavior="padding">
                    <ScrollView
                        contentContainerStyle={styles.scrollWrapper}
                        showsVerticalScrollIndicator={false}>
                        <View style={styles.wrapper}>
                            <View style={styles.header}>
                            <Image source={Images.netaHubLogo} style={{ width: width *0.35, height: height * 0.19,borderRadius:111 }} />
                            </View>
                        </View>
                        <View style={styles.loginArea}>
                            {/* <InputField
                                name={'Feedback'}
                                //   errorMessage={this.state.loginMsg}
                                placeholder={'Write your Feedback'}
                            /> */}

                            {/* <View style={styles.formShortBox2}>
                                
                                <Picker
                                    selectedValue={this.state.selectedValue1}
                                    style={styles.picker1}
                                    itemStyle={styles.pickerItem}
                                    onValueChange={(itemValue, itemIndex) => this.setState({ selectedValue1: itemValue, party_id : itemValue })}>
                                    <Picker.Item label="Party Name" value="Select Party" />
                                    {this.state.party.map(nom => <Picker.Item key={nom.party_id} label={nom.party_name} value={nom.party_id} />)}
                                </Picker>
                            </View> */}
                            <View style={styles.pickerBox}>
                                <Text style={{ fontSize: 16, }}>Party :</Text>
                                <Picker
                                    onItemChange={this.handlePicker}
                                    style={styles.picker}
                                    items={this.data}
                                    placeholder="Select Party"
                                    placeholderStyle={{ color: '#000' }}
                                    item={this.state.pickedParty}
                                // backdropAnimation={{ opactity: 0 }}
                                //androidPickerMode="dropdown"
                                //isNullable
                                //disable
                                />
                                <Image source={Images.dropDown} style={styles.dropdown} />
                            </View>
                            <View style={{ marginTop: height * 0.06,justifyContent:'center',alignItems:'center',}}>
                                <LinearGradient
                                    colors={['#00cfcf', '#00cfef']}
                                    start={{ x: 0.3, y: 0.1 }}
                                    end={{ x: 0.3, y: 1.9 }}
                                    locations={[0.1, 0.6]}
                                    style={{ borderRadius: 20 }}>
                                    <TouchableOpacity
                                        activeOpacity={1}
                                        style={styles.boxBtn2}
                                        onPress={() =>{
                                            this.submitButton()
                                        }
                                        }>
                                        <Text
                                            style={styles.btnText}>
                                            Submit
                     </Text>
                                    </TouchableOpacity>
                                </LinearGradient>
                            </View>
                            <AwesomeAlert
                            
                            show={this.state.showAlert}
                            showProgress={false}
                            title={"NETAHUB"}
                            titleStyle={{fontSize:14,color:'#000',fontWeight:"bold"}}
                            message="Nomination Done Succesfully"
                            closeOnTouchOutside={true}
                            closeOnHardwareBackPress={false}
                            //showCancelButton={true}
                            showConfirmButton={true}
                            confirmText="Okay"
                            confirmButtonColor="#00cced"
                            onConfirmPressed={() => {
                              this.hideAlert();
                            }}
                        />
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
                // console.log(state)
                return {
                  selfNominate1: state.selfNominate.selfNominate,
                  selfNominate_error: state.selfNominate.selfNominate_error,
                  party1: state.party.party,
                  party_error: state.party.party_error
                };
              };
              export default connect(mapStateToProps, {
                selfNominate, party
              })(SelfNominate);
