import { StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
export default StyleSheet.create({
    container: {
        width,
        height,
        display: 'flex',
        justifyContent: 'flex-start',
    },
    upperBar: {
        width,
        height: height * 0.075,
        backgroundColor:'#00b8e6',
        flexDirection: 'row',
        alignItems: 'center',
    },
    backIconContainer: {
    },
    headingContainer: {
        width: '60%',
        marginLeft: '10%',
        alignItems: 'center',
    },
    barText: {
        color: "#000000",
        fontSize: height * 0.03,
        fontWeight: 'bold'
    },
    barMenuIcon: {
        height: height * 0.04,
        width: height * 0.04,
        marginLeft: width * 0.04,
    },
    barNavIcon: {
        height: height * 0.05,
        width: height * 0.035,
        marginLeft: width * 0.27,
    },
    barRefreshIcon: {
        height: height * 0.04,
        width: height * 0.04,
        marginLeft: width * 0.03,
    },
    scrollWrapper: {
        width,
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingBottom: 35
        // backgroundColor: 'pink',
    },
    wrapper: {
        width: width * 1,
        height: height * 0.5,
        backgroundColor: '#00cccc',
        alignItems: 'center'
    },
    header: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '8%',
    },
    loginArea: {
        paddingHorizontal: 10,
        paddingVertical: 10,
        height: height * 0.25,
        borderWidth: 1,
        marginHorizontal: 20,
        borderRadius: 25,
        marginTop: -110,
        borderColor: 'white',
        elevation: 3,
        backgroundColor: "#ffffff",width:'90%'
    },
    loginTitle: {
        paddingLeft: 15,
    },
    boxBtn2: {
        width: width * 0.6,
        height: height * 0.07,
        display: 'flex',
        // backgroundColor: '#00cced',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
    },
    btnText: {
        fontWeight: 'bold',
        textShadowColor: '#ffffff',
        fontSize: 20,
        color: '#ffffff'
    },
    forgetText: {
        fontWeight: 'bold',
        marginTop: '10%',
        fontSize: 20
    },
    profileIcon: {
        height: height * 0.045,
        width: height * 0.045,
        borderRadius: 50,
        marginLeft: width * 0.03,
    },
    // bellIcon: {
    //     height: height * 0.045,
    //     width: height * 0.045,
    // },
    formShortBox2: {
        justifyContent: 'center',
        alignItems: 'center',
        // borderWidth:1,
        width: "100%",
        height: '35%',
        flexDirection: 'row',
        // borderRadius:width*0.02,
        // borderWidth:2,
        // backgroundColor: '#E8E8E8',
        borderBottomWidth:1,
        borderBottomColor:'#E8E8E8',
        marginTop: '4%'
    },
    picker1: {
        // borderWidth:1,
        width: "90%",
        height: '100%',
        // marginLeft: width * 0.1,
    },
    pickerItem: {
        height:10
      },
      formShortBox: {
        justifyContent:'flex-start',
        alignItems: 'center',
        
        width: "92%",
        height: '10%',
        flexDirection: 'row',
        borderBottomColor:'#A9A9A9',
       
       borderBottomWidth:1,
        // marginTop: height * 0.01,
        backgroundColor: '#ffffff',
        // marginLeft:15
        paddingLeft:8
       
    },
    pickerBox: {
        justifyContent:'flex-start',
        alignItems: 'center',
        // borderWidth: 1,
        width: "95%",
        height: '25%',
        flexDirection: 'row',
        backgroundColor: '#fff',
        borderRadius: width * 0.02,
        marginTop: height * 0.03,
        paddingLeft:10,
        borderBottomWidth:1,
        borderBottomColor:'#E8E8E8',
    },
      
    picker: {
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor:'#fff',
        width: "80%",
        height: '100%',
        flexDirection: 'row',
        backgroundColor: '#fff',
        borderRadius: width * 0.02,
        marginLeft:10
    },
    picker1: {
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor:'#fff',
        width: "94.5%",
        height: '100%',
        flexDirection: 'row',
        backgroundColor: '#fff',
        borderRadius: width * 0.02,
        marginLeft:10
    },
    dropdown: {
        height: height * 0.03,
        width: height * 0.03,
        position: "absolute",
        right: '3%',
    },
    // formShortBox: {
    //     justifyContent:'flex-start',
    //     alignItems: 'center',
        
    //     width: "92%",
    //     height: '15%',
    //     flexDirection: 'row',
    //     borderBottomColor:'#A9A9A9',
       
    //    borderBottomWidth:1,
    //     // marginTop: height * 0.01,
    //     backgroundColor: '#ffffff',
    //     // marginLeft:15
    //     paddingLeft:8
       
    // },
});