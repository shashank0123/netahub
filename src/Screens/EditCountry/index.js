import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image, FlatList, I18nManager, ActivityIndicator
} from "react-native";
const { width, height } = Dimensions.get('window');
import InputField from '../../Commons/input';
import { Images } from '../../utils'
import styles from './style';
// import { Picker } from '@react-native-community/picker';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { editCountry } from '../../redux/actions/editCountry';
import { country } from '../../redux/actions/country';
import { usercountry } from '../../redux/actions/usercountry';
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
import { Picker } from 'react-native-woodpicker';
const LoadingIcon = ({ loadingApp }) => <ActivityIndicator
    style={{ zIndex: 5, position: 'absolute', alignItems: 'center', top: '63%', left: '45%', }} size="large" color="#00b8e6" animating={loadingApp} ></ActivityIndicator>;
const translationGetters = {
    // lazy requires (metro bundler does not support symlinks)
    en: () => require("../../assets/en-US.json"),
    hi: () => require("../../assets/hi.json")
};
const translate = memoize(
    (key, config) => i18n.t(key, config),
    (key, config) => (config ? key + JSON.stringify(config) : key)
);
class EditCountry extends Component {
    constructor(props) {
        super(props);
        this.state = {
            token: '',
            selectedValue: '',
            selectedValue1: '',
            buttonText: 'Next',
            country: [],
            pincode: this.props.route.params.pincode,
            profile_pic: '',
            loadingApp: false,
            pickedCountry: '',
        }
        this._getStorageValue()
    }
    data = [
        { label: "Select Country ", value: 1 },
    ];
    handlePicker = data => {
        console.log(data)
        this.setState({ pickedCountry: data });
        if (this.props.usercountry1[0].country_id != data.country_id) {
            this.setState({ buttonText: 'Continue' })
        }
        else
            this.setState({ buttonText: 'Next' })
        console.log(data)
        this.setState({ country_id: data.value })
    };
    async _getStorageValue(){
        var value = await AsyncStorage.getItem('token')
        var profile_pic = await AsyncStorage.getItem('profile_pic')
        this.state.profile_pic = profile_pic
        if (value == null) {
            console.log(value)
            this.props.navigation.replace('Login')
        }
        else {
            this.setState({ token: value })
            this.props.country(value).then(async () => {
                if (this.props.profile_error == "Invalid Token") {
                    await AsyncStorage.setItem('token', "")
                    this.props.navigation.replace('Login')
                }
                var country = []
                country = this.props.country1.map((obj) => {
                    var obj1 = {
                        label: obj.country_name,
                        value: obj.country_id
                    }
                    return obj1
                    // console.log(obj1)
                })
                this.data = country
                this.setState({ country: this.props.country1 })
            })
            this.props.usercountry(value).then(async () => {
                if (this.props.profile_error == "Invalid Token") {
                    await AsyncStorage.setItem('token', "")
                    this.props.navigation.replace('Login')
                }
                this.setState({ usercountry: this.props.usercountry1[0].country_id, selectedValue1: this.props.usercountry1[0].country_id, country_id: this.props.usercountry1.country_id, pickedCountry : {"label": this.props.usercountry1[0].country_name, "value": this.props.usercountry1[0].country_id} })
                console.log(this.state)
            })
            this.setState({ token: value })
        }
        return value
    }
    changeCountry(itemValue) {
        this.setState({ selectedValue1: itemValue, country_id: itemValue })
        if (this.props.usercountry1[0].country_id != itemValue) {
            this.setState({ buttonText: 'Continue' })
        }
        else
            this.setState({ buttonText: 'Next' })
    }
    submitCountry() {
        this.setState({ loadingApp: true })
        if (this.state.buttonText == 'Next') {
            
            this.props.navigation.replace('EditState', { country_id: this.state.pickedCountry.value });
        }
        else
            this.props.editCountry(this.state).then(async () => {
                if (this.props.editCountry_error == "Invalid Token") {
                    await AsyncStorage.setItem('token', "")
                    this.props.navigation.replace('Login')
                }
                else
                    this.props.navigation.replace('EditState', { country_id: this.state.pickedCountry.value });
                // this.state.feedbacklist = this.props.feedback1
                // console.log(this.state.feedbacklist)
            })
    }
    loadingRender() {
        if (this.state.loadingApp) {
            return (
                <LoadingIcon />
            )
        }
    }
    render() {
        console.log(this.state)
        return (
            <View style={styles.container}>
                <View style={{ zIndex: 5, }}>
                    {this.loadingRender()}
                    <View style={styles.upperBar}>
                        <View style={styles.backIconContainer}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                                <Image source={Images.backIcon} style={styles.barMenuIcon} />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.headingContainer}>
                            <Text style={styles.barText}>Country</Text>
                        </View>
                        <View>
                            <TouchableOpacity onPress={() =>
                                this.props.navigation.navigate('Profile')}>
                                <Image source={{ uri: 'https://netahub.com/images/user/' + this.state.profile_pic }} style={styles.profileIcon} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <KeyboardAvoidingView behavior="padding">
                        <ScrollView
                            contentContainerStyle={styles.scrollWrapper}
                            showsVerticalScrollIndicator={false}>
                            <View style={styles.wrapper}>
                                <View style={styles.header}>
                                    <Image source={Images.netaHubLogo} style={{ width: width * 0.35, height: height * 0.19, borderRadius: 111 }} />
                                </View>
                            </View>
                            <View style={styles.loginArea}>
                                {/* <InputField
                                name={'Feedback'}
                                //   errorMessage={this.state.loginMsg}
                                placeholder={'Write your Feedback'}
                            /> */}
                                {/* <View style={styles.formShortBox2}>
                                    
                                    <Picker
                                        selectedValue={this.state.selectedValue1}
                                        style={styles.picker1}
                                        onValueChange={(itemValue, itemIndex) => this.changeCountry(itemValue)}>
                                        <Picker.Item label="Country" value="0" />
                                        {this.state.country.map(nom => <Picker.Item key={nom.country_id} label={nom.country_name} value={nom.country_id} />)}
                                    </Picker>
                                </View> */}
                                <View style={styles.pickerBox}>
                                    <Text style={{ fontSize: 16, marginLeft: 5 }}>Country :</Text>
                                    <Picker
                                        onItemChange={this.handlePicker}
                                        style={styles.picker}
                                        items={this.data}
                                        placeholder="Select Country"
                                        placeholderStyle={{ color: '#000' }}
                                        item={this.state.pickedCountry}
                                    // backdropAnimation={{ opactity: 0 }}
                                    //androidPickerMode="dropdown"
                                    //isNullable
                                    //disable
                                    />
                                    <Image source={Images.dropDown} style={styles.dropdown} />
                                </View>
                                <View style={{ flexDirection: 'row', marginTop: '10%', }}>
                                    <TouchableOpacity
                                        activeOpacity={1}
                                        style={{ flex: 1, borderRadius: 20, width: '60%', justifyContent: 'center', alignItems: 'center', marginBottom: -45, marginLeft: 15 }}
                                        onPress={() => this.props.navigation.goBack()}>
                                        <LinearGradient
                                            colors={['#00cfcf', '#00cfef']}
                                            start={{ x: 0.3, y: 0.1 }}
                                            end={{ x: 0.3, y: 1.9 }}
                                            locations={[0.1, 0.6]}
                                            style={styles.boxBtn2}>
                                            <Text
                                                style={styles.btnText}>
                                                Skip </Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        activeOpacity={1}
                                        style={{ flex: 1, borderRadius: 20, width: '60%', justifyContent: 'center', alignItems: 'center', marginBottom: -45, marginRight: 15 }}
                                        onPress={() => {
                                            if (this.state.pincode != '')
                                                this.submitCountry()
                                        }
                                        }>
                                        <LinearGradient
                                            colors={['#00cfcf', '#00cfef']}
                                            start={{ x: 0.3, y: 0.1 }}
                                            end={{ x: 0.3, y: 1.9 }}
                                            locations={[0.1, 0.6]}
                                            style={styles.boxBtn2}>
                                            <Text
                                                style={styles.btnText}>
                                                {this.state.buttonText}
                                            </Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </ScrollView>
                    </KeyboardAvoidingView>
                </View>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    // console.log(state)
    return {
        editCountry1: state.editCountry.editCountry,
        editCountry_error: state.editCountry.editCountry_error,
        country1: state.country.country,
        country_error: state.country.country_error,
        usercountry1: state.usercountry.usercountry,
        usercountry_error: state.usercountry.usercountry_error
    };
};
export default connect(mapStateToProps, {
    editCountry, country, usercountry
})(EditCountry);
