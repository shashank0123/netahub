import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image,  I18nManager
} from "react-native";
const { width, height } = Dimensions.get('window');
import InputField from '../../Commons/input';
import { Images } from '../../utils';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { elections } from '../../redux/actions/elections';
import { forgotPassword } from '../../redux/actions/forgotPassword';
import styles from './style';
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
import LoadingIcon from '../../components/LoadingIcon';
import AwesomeAlert from 'react-native-awesome-alerts';
const translationGetters = {
    // lazy requires (metro bundler does not support symlinks)
    en: () => require("../../assets/en-US.json"),
    hi: () => require("../../assets/hi.json")
};
const translate = memoize(
    (key, config) => i18n.t(key, config),
    (key, config) => (config ? key + JSON.stringify(config) : key)
);
class ForgetPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mobile_no: '',
            loadingApp: false,
            showAlert: false,
            responseMessage: ''
        };
    }
    onSubmit() {
        console.log(this.state)
        this.setState({ loadingApp: true })
        this.props.forgotPassword(this.state.mobile_no).then(async () => {
            console.log(this.props.forgotPassword1, 'yaha to kuch aaya hi nhi')
            if (this.props.forgotPassword1[0] && this.props.forgotPassword1[0].code == 200) {
               
                this.props.navigation.navigate('OtpScreen', { mobile_no: this.state.mobile_no })
            }
            else {
                this.showAlert()
            }

        })
        this.setState({ loadingApp:false })
    }
    showAlert = () => {
        this.setState({
            showAlert: true
        });
    };
    hideAlert = () => {
        this.setState({
            showAlert: false
        });
        this.props.navigation.navigate('Login')
    };
    loadingRender() {
        if (this.state.loadingApp) {
            return (
                <LoadingIcon />
            )
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={{ zIndex: 5, position: 'absolute' }}>
                    {this.loadingRender()}
                    <KeyboardAvoidingView behavior="padding">
                        <ScrollView
                            contentContainerStyle={styles.scrollWrapper}
                            showsVerticalScrollIndicator={false}>
                            <LinearGradient
                                colors={['#00cccc', '#00cced']}
                                start={{ x: 0.3, y: 0.1 }}
                                end={{ x: 0.3, y: 1.9 }}
                                locations={[0.1, 0.6]}
                                style={styles.wrapper}>
                                {/* <View style={styles.wrapper}> */}
                                <View style={styles.header}>
                                    <Image source={Images.netaHubLogo} style={{ width: width * 0.35, height: height * 0.19, borderRadius: 111 }} />
                                </View>
                                {/* </View> */}
                            </LinearGradient>
                            <View style={styles.loginArea}>
                                <InputField
                                    name={'Mobile Number'}
                                    alertMessage={'Please Enter Your Mobile Number'}
                                    //   errorMessage={this.state.loginMsg}
                                    placeholder={' Enter Mobile Number'}
                                    onChangeText={mobile_no => this.setState({ mobile_no })}
                                    value={this.state.mobile_no}
                                />
                                <View style={{ marginBottom: -45, justifyContent: 'center', alignItems: 'center', marginTop: 10, marginBottom: -30 }}>
                                    <LinearGradient
                                        colors={['#00cfcf', '#00cfef']}
                                        start={{ x: 0.3, y: 0.1 }}
                                        end={{ x: 0.3, y: 1.9 }}
                                        locations={[0.1, 0.6]}
                                        style={{ borderRadius: 20 }}>
                                        <TouchableOpacity
                                            activeOpacity={1}
                                            style={styles.boxBtn2}
                                            onPress={() =>
                                                {if (!this.state.loadingApp) this.onSubmit()}
                                            }>
                                            <Text
                                                style={styles.btnText}>
                                                Send OTP
                      </Text>
                                        </TouchableOpacity>
                                    </LinearGradient>
                                </View>
                                <AwesomeAlert
                                    show={this.state.showAlert}
                                    showProgress={false}
                                    title={"NETAHUB"}
                                    titleStyle={{ fontSize: 14, color: '#000', fontWeight: "bold" }}
                                    message="Mobile Number is Not Registered"
                                    closeOnTouchOutside={true}
                                    closeOnHardwareBackPress={false}
                                    //showCancelButton={true}
                                    showConfirmButton={true}
                                    confirmText="Okay"
                                    confirmButtonColor="#00cced"
                                    onConfirmPressed={() => {
                                        this.hideAlert();
                                    }}
                                />
                            </View>
                            <View style={{ marginTop: 50 }}>
                                <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}
                                    onPress={() =>
                                        this.props.navigation.navigate('Ragistration')
                                    }>
                                    <Text>
                                        <Text> Dont Have an Account?</Text>
                                        <Text style={{ fontWeight: 'bold' }}> Create Now</Text>
                                    </Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}
                                    onPress={() =>
                                        this.props.navigation.navigate('Login')
                                    }>
                                    <Text style={styles.forgetText}>
                                        <Text > Login</Text>
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </KeyboardAvoidingView>
                </View>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    // console.log(state)
    return {
        forgotPassword1: state.forgotPassword.forgotPassword,
        forgotPassword_error: state.forgotPassword.forgotPassword_error
    };
};
export default connect(mapStateToProps, {
    forgotPassword
})(ForgetPassword);
