import {StyleSheet, Dimensions} from 'react-native';
const {width, height} = Dimensions.get('window');
export default StyleSheet.create({
  container: {
    width,
    height,
    // display: 'flex',
    // marginBottom:"10%"
  },
  upperBar: {
    width,
    height: height * 0.075,
    backgroundColor: '#00b8e6',
    flexDirection: 'row',
    alignItems: 'center',
  },
  // scrollWrapper: {
  //     // height:height*11,
  //     // flex:1,
  //     // display: 'flex',
  //   },
  scrollView: {
    
    width: '100%',
    alignSelf: 'center',
  },
  contentContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'lightgrey',
    paddingBottom: 50,
  },
  backIconContainer: {},
  headingContainer: {
    width: '50%',
    marginLeft: '10%',
    alignItems: 'center',
  },
  barText: {
    color: '#000000',
    fontSize: height * 0.03,
    fontWeight: 'bold',
  },
  barMenuIcon: {
    height: height * 0.04,
    width: height * 0.04,
    marginLeft: width * 0.04,
  },
});
