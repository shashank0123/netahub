import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Dimensions,
  Image,
  Picker,
  FlatList,
  I18nManager,
  SafeAreaView,
} from 'react-native';
const {width, height} = Dimensions.get('window');
const screenWidth = Dimensions.get('window').width;
import {Images} from '../../utils';
import styles from './style';
import {WebView} from 'react-native-webview';
const scalesPageToFit = Platform.OS === 'android';
class TermsAndConditions extends Component {
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.upperBar}>
          <View style={styles.backIconContainer}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Image source={Images.backIcon} style={styles.barMenuIcon} />
            </TouchableOpacity>
          </View>
          <View style={styles.headingContainer}>
            <Text style={styles.barText}>T&C</Text>
          </View>
        </View>
        <WebView
          scalesPageToFit={scalesPageToFit}
          bounces={false}
          scrollEnabled={false}
          source={{uri:'https://netahub.com/terms-and-conditions/'}}
          style={{backgroundColor: '#fff', height}}
        />
      </SafeAreaView>
    );
  }
}
export default TermsAndConditions;
