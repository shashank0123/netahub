import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image, FlatList, I18nManager, ActivityIndicator
} from "react-native";
const { width, height } = Dimensions.get('window');
import InputField from '../../Commons/input';
import { Images } from '../../utils'
import styles from './style';
// import { Picker } from '@react-native-community/picker';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { editState } from '../../redux/actions/editState';
import { statelist } from '../../redux/actions/statelist';
import { userstate } from '../../redux/actions/userstate';
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
import { Picker } from 'react-native-woodpicker';
const LoadingIcon = ({ loadingApp }) => <ActivityIndicator
    style={{ zIndex: 5, position: 'absolute', alignItems: 'center', top: '63%', left: '45%', }} size="large" color="#00b8e6" animating={loadingApp} ></ActivityIndicator>;
const translationGetters = {
    // lazy requires (metro bundler does not support symlinks)
    en: () => require("../../assets/en-US.json"),
    hi: () => require("../../assets/hi.json")
};
const translate = memoize(
    (key, config) => i18n.t(key, config),
    (key, config) => (config ? key + JSON.stringify(config) : key)
);
class EditState extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedValue: '',
            selectedValue1: '',
            country_id: this.props.route.params.country_id,
            statelist: [],
            profile_pic: '',
            buttonText: 'Next',
            loadingApp: false,
            pickedState: '',
        }
        console.log(this.state)
        this._getStorageValue()
    }
    data = [
        { label: "Select State ", value: 1 },
    ];
    handlePicker = data => {
        this.setState({ pickedState: data });
        if (this.props.userstate1[0].state_id != data.state_id) {
            this.setState({ buttonText: 'Continue' })
        }
        else
            this.setState({ buttonText: 'Next' })
        this.setState({ state_id: data.value })
    };
    async _getStorageValue() {
        var value = await AsyncStorage.getItem('token')
        var profile_pic = await AsyncStorage.getItem('profile_pic')
        this.state.profile_pic = profile_pic
        if (value == null) {
            console.log(value)
            this.props.navigation.replace('Login')
        }
        else {
            this.setState({ token: value })
            this.props.statelist(this.state).then(async () => {
                if (this.props.statelist_error == "Invalid Token") {
                    await AsyncStorage.setItem('token', "")
                    this.props.navigation.replace('Login')
                }
                var statelist = []
                statelist = this.props.statelist1.map((obj) => {
                    var obj1 = {
                        label: obj.state_name,
                        value: obj.state_id
                    }
                    return obj1
                    // console.log(obj1)
                })
                this.data = statelist



                this.setState({ statelist: this.props.statelist1 })
            })
            this.props.userstate(value).then(async () => {
                if (this.props.profile_error == "Invalid Token") {
                    await AsyncStorage.setItem('token', "")
                    this.props.navigation.replace('Login')
                }

                this.setState(
                    { 
                        userstate: this.props.userstate1[0].state_id, 
                        selectedValue1: this.props.userstate1[0].state_id, 
                        state_id: this.props.userstate1.state_id ,  
                        pickedState : 
                        { 
                            "label": this.props.userstate1[0].state_name, 
                            "value": this.props.userstate1[0].state_id 
                        } 
                    }
                )
            })
        }
        return value
    }
    changeState(itemValue) {
        this.setState({ selectedValue1: itemValue, state_id: itemValue })
        if (this.props.userstate1[0].state_id != itemValue) {
            this.setState({ buttonText: 'Continue' })
        }
        else
            this.setState({ buttonText: 'Next' })
    }
    submitState() {
        this.setState({ loadingApp: true })
        if (this.state.buttonText == 'Next') {
            this.props.navigation.replace('EditDistrict', { state_id: this.state.pickedState.value });
        }
        else
            this.props.editState(this.state).then(async () => {
                this.setState({ loadingApp: false })
                if (this.props.editCountry_error == "Invalid Token") {
                    await AsyncStorage.setItem('token', "")
                    this.props.navigation.replace('Login')
                }
                else
                    this.props.navigation.navigate('EditDistrict', { state_id: this.state.pickedState.value });
                // this.state.feedbacklist = this.props.feedback1
                // console.log(this.state.feedbacklist)
            })
    }
    loadingRender() {
        if (this.state.loadingApp) {
            return (
                <LoadingIcon />
            )
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={{ zIndex: 5, }}>
                    {this.loadingRender()}
                    <View style={styles.upperBar}>
                        <View style={styles.backIconContainer}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                                <Image source={Images.backIcon} style={styles.barMenuIcon} />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.headingContainer}>
                            <Text style={styles.barText}>State</Text>
                        </View>
                        <View>
                            <TouchableOpacity onPress={() =>
                                this.props.navigation.navigate('Profile')}>
                                <Image source={{ uri: 'https://netahub.com/images/user/' + this.state.profile_pic }} style={styles.profileIcon} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <KeyboardAvoidingView behavior="padding">
                        <ScrollView
                            contentContainerStyle={styles.scrollWrapper}
                            showsVerticalScrollIndicator={false}>
                            <View style={styles.wrapper}>
                                <View style={styles.header}>
                                    <Image source={Images.netaHubLogo} style={{ width: width * 0.35, height: height * 0.19, borderRadius: 111 }} />
                                </View>
                            </View>
                            <View style={styles.loginArea}>
                                {/* <InputField
                                name={'Feedback'}
                                //   errorMessage={this.state.loginMsg}
                                placeholder={'Write your Feedback'}
                            /> */}
                                {/* <View style={styles.formShortBox2}>
                                    
                                    <Picker
                                        selectedValue={this.state.selectedValue1}
                                        style={styles.picker1}
                                        onValueChange={(itemValue, itemIndex) => this.changeState(itemValue)}>
                                        <Picker.Item label="State" value="Select State" />
                                        {this.state.statelist.map(nom => <Picker.Item key={nom.state_id} label={nom.state_name} value={nom.state_id} />)}
                                    </Picker>
                                </View> */}
                                <View style={styles.pickerBox}>
                                    <Text style={{ fontSize: 16, marginLeft: 5 }}>State :</Text>
                                    <Picker
                                        onItemChange={this.handlePicker}
                                        style={styles.picker}
                                        items={this.data}
                                        placeholder="Select State"
                                        placeholderStyle={{ color: '#000' }}
                                        item={this.state.pickedState}
                                    // backdropAnimation={{ opactity: 0 }}
                                    //androidPickerMode="dropdown"
                                    //isNullable
                                    //disable
                                    />
                                    <Image source={Images.dropDown} style={styles.dropdown} />
                                </View>
                               
                                <View style={{ flexDirection: 'row', marginTop: '10%' }}>
                                    <TouchableOpacity
                                        activeOpacity={1}
                                        style={{ flex: 1, borderRadius: 20, width: '60%', justifyContent: 'center', alignItems: 'center', marginBottom: -45,marginLeft:15 }}
                                        onPress={() => this.props.navigation.goBack()}>
                                        <LinearGradient
                                            colors={['#00cfcf', '#00cfef']}
                                            start={{ x: 0.3, y: 0.1 }}
                                            end={{ x: 0.3, y: 1.9 }}
                                            locations={[0.1, 0.6]}
                                            style={styles.boxBtn2}>
                                            <Text
                                                style={styles.btnText}>
                                                Skip
                      </Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        activeOpacity={1}
                                        style={{ flex: 1, borderRadius: 20, width: '60%', justifyContent: 'center', alignItems: 'center', marginBottom: -45,marginRight:15 }}
                                        onPress={() =>
                                            this.submitState()
                                        }>
                                        <LinearGradient
                                            colors={['#00cfcf', '#00cfef']}
                                            start={{ x: 0.3, y: 0.1 }}
                                            end={{ x: 0.3, y: 1.9 }}
                                            locations={[0.1, 0.6]}
                                            style={styles.boxBtn2}>
                                            <Text
                                                style={styles.btnText}>
                                                {this.state.buttonText}
                                            </Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </ScrollView>
                    </KeyboardAvoidingView>
                </View>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    // console.log(state)
    return {
        editState1: state.editState.editState,
        editState_error: state.editState.editState_error,
        statelist1: state.statelist.statelist,
        statelist_error: state.statelist.statelist_error,
        userstate1: state.userstate.userstate,
        userstate_error: state.userstate.userstate_error
    };
};
export default connect(mapStateToProps, {
    editState, statelist, userstate
})(EditState);
