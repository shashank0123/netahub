import React, { Component, useState } from "react";
import {
	View,
	Text,
	TouchableOpacity,
	KeyboardAvoidingView,
	ScrollView,
	TextInput,
	Dimensions,
	Image, I18nManager,
	Platform, PermissionsAndroid
} from "react-native";
import axios from 'axios'
import Geolocation from '@react-native-community/geolocation';
const { width, height } = Dimensions.get('window');
import InputField from '../../Commons/input';
import { Images } from '../../utils';
import LinearGradient from 'react-native-linear-gradient';

import styles from './style';
import CheckBox from '@react-native-community/checkbox';
import Icon from 'react-native-vector-icons/MaterialIcons'
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { register } from '../../redux/actions/register';
import { login } from '../../redux/actions/login';
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
import LoadingIcon from '../../components/LoadingIcon';
const translationGetters = {
	// lazy requires (metro bundler does not support symlinks)
	en: () => require("../../assets/en-US.json"),
	hi: () => require("../../assets/hi.json")
};
const translate = memoize(
	(key, config) => i18n.t(key, config),
	(key, config) => (config ? key + JSON.stringify(config) : key)
);
const genders = [
	{
		id: 1,
		name: "Male",
	},
	{
		id: 2,
		name: "Female",
	},
	{
		id: 3,
		name: "Other",
	},
]
class Ragisteration extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			option: 0,
			selectedValue: '',
			date: new Date(),
			show_date: false,
			display_name: '',
			user_password: '',
			email_id: '',
			mobile_no: '',
			dateofbirth: '',
			pincode: null,
			pincode2: '',
			qualification: '',
			occupation: '',
			gender: '',
			referred_by: '',
			status: '',
			latitude: 0,
			longitude: 0,
			error: null,
			Address: null,
			button: 1,
			display_name_error: '',
			loadingApp: false,
			checked: ''
		}
		this.requestPermissions()
	}
	async getLocation() {
		console.log('hi')
		Geolocation.getCurrentPosition(
			async (position) => {
				this.setState({
					latitude: position.coords.latitude,
					longitude: position.coords.longitude,
				});


				var url = 'https://us1.locationiq.com/v1/reverse.php?key=pk.074771bbfc0b9340c8c037f569cbde1d&format=json&lat=' + position.coords.latitude + '&lon=' + position.coords.longitude
				// var url = "http://www.mapquestapi.com/geocoding/v1/reverse?key=tLwAB3CQtAjCeieRaR3JkKXRtL6GJac6&location=" + position.coords.latitude + ","+ position.coords.longitude +"&includeRoadMetadata=true&includeNearestIntersection=true"
				console.log(url)
				await axios
					.get(url)
					.then((response) => {

						console.log(response.data.address.postcode);


						this.setState({
							pincode2: response.data.address.postcode,

						})
						console.log(this.state)
					})
					.catch((error) => {
						console.log(error);
					});
			},
			(error) => {
				// See error code charts below.
				this.setState({
					error: error.message
				}),
					console.log(error.code, error.message);
			},
			{ enableHighAccuracy: true, timeout: 20000, maximumAge: 36000000 }
		);
	}

	async componentDidMount() {
		this.setState({ button: 1 })
		this.getLocation()
	}
	async requestPermissions() {
		if (Platform.OS === 'ios') {
			geolocation.requestAuthorization();
			Geolocation.setRNConfiguration({
				skipPermissionRequests: false,
				authorizationLevel: 'whenInUse',
			});
		}
		if (Platform.OS === 'android') {
			const result = await PermissionsAndroid.request(
				PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
				{
					title: 'Permission Explanation',
					message: 'Fastrack would like to access your location for address!',
				},
			);
			if (result !== 'granted') {
				geolocation.requestAuthorization();
				console.log('Access to location was denied');
				return;
			}
		}
	}


	onSubmit() {
		this.setState({
			button: 0, username: this.state.mobile_no, password: this.state.user_password, display_name_error: '',
			mobile_error: '',
			password_error: ''
		})
		console.log(this.state)
		// check if the latitude and longitude is retrieved or not before calling the api.
		if (this.state.checked && this.state.checked != '') {
			if (this.state.display_name != '' && this.state.mobile_no != '' && this.state.user_password != '') {
				this.setState({ loadingApp: true })
				this.props.register(this.state).then(async () => {
					this.props.login(this.state).then(async () => {
						this.setState({ loadingApp: false })
						var value = await AsyncStorage.getItem('token')
						if (value != null) {
							console.log(value)
							this.props.navigation.replace('AppHome')
						}
					})
				})
			}
			else {
				if (this.state.display_name == '') {
					this.setState({ display_name_error: 'Please input your name' })
				}
				if (this.state.mobile_no == '') {
					this.setState({ mobile_error: 'Please input your mobile no' })
				}
				if (this.state.user_password == '') {
					this.setState({ password_error: 'Please set your password' })
				}
			}
		}
		else {
			this.setState({ checkbox_error: 'Please Accept Terms and Conditions' })
		}
		this.setState({ button: 1 })
	}

	loadingRender() {
		if (this.state.loadingApp) {
			return (
				<LoadingIcon />
			)
		}
	}
	render() {
		console.log(this.state.pincode, 'pincode')
		return (
			<View style={styles.container}>
				<View style={{ zIndex: 5, }}>
					{this.loadingRender()}
					<KeyboardAvoidingView behavior="padding">
						<ScrollView
							contentContainerStyle={styles.scrollWrapper}
							showsVerticalScrollIndicator={false}>
							<LinearGradient
								colors={['#00cccc', '#00cced']}
								start={{ x: 0.3, y: 0.1 }}
								end={{ x: 0.3, y: 1.9 }}
								locations={[0.1, 0.6]}
								style={styles.wrapper}>
								{/* <View style={styles.wrapper}> */}
								<View style={styles.header}>
									<Image source={Images.netaHubLogo} style={{ width: width * 0.35, height: height * 0.19, borderRadius: 111 }} />
								</View>
							</LinearGradient>
							{/* </View> */}
							<View style={styles.loginArea}>
								<InputField
									name={'Name'}
									alertMessage={'Please Enter Your Name'}
									errorMessage={this.state.display_name_error}
									onChangeText={display_name => {
										console.log('hi')
										this.setState({ display_name })
									}}
									value={this.state.display_name}
									placeholder={' Name Here'}
								/>
								<InputField
									name={'Email'}
									alertMessage={'Please Enter Your Email'}
									placeholder={' Email Here'}
									onChangeText={email_id => this.setState({ email_id })}
									errorMessage={this.state.loginMsg}
								/>
								<InputField
									name={'Mobile Number'}
									alertMessage={'Please Enter Your Mobile Number'}
									placeholder={' Phone Number Here'}
									onChangeText={mobile_no => this.setState({ mobile_no, user_name: mobile_no })}
									keyboardType={'number-pad'}
									maxLength={10}

									errorMessage={this.state.mobile_error}
								/>
								<InputField
									name={'Password'}
									alertMessage={'Please Enter New Password'}
									placeholder={' ********'}
									onChangeText={user_password => this.setState({ user_password })}
									secureTextEntry={true}
									errorMessage={this.state.password_error}
								/>
								<View style={{width:width*0.8,marginLeft:'3%'}}>
									<Text style={{fontWeight:'bold',marginLeft:10}}>Pincode</Text>
								<TextInput
								style={{paddingLeft:10,marginBottom:20,paddingBottom:20}}
									name={'Pincode'}
									onChangeText={(pincode) => {
										console.log(this.state.pincode)
										this.setState({ pincode })
									}}
									keyboardType={'number-pad'}
									maxLength={6}
									value={this.state.pincode}
									defaultValue={this.state.pincode2}
									underlineColorAndroid='#000'

								/>
								</View>
								<InputField
									name={'Refferred By (optional)'}
									alertMessage={'Please Enter Name of Person who Reffered you'}
									onChangeText={referred_by => this.setState({ referred_by })}
									placeholder={' Refferred By'}
									keyboardType={'number-pad'}
									maxLength={10}
								/>

								<View style={{ flexDirection: 'row', marginLeft: 10, marginBottom: 10, justifyContent: 'flex-start', alignItems: 'flex-start' }}>
									<CheckBox
										value={this.state.checked}
										errorMessage={this.state.checkbox_error}
										tintColors={{ true: '#00cfef', false: '#A0A0A0' }}
										onValueChange={() => this.setState({ checked: !this.state.checked })}

									/>
									<Text style={{ marginTop: 8, fontWeight: 'bold' }}> I Accept Terms & Conditions</Text>

								</View>
								<Text style={{ marginTop: 8, fontWeight: 'bold', color: 'red' }}> {this.state.checkbox_error}</Text>
								<View style={{ justifyContent: 'center', alignItems: 'center' }}>

									{this.state.button ?
										<TouchableOpacity
											activeOpacity={1}
											style={{ flex: 1, borderRadius: 20, width: '60%', justifyContent: 'center', alignItems: 'center', marginTop: 10, marginBottom: -30 }}
											onPress={() =>
												this.onSubmit()
											}>
											<LinearGradient
												colors={['#00cfcf', '#00cfef']}
												start={{ x: 0.3, y: 0.1 }}
												end={{ x: 0.3, y: 1.9 }}
												locations={[0.1, 0.6]}
												style={styles.boxBtn2}>
												<Text
													style={styles.btnText}>
													Next
                      </Text>
											</LinearGradient>
										</TouchableOpacity>
										:

										<TouchableOpacity
											activeOpacity={1}
											style={{ flex: 1, borderRadius: 20, width: '60%', justifyContent: 'center', alignItems: 'center', marginTop: 30, marginBottom: -70 }}
											onPress={() =>
												console.log(this.state)
											}>
											<LinearGradient
												colors={['#00cfcf', '#00cfef']}
												start={{ x: 0.3, y: 0.1 }}
												end={{ x: 0.3, y: 1.9 }}
												locations={[0.1, 0.6]}
												style={styles.boxBtn2}>
												<Text style={styles.btnText}>Wait</Text>
											</LinearGradient>
										</TouchableOpacity>
									}
								</View>

							</View>
							<View style={{ marginTop: 20 }}>
								<TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}
									onPress={() =>
										this.props.navigation.navigate('Login')
									}>
									<Text style={styles.dontHaveAccountText}>
										<Text> Already Have an Account?</Text>
										<Text style={{ fontWeight: 'bold' }}> Login</Text>
									</Text>
								</TouchableOpacity>
							</View>


							<View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: '5%', paddingBottom: "10%" }}>
								<TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}
									onPress={() =>
										this.props.navigation.navigate('TermsAndConditions')
									}>
									<Text style={{ fontWeight: 'bold' }}>
										<Text > Terms & Conditions </Text>
									</Text>
								</TouchableOpacity>
								<Text style={{ fontWeight: 'bold' }}>||</Text>
								<TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}
									onPress={() =>
										this.props.navigation.navigate('TermsAndConditions')
									}>
									<Text style={{ fontWeight: 'bold' }}>
										<Text > Privacy Policy </Text>
									</Text>
								</TouchableOpacity>
							</View>



						</ScrollView>
					</KeyboardAvoidingView>
				</View>
			</View>
		);
	}
}
const mapStateToProps = (state) => {
	return {
		register1: state.register.register,
		register_error: state.register.register_error,
		login1: state.login.login,
		login_error: state.login.login_error
	};
};
export default connect(mapStateToProps, {
	register, login
})(Ragisteration);
