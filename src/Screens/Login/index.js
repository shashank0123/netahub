import React, { Component, setState } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image,  I18nManager, Alert 
} from "react-native";
const { width, height } = Dimensions.get('window');
import InputField from '../../Commons/input';
import { Images } from '../../utils'
import LinearGradient from 'react-native-linear-gradient';
import styles from './style';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { login } from '../../redux/actions/login';
import LoadingIcon from '../../components/LoadingIcon';
import messaging from '@react-native-firebase/messaging';
import { fcmService } from './../../utils/FCMService';
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
const translationGetters = {
    // lazy requires (metro bundler does not support symlinks)
    en: () => require("../../assets/en-US.json"),
    hi: () => require("../../assets/hi.json")
};
const translate = memoize(
    (key, config) => i18n.t(key, config),
    (key, config) => (config ? key + JSON.stringify(config) : key)
);
class Login extends Component {


    state = { hidePassword: true, username: '', password: '', device_token: "", imei: "", loadingApp: false }
    constructor(props) {
        super(props);
        this.state = {
            hidePassword: true,
            username: '', password: '', device_token: "", imei: "",
      mobile_os: ''
        }
        fcmService.registerAppWithFCM()
        this._getStorageValue();
    }

    submitLogin() {
       
        messaging().getToken()
      .then(fcmToken => {
        if (fcmToken) {
          this.state.device_token = fcmToken
          console.log(this.state.device_token)
        }

      })
      .catch(error => {
        console.log("[FCMService] getToken rejected", error)
      })
      if (this.state.device_token != null) {
           this.setState({ loadingApp: true })
        this.props.login(this.state).then(async () => {
            this.setState({ loadingApp: false })
            var value = await AsyncStorage.getItem('token')
            var fcm = await AsyncStorage.getItem('fcmToken')
    var mobile_os = await AsyncStorage.getItem('mobile_os')
    this.state.device_token = fcm
    this.state.mobile_os = mobile_os
            await AsyncStorage.setItem('updatestatus', '1')
            if (value != null) {
                console.log(value)
                this.props.navigation.replace('AppHome')
            }
            else{
                
                // Alert.alert("Please check your credentials")
                Alert.alert(
                    'NetaHub',
                    'Please check your credentials',
                  
                   
                  )
            }
        })
    }
    }

    
    loadingRender() {
        if (this.state.loadingApp) {
            return (
                <LoadingIcon />
            )
        }
    }
    async _getStorageValue() {
        var value = await AsyncStorage.getItem('token')
        var fcm = await AsyncStorage.getItem('fcmToken')
        await AsyncStorage.setItem('updatestatus', '1')
        this.setState({ device_token: fcm })
        if (value != null) {
            console.log(value)
            this.props.navigation.replace('AppHome')
        }
        return value
    }
    render() {
        return (
            <View style={styles.container}>
                {/* <View style={{ zIndex: 5, position: 'absolute' }}> */}
                    {this.loadingRender()}
                    <KeyboardAvoidingView behavior="padding">
                        <ScrollView
                            contentContainerStyle={styles.scrollWrapper}
                            showsVerticalScrollIndicator={false}>
                            <LinearGradient
                                colors={['#00cccc', '#00cced']}
                                start={{ x: 0.3, y: 0.1 }}
                                end={{ x: 0.3, y: 1.9 }}
                                locations={[0.1, 0.6]}
                                style={styles.wrapper}>
                                {/* <View style={styles.wrapper}> */}
                                <View style={styles.header}>
                                    <Image source={Images.netaHubLogo} style={{ width: width * 0.35, height: height * 0.19, borderRadius: 111 }} />
                                </View>
                                {/* </View> */}
                            </LinearGradient>
                            <View style={styles.loginArea}>
                                <InputField
                                    name={'Mobile Number'}
                                    alertMessage={'Please Enter Your Mobile Number'}
                                    //   errorMessage={this.state.loginMsg}
                                    placeholder={' Enter Mobile Number'}
                                    onChangeText={username => this.setState({ username })}
                                    value={this.state.username}
                                />
                                <InputField
                                    name={'Password'}
                                    alertMessage={'Please Enter Your Password'}
                                    placeholder={' Enter Password'}
                                    onChangeText={password => this.setState({ password })}
                                    secureTextEntry={true}
                                    value={this.state.password}
                                //   errorMessage={this.state.loginMsg}
                                />
                                <TouchableOpacity
                                    activeOpacity={1}
                                    style={{ flex: 1, borderRadius: 20, width: '80%', justifyContent: 'center', alignItems: 'center', marginLeft: '10%', marginTop: 10, marginBottom: -30 }}
                                    onPress={() => this.submitLogin()}>
                                    <LinearGradient
                                        colors={['#00cfcf', '#00cfef']}
                                        start={{ x: 0.3, y: 0.1 }}
                                        end={{ x: 0.3, y: 1.9 }}
                                        locations={[0.1, 0.6]}
                                        style={styles.boxBtn2}>
                                        <Text
                                            style={styles.btnText}>
                                            Login
                      </Text>
                                    </LinearGradient>
                                </TouchableOpacity>
                            </View>
                            <View style={{ marginTop:15 }}>
                                <Text style={{ fontSize: 12, marginTop: 5, textAlign: 'center' }}>SignUp with</Text>
                                <View style={{ flexDirection: 'row', paddingTop: 10, alignContent: 'center', justifyContent: 'center' }}>
                                    <TouchableOpacity onPress={() =>
                                        this.props.navigation.navigate('Profile')}>
                                        <Image source={Images.googleLogo} style={styles.googleIcon} />
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={{ marginLeft: '5%' }}
                                        onPress={() => this.props.navigation.navigate('Profile')}>
                                        <Image source={Images.fbLogo} style={styles.fbIcon} />
                                    </TouchableOpacity>
                                </View>
                                <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', marginTop: 20 }}
                                    onPress={() =>
                                        this.props.navigation.navigate('Ragistration')
                                    }>
                                    <Text>
                                        <Text> Don't Have an Account?</Text>
                                        <Text style={{ fontWeight: 'bold' }}> Create Now</Text>
                                    </Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}
                                    onPress={() =>
                                        this.props.navigation.navigate('ForgetPassword')
                                    }>
                                    <Text style={styles.forgetText}>
                                        <Text > Forget Password</Text>
                                    </Text>
                                </TouchableOpacity>
                                <View style={{flexDirection:'row',justifyContent:'center',marginTop:'2%'}}>
                                <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}
                                    onPress={() =>
                                        this.props.navigation.navigate('TermsAndConditions')
                                    }>
                                    <Text style={styles.forgetText}>
                                        <Text > Terms & Conditions </Text>
                                    </Text>
                                </TouchableOpacity>
                                <Text style={{marginTop:5,fontWeight:'bold'}}>||</Text>
                                <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}
                                    onPress={() =>
                                        this.props.navigation.navigate('PrivacyPolicy')
                                    }>
                                    <Text style={styles.forgetText}>
                                        <Text > Privacy Policy </Text>
                                    </Text>
                                </TouchableOpacity>
                                </View>

                            </View>
                        </ScrollView>
                    </KeyboardAvoidingView>
                {/* </View> */}
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        login1: state.login.login
    };
};
export default connect(mapStateToProps, {
    login
})(Login);
