import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image,  FlatList, I18nManager
} from "react-native";
const { width, height } = Dimensions.get('window');

import ImagePicker from 'react-native-image-picker';
import { Images } from '../../utils'
import styles from './style';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { nomineeDetails } from '../../redux/actions/nomineeDetails';
import { selfNominate } from '../../redux/actions/selfNominate';
import { castVoteSave } from '../../redux/actions/castVoteSave';
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";

const translationGetters = {
    // lazy requires (metro bundler does not support symlinks)
    en: () => require("../../assets/en-US.json"),
    hi: () => require("../../assets/hi.json")
};
const translate = memoize(
    (key, config) => i18n.t(key, config),
    (key, config) => (config ? key + JSON.stringify(config) : key)
);
class NomineeDetails extends Component {
    constructor(props) {
        super(props)
        this.state = {
            nominee: this.props.route.params.nominee,
			cons_id : this.props.route.params.nominee.cons_id,
			survey_id : this.props.route.params.nominee.survey_id,
			nominee_name : this.props.route.params.nominee.nominee_name,
            profile_pic: '',
            imageuri: '',
        }
        this._getStorageValue()
    }
    
    async _getStorageValue() {
        var value = await AsyncStorage.getItem('token')
        var profile_pic = await AsyncStorage.getItem('profile_pic')
        this.setState({profile_pic})
        if (value == null) {
            console.log(value)
            this.props.navigation.replace('Login')
        }
        else {
            this.state.nominee.token = value
            this.state.token = value
			
        }
        return value
    }
    submitVote() {
        this.props.castVoteSave(this.state.nominee).then(async () => {
            if (this.props.survey_error == "Invalid Token") {
                await AsyncStorage.setItem('token', "")
                this.props.navigation.replace('Login')
            }
            this.state.nominee.voted = 1
            this.props.navigation.navigate('CastVotes', {election_id: this.state.nominee.election_id,
                survey_id: this.state.nominee.survey_id,
                cons_id: this.state.nominee.cons_id,})
        })
    }
    state = {
        avatarSource: null,
    }
    selectImage = async () => {
        let options = {
            title: 'You can choose one image',
            maxWidth: 256,
            maxHeight: 256,
            quality: 0.5,
            base64: true,
            storageOptions: {
                skipBackup: true
            }
        };
        ImagePicker.showImagePicker(options, (response) => {
            console.log({ response });
			this.state.image = response.data
			this.props.selfNominate(this.state).then(async () => {
            if (this.props.survey_error == "Invalid Token") {
                await AsyncStorage.setItem('token', "")
                this.props.navigation.replace('Login')
            }
            // this.props.navigation.replace('AppHome')
        })
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                this.setState({
                    avatarSource: response.uri,
                });
            }
        });
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.upperBar}>
                    <View style={styles.backIconContainer}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image source={Images.backIcon} style={styles.barMenuIcon} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.headingContainer}>
                        <Text style={styles.barText}>Details</Text>
                    </View>
                    <View>
                        <TouchableOpacity onPress={() =>
                            this.props.navigation.navigate('Profile')}>
                            <Image source={{ uri: 'https://netahub.com/images/user/' + this.state.profile_pic }} style={styles.profileIcon} />
                        </TouchableOpacity>
                    </View>
                </View>
                
                       
                        <LinearGradient
                            colors={['#00cccc', '#00cced']}
                            start={{ x: 0.16, y: 0.1 }}
                            end={{ x: 1.1, y: 1.1 }}
                            locations={[0.16, 50]}
                            style={styles.upperCont}>
                                 <TouchableOpacity onPress={this.selectImage}>
                                <View style={styles.profileImgWrapper}>
                                    <Image 
                                    //   source={(this.state.nominee.photo_path!= '') ? { uri: 'https://netahub.com/images/user/' + this.state.nominee.photo_path } : Images.palceholderImage}   style={{ height: 90, width: 90, borderRadius: 50,  }}
                                    source={this.state.avatarSource ? { uri: this.state.avatarSource } : Images.palceholderImage} style={{ height: 90, width: 90, borderRadius: 50 }} 
                                   />
                                  
                                </View>
                            </TouchableOpacity>
                            
                        </LinearGradient>
                    
                        <View style={{justifyContent:'center',alignItems:'center'}}>
                         <View style={styles.docDetailedWrapper2}>
                            <View
                                style={[styles.docSpecsWrapper, { marginTop: 20 }]}>
                                <View style={styles.docNameWrapper}>
                                    <Text style={styles.docNameText}>
                                        {this.state.nominee.nominee_name}
                                    </Text>
                                    <Text style={styles.docSubNameText}>
                                        About me:
                                        </Text>
                                    <Text  style={styles.abtSubNameText}>
                                        {this.state.nominee.user_information}
                                    </Text>
                                </View>
                            </View>
                            {this.state.nominee.voted == 1 ? <TouchableOpacity
                                activeOpacity={1}
                                style={[styles.boxNominate1, { backgroundColor: 'green' }]}
                            // onPress={() =>
                            //     this.props.navigation.navigate('CastVote')}
                            >
                                <Text style={{ fontWeight: "bold", fontSize: 18 }}>Voted</Text>
                            </TouchableOpacity> : <TouchableOpacity
                                activeOpacity={1}
                                style={styles.boxNominate1}
                                onPress={() =>
                                    this.submitVote()}
                            >
                                    <Text style={{ fontWeight: "bold", fontSize: 18 }}>Vote</Text>
                                </TouchableOpacity>}
                        </View>
                        </View>
                    {/* </ScrollView> */}
                {/* </KeyboardAvoidingView> */}
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    // console.log(state)
    return {
        nomineeDetails1: state.nomineeDetails.nomineeDetails,
        nomineeDetails_error: state.nomineeDetails.nomineeDetails_error,
        castVoteSave1: state.castVoteSave.castVoteSave,
        castVoteSave_error: state.castVoteSave.castVoteSave_error,
		selfNominate1: state.selfNominate.selfNominate,
        selfNominate_error: state.selfNominate.selfNominate_error
    };
};
export default connect(mapStateToProps, {
    nomineeDetails, castVoteSave, selfNominate
})(NomineeDetails);
``