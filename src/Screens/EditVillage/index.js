import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image, FlatList, I18nManager, ActivityIndicator
} from "react-native";
const { width, height } = Dimensions.get('window');
import InputField from '../../Commons/input';
import { Images } from '../../utils'
import styles from './style';
// import { Picker } from '@react-native-community/picker';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { editVillage } from '../../redux/actions/editVillage';
import { villagelist } from '../../redux/actions/villagelist';
import { uservillage } from '../../redux/actions/uservillage';
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
import { Picker } from 'react-native-woodpicker';

const LoadingIcon = ({ loadingApp }) => <ActivityIndicator
    style={{ zIndex: 5, position: 'absolute', alignItems: 'center', top: '63%', left: '45%', }} size="large" color="#00b8e6" animating={loadingApp} ></ActivityIndicator>;
const translationGetters = {
    // lazy requires (metro bundler does not support symlinks)
    en: () => require("../../assets/en-US.json"),
    hi: () => require("../../assets/hi.json")
};

const translate = memoize(
    (key, config) => i18n.t(key, config),
    (key, config) => (config ? key + JSON.stringify(config) : key)
);


class EditVillage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedValue: '',
            selectedValue1: '',
            profile_pic: '',
            buttonText: 'Next',
            villagelist: [],
            block_id: this.props.route.params.block_id,
            pincode: '',
            village_id: 0,
            loadingApp: false,
            pickedVillage: '',

        }
        this._getStorageValue()
    }
    data = [
        { label: "Select Village ", value: 1 },
    ];
    handlePicker = data => {
        this.setState({ pickedVillage: data });
        if (typeof this.props.uservillage1[0] != 'undefined') {
            if (this.props.uservillage1[0].village_id != data.village_id) {
                this.setState({ buttonText: 'Continue' })
            }
            else
                this.setState({ buttonText: 'Next' })
            this.setState({ village_id: data.value })
        }
    };
    async _getStorageValue() {

        var value = await AsyncStorage.getItem('token')
        var profile_pic = await AsyncStorage.getItem('profile_pic')
        this.state.profile_pic = profile_pic
        if (value == null) {
            this.props.navigation.replace('Login')
        }
        else {
            this.setState({ 'token': value })
            this.props.villagelist(this.state).then(async () => {
                if (this.props.profile_error == "Invalid Token") {
                    await AsyncStorage.setItem('token', "")
                    this.props.navigation.replace('Login')
                }
                var villagelist = []
                villagelist = this.props.villagelist1.map((obj) => {
                    var obj1 = {
                        label: obj.village_name,
                        value: obj.village_id
                    }
                    return obj1
                    // console.log(obj1)
                })
                this.data = villagelist
                this.setState({ villagelist: this.props.villagelist1 })
            })
            this.props.uservillage(value).then(async () => {
                if (this.props.profile_error == "Invalid Token") {
                    await AsyncStorage.setItem('token', "")
                    this.props.navigation.replace('Login')
                }
                this.setState({
                    uservillage: this.props.uservillage1[0].village_id, selectedValue1: this.props.uservillage1[0].village_id, village_id: this.props.uservillage1.village_id,
                    pickedVillage:
                    {
                        "label": this.props.uservillage1[0].village_name,
                        "value": this.props.uservillage1[0].village_id
                    }
                })
            })

        }
        return value
    }

    changeVillage(itemValue) {
        this.setState({ selectedValue1: itemValue, village_id: itemValue })
        console.log(this.state.selectedValue1)
        if (this.props.uservillage1)
            if (this.props.uservillage1[0].village_id != itemValue) {
                this.setState({ buttonText: 'Continue' })
            }
            else
                this.setState({ buttonText: 'Next' })
    }

    renderForm() {

        if (this.state.selectedValue1 == '' || this.state.selectedValue1 == 0)
            return (<><View style={{ marginTop: 15 }}>
                <InputField
                    name={'Village Name'}
                    alertMessage={'Please Enter Your Village Name'}
                    //   errorMessage={this.state.loginMsg}
                    placeholder={'Enter Village'}
                    onChangeText={
                        village_name => {

                            this.setState({ village_name })
                        }}
                    value={this.state.village_name}
                />
            </View>
                <View >
                    <InputField
                        name={'Pincode'}
                        alertMessage={'Please Enter Your Pincode'}
                        //   errorMessage={this.state.loginMsg}
                        placeholder={'Enter Pincode'}
                        onChangeText={
                            pincode => {

                                this.setState({ pincode })
                            }}
                        value={this.state.pincode}
                    />
                </View></>)
        else {

        }
    }

    submitVillage() {
        this.setState({ loadingApp: true })

        if (this.state.buttonText == 'Next') {
            this.props.navigation.replace('EditWard', { village_id: this.state.pickedVillage.value });
        }
        else
            this.props.editVillage(this.state).then(async () => {
                if (this.props.editVillage_error == "Invalid Token") {
                    await AsyncStorage.setItem('token', "")
                    this.props.navigation.replace('Login')
                }
                else
                    this.props.navigation.replace('EditWard', { village_id: this.state.pickedVillage.value });
            })
    }
    loadingRender() {
        if (this.state.loadingApp) {
            return (
                <LoadingIcon />
            )
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={{ zIndex: 5, }}>
                    {this.loadingRender()}
                    <View style={styles.upperBar}>
                        <View style={styles.backIconContainer}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                                <Image source={Images.backIcon} style={styles.barMenuIcon} />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.headingContainer}>
                            <Text style={styles.barText}>Village</Text>
                        </View>
                        <View>
                            <TouchableOpacity onPress={() =>
                                this.props.navigation.navigate('Profile')}>
                                <Image source={{ uri: 'https://netahub.com/images/user/' + this.state.profile_pic }} style={styles.profileIcon} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <KeyboardAvoidingView behavior="padding">
                        <ScrollView
                            contentContainerStyle={styles.scrollWrapper}
                            showsVerticalScrollIndicator={false}>
                            <View style={styles.wrapper}>
                                <View style={styles.header}>
                                    <Image source={Images.netaHubLogo} style={{ width: width * 0.35, height: height * 0.19, borderRadius: 111 }} />
                                </View>
                            </View>
                            <View style={styles.loginArea}>


                                {/* <View style={styles.formShortBox2}>
                                   
                                    <Picker
                                        selectedValue={this.state.selectedValue1}
                                        style={styles.picker1}
                                        onValueChange={(itemValue, itemIndex) => this.changeVillage(itemValue)}>
                                        <Picker.Item label="Add New" value="0" />
                                        {this.state.villagelist.map(nom => <Picker.Item key={nom.village_id} label={nom.village_name} value={nom.village_id} />)}
                                    </Picker>
                                </View> */}
                                <View style={styles.pickerBox}>
                                    <Text style={{ fontSize: 16, marginLeft: 5 }}>Village :</Text>
                                    <Picker
                                        onItemChange={this.handlePicker}
                                        style={styles.picker}
                                        items={this.data}
                                        placeholder="Select Village"
                                        placeholderStyle={{ color: '#000' }}
                                        item={this.state.pickedVillage}
                                    // backdropAnimation={{ opactity: 0 }}
                                    //androidPickerMode="dropdown"
                                    //isNullable
                                    //disable
                                    />
                                    <Image source={Images.dropDown} style={styles.dropdown} />
                                </View>
                                {this.renderForm()}


                                <View style={{ flexDirection: 'row', marginTop: '2%' }}>
                                    <TouchableOpacity
                                        activeOpacity={1}
                                        style={{ flex: 1, borderRadius: 20, width: '60%', justifyContent: 'center', alignItems: 'center', marginBottom: -45, marginLeft: 15 }}
                                        onPress={() => this.props.navigation.goBack()}>
                                        <LinearGradient
                                            colors={['#00cfcf', '#00cfef']}
                                            start={{ x: 0.3, y: 0.1 }}
                                            end={{ x: 0.3, y: 1.9 }}
                                            locations={[0.1, 0.6]}
                                            style={styles.boxBtn2}>
                                            <Text
                                                style={styles.btnText}>
                                                Skip </Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        activeOpacity={1}
                                        style={{ flex: 1, borderRadius: 20, width: '60%', justifyContent: 'center', alignItems: 'center', marginBottom: -45, marginRight: 15 }}
                                        onPress={() =>
                                            this.submitVillage()
                                        }>
                                        <LinearGradient
                                            colors={['#00cfcf', '#00cfef']}
                                            start={{ x: 0.3, y: 0.1 }}
                                            end={{ x: 0.3, y: 1.9 }}
                                            locations={[0.1, 0.6]}
                                            style={styles.boxBtn2}>
                                            <Text
                                                style={styles.btnText}>
                                                {this.state.buttonText}
                                            </Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </ScrollView>
                    </KeyboardAvoidingView>
                </View>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    // console.log(state)
    return {
        editVillage1: state.editVillage.editVillage,
        editVillage_error: state.editVillage.editVillage_error,
        uservillage1: state.uservillage.uservillage,
        uservillage_error: state.uservillage.uservillage_error,
        villagelist1: state.villagelist.villagelist,
        villagelist_error: state.villagelist.villagelist_error,
    };
};
export default connect(mapStateToProps, {
    editVillage, uservillage, villagelist
})(EditVillage);
