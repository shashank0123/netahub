import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image, FlatList, I18nManager, ActivityIndicator
} from "react-native";
const { width, height } = Dimensions.get('window');
import InputField from '../../Commons/input';
import { Images } from '../../utils'
import styles from './style';
// import { Picker } from '@react-native-community/picker';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { editSociety } from '../../redux/actions/editSociety';
import { societylist } from '../../redux/actions/societylist';
import { usersociety } from '../../redux/actions/usersociety';
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
import AwesomeAlert from 'react-native-awesome-alerts';
import { Picker } from 'react-native-woodpicker';
const LoadingIcon = ({ loadingApp }) => <ActivityIndicator
    style={{ zIndex: 5, position: 'absolute', alignItems: 'center', top: '63%', left: '45%', }} size="large" color="#00b8e6" animating={loadingApp} ></ActivityIndicator>;
const translationGetters = {
    // lazy requires (metro bundler does not support symlinks)
    en: () => require("../../assets/en-US.json"),
    hi: () => require("../../assets/hi.json")
};

const translate = memoize(
    (key, config) => i18n.t(key, config),
    (key, config) => (config ? key + JSON.stringify(config) : key)
);


class EditSociety extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedValue: '',
            selectedValue1: '',
            profile_pic: '',
            showAlert: false,
            district_id: this.props.route.params.district_id,
            societylist: [],
            user_society: '',
            buttonText: 'Next',
            loadingApp: false,
            pickedSociety: '',
        }
        this._getStorageValue()
    }
    data = [
        { label: "Select Society ", value: 1 },
    ];
    handlePicker = data => {
        if (typeof data != 'undefined') {
            this.setState({ pickedSociety: data });
            if (this.props.usersociety1.length > 0)
                if (typeof this.props.usersociety1[0].society_id != 'undefined' && this.props.usersociety1[0].society_id != data.society_id) {
                    this.setState({ buttonText: 'Continue' })
                }
                else
                    this.setState({ buttonText: 'Next' })
            else
                this.setState({ buttonText: 'Next' })
            this.setState({ society_id: data.value })
        }
        else
            this.setState({ pickedSociety: { 'label': '0', 'value': 'select' } })
    };
    async _getStorageValue() {

        var value = await AsyncStorage.getItem('token')
        var profile_pic = await AsyncStorage.getItem('profile_pic')
        this.state.profile_pic = profile_pic
        if (value == null) {
            this.props.navigation.replace('Login')
        }
        else {
            this.setState({ 'token': value })
            this.props.societylist(this.state).then(async () => {
                if (this.props.profile_error == "Invalid Token") {
                    await AsyncStorage.setItem('token', "")
                    this.props.navigation.replace('Login')
                }
                var societylist = []
                societylist = this.props.societylist1.map((obj) => {
                    var obj1 = {
                        label: obj.society_name,
                        value: obj.society_id
                    }
                    return obj1
                    console.log(obj1)
                })
                this.data = societylist

                console.log(this.props.societylist1)
                this.setState({ societylist: this.props.societylist1 })
            })
            this.props.usersociety(value).then(async () => {
                if (this.props.profile_error == "Invalid Token") {
                    await AsyncStorage.setItem('token', "")
                    this.props.navigation.replace('Login')
                }
                else
                    if (typeof this.state.user_society != 'undefined')
                        this.setState({
                            usersociety: this.props.usersociety1[0].society_id, selectedValue1: this.props.usersociety1[0].society_id, society_id: this.props.usersociety.society_id,
                            pickedSociety:
                            {
                                "label": this.props.usersociety1[0].society_name,
                                "value": this.props.usersociety1[0].society_id
                            }
                        })
            })

        }
        return value
    }

    changeSociety(itemValue) {
        this.setState({ selectedValue1: itemValue, society_id: itemValue })
        if (typeof this.state.user_society != 'undefined' && this.state.user_society != itemValue) {
            this.setState({ buttonText: 'Next' })
        }
        else
            this.setState({ buttonText: 'Continue' })
    }

    renderForm() {

        if (this.state.selectedValue1 == '' || this.state.selectedValue1 == 0)
            return (<>
                <View style={{ marginTop: 15 }}>
                    <InputField
                        name={'Society Name'}
                        alertMessage={'Please Enter Your Society Name'}
                        //   errorMessage={this.state.loginMsg}
                        placeholder={'Enter Society'}
                        onChangeText={
                            society_name => {

                                this.setState({ society_name })
                            }}
                        value={this.state.society_name}
                    />
                </View>
            </>)
        else {

        }
    }

    submitSociety() {
        this.setState({ loadingApp: true })
        if (this.state.buttonText == 'Next') {
            this.props.navigation.replace('Profile');
        }
        else
            this.props.editSociety(this.state).then(async () => {
                if (this.props.editSociety_error == "Invalid Token") {
                    await AsyncStorage.setItem('token', "")
                    this.props.navigation.replace('Login')
                }
                else
                    this.props.navigation.replace('Profile');
                this.showAlert()
            })
    }
    loadingRender() {
        if (this.state.loadingApp) {
            return (
                <LoadingIcon />
            )
        }
    }

    showAlert = () => {
        this.setState({
            showAlert: true
        });
    };
    hideAlert = () => {
        this.setState({
            showAlert: false
        });
        this.props.navigation.replace('Profile')
    };
    render() {
        return (
            <View style={styles.container}>
                <View style={{ zIndex: 5, }}>
                    {this.loadingRender()}
                    <View style={styles.upperBar}>
                        <View style={styles.backIconContainer}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                                <Image source={Images.backIcon} style={styles.barMenuIcon} />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.headingContainer}>
                            <Text style={styles.barText}>Society</Text>
                        </View>
                        <View>
                            <TouchableOpacity onPress={() =>
                                this.props.navigation.navigate('Profile')}>
                                <Image source={{ uri: 'https://netahub.com/images/user/' + this.state.profile_pic }} style={styles.profileIcon} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <KeyboardAvoidingView behavior="padding">
                        <ScrollView
                            contentContainerStyle={styles.scrollWrapper}
                            showsVerticalScrollIndicator={false}>
                            <View style={styles.wrapper}>
                                <View style={styles.header}>
                                    <Image source={Images.netaHubLogo} style={{ width: width * 0.35, height: height * 0.19, borderRadius: 111 }} />
                                </View>
                            </View>
                            <View style={styles.loginArea}>

                                {/* <View style={styles.formShortBox2}>
                                
                                <Picker
                                    selectedValue={this.state.selectedValue1}
                                    style={styles.picker1}
                                    onValueChange={(itemValue, itemIndex) => this.changeSociety(itemValue)}>
                                    <Picker.Item label="Add New" value="0" />
                                    {this.state.societylist.map(nom => <Picker.Item key={nom.society_id} label={nom.society_name} value={nom.society_id} />)}
                                </Picker>
                            </View> */}
                                <View style={styles.pickerBox}>
                                    <Text style={{ fontSize: 16, marginLeft: 5 }}>Society :</Text>
                                    {this.data.length > 0 ?
                                        <Picker
                                            onItemChange={this.handlePicker}
                                            style={styles.picker}
                                            items={this.data}
                                            placeholder="Select Society"

                                            placeholderStyle={{ color: '#000' }}
                                            item={this.state.pickedSociety}
                                        // backdropAnimation={{ opactity: 0 }}
                                        //androidPickerMode="dropdown"
                                        //isNullable
                                        //disable
                                        />
                                        : <></>}
                                    <Image source={Images.dropDown} style={styles.dropdown} />
                                </View>






                                {this.renderForm()}

                                <View style={{ flexDirection: 'row', marginTop: '5%' }}>
                                    <TouchableOpacity
                                        activeOpacity={1}
                                        style={{ flex: 1, borderRadius: 20, width: '60%', justifyContent: 'center', alignItems: 'center', marginBottom: -45, marginLeft: 15 }}
                                        onPress={() => this.props.navigation.goBack()}>
                                        <LinearGradient
                                            colors={['#00cfcf', '#00cfef']}
                                            start={{ x: 0.3, y: 0.1 }}
                                            end={{ x: 0.3, y: 1.9 }}
                                            locations={[0.1, 0.6]}
                                            style={styles.boxBtn2}>
                                            <Text
                                                style={styles.btnText}>
                                                Skip </Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        activeOpacity={1}
                                        style={{ flex: 1, borderRadius: 20, width: '60%', justifyContent: 'center', alignItems: 'center', marginBottom: -45, marginRight: 15 }}
                                        onPress={() =>
                                            this.submitSociety()
                                        }>
                                        <LinearGradient
                                            colors={['#00cfcf', '#00cfef']}
                                            start={{ x: 0.3, y: 0.1 }}
                                            end={{ x: 0.3, y: 1.9 }}
                                            locations={[0.1, 0.6]}
                                            style={styles.boxBtn2}>
                                            <Text
                                                style={styles.btnText}>
                                                {this.state.buttonText}
                                            </Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                </View>

                                <AwesomeAlert
                                    show={this.state.showAlert}
                                    showProgress={false}
                                    title={"NETAHUB"}
                                    titleStyle={{ fontSize: 14, color: '#000', fontWeight: "bold" }}
                                    message="Society Changed Succesfully"
                                    closeOnTouchOutside={true}
                                    closeOnHardwareBackPress={false}
                                    //showCancelButton={true}
                                    showConfirmButton={true}
                                    confirmText="Okay"
                                    confirmButtonColor="#00cced"
                                    onConfirmPressed={() => {
                                        this.hideAlert();
                                    }}
                                />
                            </View>
                        </ScrollView>
                    </KeyboardAvoidingView>
                </View>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    // console.log(state)
    return {
        editSociety1: state.editSociety.editSociety,
        editSociety_error: state.editSociety.editSociety_error,
        societylist1: state.societylist.societylist,
        societylist_error: state.societylist.societylist_error,
        usersociety1: state.usersociety.usersociety,
        usersociety_error: state.usersociety.usersociety_error
    };
};
export default connect(mapStateToProps, {
    editSociety, societylist, usersociety
})(EditSociety);
