import React, { Component, setState } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image, FlatList,
I18nManager
} from "react-native";
const { width, height } = Dimensions.get('window');
import InputField from '../../Commons/input';
import { heightToDp, Images } from '../../utils'
import styles from './style';
import { Picker } from 'react-native-woodpicker';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { addConstituency } from '../../redux/actions/addConstituency';
import { unmappedElection } from '../../redux/actions/unmappedElection';
import { unmappedConstituency } from '../../redux/actions/unmappedConstituency';
import { insertUserCons } from '../../redux/actions/insertUserCons';

import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
import AwesomeAlert from 'react-native-awesome-alerts';
const translationGetters = {
  // lazy requires (metro bundler does not support symlinks)
  en: () => require("../../assets/en-US.json"),
  hi: () => require("../../assets/hi.json")
};

const translate = memoize(
  (key, config) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key)
);

class AddConstituency extends Component {
    constructor(props) {
        super(props);
        this.state = {
            token:'',
            selectedValue: '',
            electionName: '',
            selectedValue2: '',
            election_id:'',
            elections:[
    { label: "Select Election ", value: 1 },
   
    
  ],
            constituency:[],
            profile_pic:'',
            location_type:'',
            location_id:'',
            location_name:'',
            callcount : 0,
            showAlert: false,
            ConstName:[],
            cons_id:'',
            pickedElection: '',
            pickedConstituency: '',
        }
         this.elections = [
    { label: "Select Election ", value: 1 },
   
    
  ];
   this.constituency = [
    { label: "Select Constituency ", value: 1 },
    
  ];
    this._getStorageValue();
  }
  
  async _getStorageValue(){
    var value = await AsyncStorage.getItem('token')
    var profile_pic = await AsyncStorage.getItem('profile_pic')
    this.state.profile_pic = profile_pic
    if (value == null){
      console.log(value)
      this.props.navigation.replace('Login')
    }
    else{
      this.props.unmappedElection(value).then(async ()=>{
        if (this.props.unmappedElection_error == "Invalid Token"){
          await AsyncStorage.setItem('token', "")
          this.props.navigation.replace('Login')
        }
        else{
        console.log(this.props.unmappedElection1, 'shahsank')
        var electionlist = []
         electionlist = this.props.unmappedElection1.map((obj) => {
           
          var obj1 = {
            label : obj.election_name,
            value: obj.election_id,
            election_id: obj.election_id,
            location_type: obj.location_type,
            location_name: obj.location_name,
            location_id: obj.location_id,
          }
          return obj1
          
        })
        console.log(electionlist, 'shashank22')
        this.elections = electionlist
        this.setState({elections: electionlist})
        console.log(this.elections, 'shashank231')
      }
      })
      this.setState({token:value})
      // console.log(this.state.elections, 'hi')
  }

    return value
  }

  updateConstituency(data){
      data.token = this.state.token
      this.props.unmappedConstituency(data).then(async ()=>{
        if (this.props.unmappedConstituency_error == "Invalid Token"){
          await AsyncStorage.setItem('token', "")
          this.props.navigation.replace('Login')
        }
        else{
          console.log(this.props.unmappedConstituency1)
            if (this.props.unmappedConstituency1.length > 0){
            var constituencylist = []
           constituencylist = this.props.unmappedConstituency1.map((obj) => {
             
            var obj1 = {
              label : obj.cons_name,
              value: obj.cons_id,
              
            }
            return obj1
            
          })
          console.log(constituencylist, 'shashank22')
          this.constituency = constituencylist
          console.log(this.constituency, 'shashank231')
          this.setState({ConstName : this.props.unmappedConstituency1})
        }
      }
      })
  }
  submitConstituency() {
      this.props.insertUserCons(this.state).then(async ()=>{
        if (this.props.insertUserCons_error == "Invalid Token"){
          await AsyncStorage.setItem('token', "")
          this.props.navigation.replace('Login')
        }
        this.setState({ConstName : this.props.insertUserCons1})
      })
    this.showAlert()

}
showAlert = () => {
    this.setState({
        showAlert: true
    });
};

hideAlert = () => {
    this.setState({
        showAlert: false
    });
    this.props.navigation.replace('MyConstituency')
};



  handleElectionChange = electionItem => {
    this.setState({ pickedElection: electionItem });
    this.setState({election_id: electionItem.value})
    this.updateConstituency(electionItem)
    this.setState({location_name : electionItem.location_name})
    this.setState({election_id : electionItem.election_id})
    // if (this.state.elections[itemIndex-1]){
    // this.setState({ 
    //                 electionName: itemValue, 
    //                 election_id:itemValue, 
    //                 location_name: 
    //                 this.state.elections[itemIndex-1].location_name, 
    //                 location_type: this.state.elections[itemIndex-1].location_type, 
    //                 location_id: this.state.elections[itemIndex-1].location_id });
    //                 console.log(itemIndex, itemValue);
    //                 this.updateConstituency(this.state.elections[itemIndex-1])
    //               }}
  };
  handleContituencyChange = constituencyItem => {
    
    this.setState({ pickedConstituency: constituencyItem });
    this.setState({ cons_id: constituencyItem.value});
    this.state.cons_id=constituencyItem.value
    
    
  };



    render() {
      
      this.elections = this.state.elections
        return (
            <View style={styles.container}>
                <View style={styles.upperBar}>
                    <View style={styles.backIconContainer}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image source={Images.backIcon} style={styles.barMenuIcon} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.headingContainer}>
                        <Text style={styles.barText}>{translate('add_constituency')}</Text>
                    </View>
                    <View>
                        <TouchableOpacity onPress={() =>
                            this.props.navigation.navigate('Profile')}>
                            <Image source={{ uri: 'https://netahub.com/images/user/' + this.state.profile_pic }} style={styles.profileIcon} />
                        </TouchableOpacity>
                    </View>
                </View>
                <KeyboardAvoidingView behavior="padding">
                    <ScrollView
                        contentContainerStyle={styles.scrollWrapper}
                        showsVerticalScrollIndicator={false}>
                        <View style={styles.wrapper}>
                            <View style={styles.header}>
                                <Image source={Images.netaHubLogo} style={{ width: width * 0.35, height: height * 0.19, borderRadius: 111 }} />
                            </View>
                        </View>
                        <View style={styles.loginArea}>
                            <View style={{
                                flexDirection: 'row', justifyContent: 'space-between', marginLeft: '5%', marginRight: '5%', borderBottomColor: '#808080',
                                borderBottomWidth: 1, marginTop: 25,
                            }}>
                                <Text style={{ fontSize: 16, color: '#5e6063', marginTop: 10, paddingBottom: 15 }}>{translate('election_name')}:</Text>
                                <View style={styles.formShortBox2}>
                                   
                                    <Picker
                                        onItemChange={this.handleElectionChange}
                                        style={styles.picker}
                                        items={this.elections}
                                        placeholder="Select Election"
                                       
                                        placeholderStyle={{ color: '#000' }}
                                        item={this.state.pickedElection}
                                      // backdropAnimation={{ opactity: 0 }}
                                      //androidPickerMode="dropdown"
                                      //isNullable
                                      //disable
                                      />
                                </View>
                            </View>
                            <View style={{
                                flexDirection: 'row', marginLeft: '5%', marginRight: '5%', borderBottomColor: '#808080',
                                borderBottomWidth: 1, marginTop: 20
                            }}>
                                <Text style={{ fontSize: 16, color: '#5e6063', marginTop: 5 }}>{translate('location')}:</Text>
                                <Text style={{ fontSize: 16, color: '#5e6063', marginTop: 5, marginBottom: 15, marginLeft: '24%' }}>{this.state.location_name}</Text>
                            </View>
                            <View style={{
                                flexDirection: 'row', justifyContent: 'space-between', marginLeft: '5%', marginRight: '5%', borderBottomColor: '#808080',
                                borderBottomWidth: 1, marginTop: 25,
                            }}>
                                <Text style={{ fontSize: 16, color: '#5e6063', marginTop: 10, paddingBottom: 15 }}>{translate('constituency')}:</Text>
                                <View style={styles.formShortBox2}>
                                    
                                    <Picker
                                        onItemChange={this.handleContituencyChange}
                                        style={styles.picker}
                                        items={this.constituency}
                                        placeholder="Select Constituency"
                                       
                                        placeholderStyle={{ color: '#000' }}
                                        item={this.state.pickedConstituency}
                                      // backdropAnimation={{ opactity: 0 }}
                                      //androidPickerMode="dropdown"
                                      //isNullable
                                      //disable
                                      />
                                </View>
                            </View>
                            <View style={styles.btnBoxWrapper}>
                                <LinearGradient
                                    colors={['#00cfcf', '#00cfef']}
                                    start={{ x: 0.3, y: 0.1 }}
                                    end={{ x: 0.3, y: 1.9 }}
                                    locations={[0.1, 0.6]}
                                    style={{ borderRadius: 20 }}>
                                    <TouchableOpacity
                                        activeOpacity={1}
                                        style={styles.boxBtn2}
                                        onPress={() =>
                                            this.submitConstituency()
                                        }>
                                        <Text
                                            style={styles.btnText}>
                                            {translate('submit')}
                     </Text>
                                    </TouchableOpacity>
                                </LinearGradient>
                            </View>
                            <AwesomeAlert
                            
                            show={this.state.showAlert}
                            showProgress={false}
                            title={"NETAHUB"}
                            titleStyle={{fontSize:14,color:'#000',fontWeight:"bold"}}
                            message="Constituency Added Sucessfully"
                            closeOnTouchOutside={true}
                            closeOnHardwareBackPress={false}
                            //showCancelButton={true}
                            showConfirmButton={true}
                            confirmText="Okay"
                            confirmButtonColor="#00cced"
                            onConfirmPressed={() => {
                              this.hideAlert();
                            }}
                        />
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
                // console.log(state)
                return {
                  addConstituency1: state.addConstituency.addConstituency,
                  addConstituency_error: state.addConstituency.addConstituency_error,
                  unmappedElection1: state.unmappedElection.unmappedElection,
                  unmappedElection_error: state.unmappedElection.unmappedElection_error,
                  unmappedConstituency1: state.unmappedConstituency.unmappedConstituency,
                  unmappedConstituency_error: state.unmappedConstituency.unmappedConstituency_error,
                  insertUserCons1: state.insertUserCons.insertUserCons,
                  insertUserCons_error: state.insertUserCons.insertUserCons_error
                };
              };
              export default connect(mapStateToProps, {
                addConstituency, unmappedElection, unmappedConstituency, insertUserCons
              })(AddConstituency);
