import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image, Picker, FlatList, I18nManager
} from "react-native";
const { width, height } = Dimensions.get('window');
const screenWidth = Dimensions.get("window").width;
import InputField from '../../Commons/input';
import { Images } from '../../utils'
import Share from "react-native-share";
import styles from './style';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { notification } from '../../redux/actions/notification';
import Swipeout from 'react-native-swipeout';
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";
const translationGetters = {
    // lazy requires (metro bundler does not support symlinks)
    en: () => require("../../assets/en-US.json"),
    hi: () => require("../../assets/hi.json")
};
const translate = memoize(
    (key, config) => i18n.t(key, config),
    (key, config) => (config ? key + JSON.stringify(config) : key)
);
const assembly = [
    {
        id: '1',
        name: 'Denish',
        image: Images.parlmnt
    }
];
class NotificationScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            notifications: [],
            profile_pic: '',
            refreshing: false,
        }
        this._getStorageValue();
    }
    _onRefresh() {
        this.setState({ refreshing: true });
        this._getStorageValue().then(() => {
            this.setState({ refreshing: false });
        });
    }
    async _getStorageValue() {
        var value = await AsyncStorage.getItem('token')
        var profile_pic = await AsyncStorage.getItem('profile_pic')
        this.state.profile_pic = profile_pic
        if (value == null) {
            console.log(value)
            this.props.navigation.replace('Login')
        }
        else {
            this.setState({ token: value })
            this.props.notification(value).then(async () => {
                console.log(this.props.notification_error)
                if (this.props.notification_error == "Invalid Token") {
                    await AsyncStorage.setItem('token', "")
                    this.props.navigation.replace('Login')
                }
                console.log(this.state.notifications)
                if (this.props.notification1 && this.props.notification1[0] && this.props.notification1[0].title)
                    this.setState({ notifications: this.props.notification1 })
                console.log(this.state)
            })
        }
        return value
    }
    renderData(rowData){
        let swipeBtns = [
            {
                component: (
                    <View
                        style={{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center',
                            flexDirection: 'column',
                        }}
                    >
                        <Image source={Images.delet} style={{ height: height * 0.04, width: height * 0.04, }} />
                    </View>
                ),
                text: 'Delete',
                sensitivity: 50,
                backgroundColor: 'red',
                underlayColor: 'rgba(0, 0, 0, 1, 0.6)',
                onPress: () => { this.deleteNote(rowData) }
            },
            // {
            //     text: 'Duplicate',
            //     backgroundColor: 'blue',
            //     underlayColor: 'rgba(0, 0, 0, 1, 0.6)',
            //     onPress: () => { this.duplicateNote(rowData) }
            // }
        ];
    }
    render() {
        
        return (
            <View style={styles.container}>
                <View style={styles.upperBar}>
                    <View style={styles.backIconContainer}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image source={Images.backIcon} style={styles.barMenuIcon} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.headingContainer}>
                        <Text style={styles.barText}>Notification</Text>
                    </View>
                    <View>
                        <TouchableOpacity onPress={() =>
                            this.props.navigation.navigate('Profile')}>
                            <Image source={{ uri: 'https://netahub.com/images/user/' + this.state.profile_pic }} style={styles.profileIcon} />
                        </TouchableOpacity>
                    </View>
                </View>
                <KeyboardAvoidingView behavior="padding">
                    <LinearGradient
                        colors={['#00cfcf', '#00cfef']}
                        start={{ x: 0.3, y: 0.1 }}
                        end={{ x: 0.3, y: 1.9 }}
                        locations={[0.1, 0.6]}
                        style={styles.wrapper}>
                    </LinearGradient>
                    <View style={{ alignItems: 'center', marginBottom: '5%', }}>
                        { this.state.notifications.length > 0 ? <FlatList
                            showsVerticalScrollIndicator={false}
                            contentContainerStyle={{ marginTop: '25%', paddingBottom: '55%' }}
                            data={this.state.notifications}
                            renderItem={({ item }) => (
                                <>
                                    
                                        <View style={styles.docDetailedWrapper2}>
                                            <View
                                                style={[styles.docSpecsWrapper, { marginTop: 10 }]}>
                                                <Image source={{ uri: 'https://netahub.com/images/' + item.photo_path }} style={styles.profileIcon2} />
                                                <View style={styles.docNameWrapper}>
                                                    <Text numberOfLines={1} style={styles.docNameText}>
                                                        {item.title}
                                                    </Text>
                                                    <Text numberOfLines={2} style={styles.docSubNameText}>
                                                        {item.message}
                                                    </Text>
                                                </View>
                                            </View>
                                        </View>
                                    
                                </>
                            )}
                            keyExtractor={item => item.id}
                        />
                    :
                    <>
                    <View style={styles.docDetailedWrapper3}>
              <View style={{marginLeft: 20}}>
                <Image source={Images.netaHubLogo} style={styles.bellIcon1} />
              </View>
              <View style={styles.docNameWrapper}>
                <Text style={styles.docNameText}>
                !!! No Notifications Found !!!
                </Text>
              </View>
            </View>
                    </>}
                    </View>
                </KeyboardAvoidingView>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    console.log('dinesh', JSON.stringify(state))
    return {
        notification1: state.notification.notification,
        notification_error: state.notification.notification_error
    };
};
export default connect(mapStateToProps, {
    notification
})(NotificationScreen);
