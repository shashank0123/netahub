import { StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
export default StyleSheet.create({
    container: {
        width,
        height,
        display: 'flex',
    },
    upperBar: {
        width,
        height: height * 0.075,
        backgroundColor: '#00b8e6',
        flexDirection: 'row',
        alignItems: 'center',
    },
    backIconContainer: {
    },
    headingContainer: {
        width: '50%',
        marginLeft: '10%',
        alignItems: 'center',
    },
    barText: {
        color: "#000000",
        fontSize: height * 0.03,
        fontWeight: 'bold'
    },
    barMenuIcon: {
        height: height * 0.04,
        width: height * 0.04,
        marginLeft: width * 0.04,
    },
    scrollWrapper: {
        width,
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingBottom: '33%'
        // backgroundColor: 'pink',
    },
    wrapper: {
        width: width * 1,
        height: height * 0.3,
        backgroundColor: '#00cccc',
        alignItems: 'center',
        position: 'absolute'
    },
    docDetailedWrapper2: {
        width: width * 0.98,
        height: height * 0.1,
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#ffffff',
        // marginTop: height * 0.02,
        // borderRadius: 15,
        shadowColor: 'rgba(1, 1, 1, 1)',
        // elevation: 4,
        // marginLeft: 18,
        borderBottomWidth: .5,
        borderBottomColor: "#C8C8C8",
    },
    docSpecsWrapper: {
        width: '93%',
        display: 'flex',
        flexDirection: 'row',
    },
    docdetailImg: {
        width: 50,
        height: 50,
        borderRadius: 30,
        borderWidth: 1,
        // borderColor: colors.WHITE,
        // shadowColor: colors.BLACK,
        shadowOffset: { width: 2, height: 2 },
        shadowOpacity: 1,
        shadowRadius: 1,
        elevation: 5,
        marginBottom: 4
    },
    docNameWrapper: {
        width: '80%',
        marginLeft:35,
        display: 'flex',
        // marginTop: 7,
        flexDirection: 'column',
    },
    docNameText: {
        fontFamily: 'Helvetica Neue',
        color: '#000',
        fontSize: width * 0.038,
        fontWeight: 'bold',
        paddingBottom: 1,
    },
    docSubNameText: {
        // color: '#898A8F',
        color: '#000',
        fontSize: width * 0.033,
        fontWeight: '400',
        fontFamily: 'Helvetica Neue',
        fontStyle: 'normal',
    },
    profileIcon: {
        height: height * 0.05,
        width: height * 0.05,
        borderRadius: 50,
        marginLeft: width * 0.13,
    },
    profileIcon2: {
        height: height * 0.06,
        width: height * 0.06,
        borderRadius: 50,
        marginTop: 4,
        marginLeft: width * 0.01,
    },
    bottomPadding: {
        marginBottom: 10
    },
    bellIcon1: {
        height: height * 0.04,
        width: height * 0.04,
        borderRadius:50
        
    },
    docDetailedWrapper3: {
        width: width * 0.9,
        height: height * 0.08,
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#ffffff',
        marginTop: height * 0.38,
        borderRadius: 5,
        shadowColor: 'rgba(1, 1, 1, 1)',
        // elevation: 4,
        // marginLeft: 18,
        borderBottomWidth: .5,
        borderBottomColor: "#C8C8C8",
        flexDirection:'row'
    },
});