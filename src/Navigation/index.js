import 'react-native-gesture-handler';
import React from 'react';
import { Image } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Images } from '../utils';
//Screens
import Login from '../Screens/Login';
import Ragistration from '../Screens/Ragisteration';
import OtpRagistration from '../Screens/OtpRagistration';
import AdressDetails from '../Screens/AdressDetails';
import Dashboard from '../Screens/Dashboard';
import Reports from '../Screens/Reports';
import Settings from '../Screens/Settings';
import Elections from '../Screens/Elections';
import ForgetPassword from '../Screens/ForgetPassword';
import Profile from '../Screens/Profile';
import AddNominee from '../Screens/AddNominee';
import SurveyName from '../Screens/Survey Name';
import FeedBack from '../Screens/FeedBack';
import AgeAnalysis from '../Screens/AgeAnalysis';
import ProfessionAnalysis from '../Screens/ProfessionAnalysis';
import TehsilAnalysis from '../Screens/TehsilAnalysis';


import GenderAnalysis from '../Screens/GenderAnalysis';

import MyConstituency from '../Screens/MyConstituency';
import FAQs from '../Screens/FAQs';
import SelfNominate from '../Screens/SelfNominate';
import CastVotes from '../Screens/CastVote';

import Example from '../Screens/Example';
import EditCollege from '../Screens/EditCollege';
import EditSociety from '../Screens/EditSociety';

import EditCountry from '../Screens/EditCountry';
import EditPincode from '../Screens/EditPincode';
import EditDistrict from '../Screens/EditDistrict';
import EditBlock from '../Screens/EditBlock';
import EditWard from '../Screens/EditWard';
import EditState from '../Screens/EditState';
import EditTehsil from '../Screens/EditTehsil';
import EditVillage from '../Screens/EditVillage';

import AddConstituency from '../Screens/AddConstituency';
import OtpScreen from '../Screens/OtpScreen';
import NomineeDetails from '../Screens/NomineeDetails';
import NotificationScreeen from '../Screens/NotificationScrren';
import ReferAndEarn from '../Screens/Refern&Earn';
import TermsAndConditions from '../Screens/T&C';
import PrivacyPolicy from '../Screens/PrivacyPolicy';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
export function AppHome() {
  return (
    <Tab.Navigator
      initialRouteName="Elections"
      tabBarOptions={{
        activeTintColor: 'rgb(85,136,231)',
        style: { height: 60, zIndex:1000 },
      }}>
      <Tab.Screen
        name="Elections"
        component={Elections}
        options={{
          tabBarLabel: 'Elections',
          tabBarIcon: ({ focused }) => {
            if (focused == true) {
              return <Image source={Images.election1} style={{width:50,height:45}} />;
            } else {
              return <Image source={Images.election2}  style={{width:50,height:45}} />;
            }
          },
        }}
      />
      {/* <Tab.Screen
        name="Appointments"
        component={Appointments}
        options={{
          tabBarLabel: 'Appointments',
          tabBarIcon: ({ focused }) => {
            if (focused == true) {
              return <Image source={Icons.homeIcon} />;
            } else {
              return <Image source={Icons.homeIcon2} />;
            }
          },
        }}
      /> */}
      <Tab.Screen
        name="Dashboard"
        component={Dashboard}
        options={{
          tabBarLabel: 'Dashboard',
          tabBarIcon: ({ focused }) => {
            if (focused == true) {
              return <Image source={Images.dashboard2} style={{width:50,height:45}} />;
            } else {
              return  <Image source={Images.dashboard1} style={{width:50,height:45}} />;
            }
          },
        }}
      />
      <Tab.Screen
        name="Reports"
        component={Reports}
        options={{
          tabBarLabel: 'Reports',
          tabBarIcon: ({ focused }) => {
            if (focused == true) {
              return <Image source={Images.report2} style={{width:50,height:45}} />;
            } else {
              return <Image source={Images.report1} style={{width:50,height:45}} />;
            }
          },
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({ focused }) => {
            if (focused == true) {
              return <Image source={Images.profile2} style={{width:38,height:38}} />;
            } else {
              return  <Image source={Images.profile1} style={{width:38,height:38}} />;
            }
          },
        }}
      />
      <Tab.Screen
        name="Settings"
        component={Settings}
        options={{
          tabBarLabel: 'Settings',
          tabBarIcon: ({ focused }) => {
            if (focused == true) {
              return <Image source={Images.settings2} style={{width:50,height:45}} />;
            } else {
              return <Image source={Images.settings1} style={{width:50,height:45}} />;
            }
          },
        }}
      />
       
    </Tab.Navigator>
  );
}
function Navigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Login"
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="AppHome" component={AppHome} />
        <Stack.Screen name="Ragistration" component={Ragistration} />
        <Stack.Screen name="OtpRagistration" component={OtpRagistration} />
        <Stack.Screen name="AdressDetails" component={AdressDetails} />
        <Stack.Screen name="Elections" component={AppHome} />
        <Stack.Screen name="Settings" component={Settings} />
        <Stack.Screen name="Reports" component={Reports} />
        <Stack.Screen name="ForgetPassword" component={ForgetPassword} />
        <Stack.Screen name="Profile" component={Profile} />
        <Stack.Screen name="AddNominee" component={AddNominee} />
        <Stack.Screen name="SurveyName" component={SurveyName} />
        <Stack.Screen name="FeedBack" component={FeedBack} />
        <Stack.Screen name="AgeAnalysis" component={AgeAnalysis} />
        <Stack.Screen name="ProfessionAnalysis" component={ProfessionAnalysis} />
        <Stack.Screen name="TehsilAnalysis" component={TehsilAnalysis} />
       
        <Stack.Screen name="GenderAnalysis" component={GenderAnalysis} />
       
        <Stack.Screen name="MyConstituency" component={MyConstituency} />
        <Stack.Screen name="FAQs" component={FAQs} />
        <Stack.Screen name="SelfNominate" component={SelfNominate} />
        <Stack.Screen name="CastVotes" component={CastVotes} />
        <Stack.Screen name="EditSociety" component={EditSociety} />
        <Stack.Screen name="EditCollege" component={EditCollege} />
        <Stack.Screen name="Example" component={Example} />
        <Stack.Screen name="EditCountry" component={EditCountry} />
        <Stack.Screen name="EditPincode" component={EditPincode} />
        <Stack.Screen name="EditDistrict" component={EditDistrict} />
        <Stack.Screen name="EditBlock" component={EditBlock} />
        <Stack.Screen name="EditWard" component={EditWard} />
        <Stack.Screen name="EditState" component={EditState} />
        <Stack.Screen name="EditTehsil" component={EditTehsil} />
        <Stack.Screen name="EditVillage" component={EditVillage} />

        <Stack.Screen name="AddConstituency" component={AddConstituency} />
        <Stack.Screen name="OtpScreen" component={OtpScreen} />
        <Stack.Screen name="NomineeDetails" component={NomineeDetails} />
        <Stack.Screen name="NotificationScreen" component={NotificationScreeen} /> 
        <Stack.Screen name="ReferAndEarn" component={ReferAndEarn} />  
        <Stack.Screen name="TermsAndConditions" component={TermsAndConditions} />
        <Stack.Screen name="PrivacyPolicy" component={PrivacyPolicy} />        
      </Stack.Navigator>
    </NavigationContainer>
  );
}
export default Navigation;
