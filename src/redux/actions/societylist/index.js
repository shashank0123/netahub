import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const societylist = (state) => {
  var url = urlObj.societylist_url+"?token="+state.token+"&district_id="+state.district_id;
  return async (dispatch, getState) => {
    await axios
      .get(url)
      .then(async (response) => {
        
          dispatch({
            type: 'ELECTION',
            payload: response.data
            
          });
        // }
      })
      .catch((error) => {
        console.log(error);
      });
  };
};