import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const unmappedElection = (token) => {
  url = urlObj.unmappedElection_url+"?token="+token;
  return async (dispatch, getState) => {
    await axios
      .get(url)
      .then(async (response) => {
        if (response.data[0].code == 500){

          dispatch({
          type: 'FAILED',
          payload: response.data[0].message
          
        });
        }
        else{
          console.log(response.data)
          dispatch({
            type: 'ELECTION',
            payload: response.data
            
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
};