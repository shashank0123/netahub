import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const profilepic = (state) => {
  const form = new FormData();
    form.append('filedata',state.data)
	form.append('token',state.token)
  url = urlObj.profilepic_url;
  return async (dispatch, getState) => {
    await axios
      .post(url, form)
      .then(async (response) => {
        if (response.data[0].code == 500){
          dispatch({
          type: 'FAILED',
          payload: response.data.message
          
        });
        }
        else{
          dispatch({
            type: 'ELECTION',
            payload: response.data
            
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
};