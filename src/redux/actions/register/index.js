import axios from 'axios';
import urlObj from '../../url';
export const register = (state) => {
  var url = urlObj.register_url + "?display_name=" + state.display_name + "&email_id=" + state.email_id + "&mobile_no=" + state.mobile_no + "&user_password=" + state.user_password + "&qualification=" + state.qualification + "&gender=" + state.gender + "&occupation=" + state.occupation + "&referred_by=" + state.referred_by + "&status=" + state.status + "&pincode=" + state.pincode;
  console.log(url)
  return async (dispatch, getState) => {
    await axios
      .get(url)
      .then(async (response) => {
        if (response.data[0].code == 500) {
          dispatch({
            type: 'FAILED',
            payload: response.data.message

          });
        }
        else {
          dispatch({
            type: 'ELECTION',
            payload: response.data

          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
};