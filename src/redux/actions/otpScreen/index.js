import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const otpScreen = (state) => {
   var url = urlObj.otpScreen_url+"?otp="+state.otp+"&new_password="+state.new_password+"&conf_password="+state.conf_password+"&login="+state.mobile_no
  return async (dispatch, getState) => {
    await axios
      .get(url)
      .then(async (response) => {
        if (response.data[0].code == 500){
          dispatch({
          type: 'FAILED',
          payload: response.data.message
          
        });
        }
        else{
          dispatch({
            type: 'OTPSCREEN',
            payload: response.data
            
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
};