import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const addNominee = (token) => {
  url = urlObj.addNominee_url+"?token="+token;
  return async (dispatch, getState) => {
    await axios
      .get(url)
      .then(async (response) => {
        console.log(response.data)
        if (response.data[0].code == 500){
          dispatch({
          type: 'FAILED',
          payload: response.data.message
          
        });
        }
        else{
          dispatch({
            type: 'addNominee',
            payload: response.data
            
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
};