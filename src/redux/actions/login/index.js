import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const login = (login) => {
  var url = urlObj.login_url+"?login="+login.username+"&password="+login.password+"&device_token="+login.device_token+"&imei="+login.imei;
  console.log(url)
  return async (dispatch, getState) => {
    await AsyncStorage.setItem('username', login.username);
    await axios
      .get(url)
      .then(async (response) => {
        if (response.data[0].code == 500){
          dispatch({
          type: 'FAILED',
          payload: response.data.message
          
        });
        }
        else{
          await AsyncStorage.setItem('token', response.data[0].token);
          
          dispatch({
            type: 'LOGIN',
            payload: response.data.token
            
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
};