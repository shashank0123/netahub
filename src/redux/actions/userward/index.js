import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const userward = (token) => {
  url = urlObj.userward_url+"?token="+token;
  return async (dispatch, getState) => {
    console.log(url)
    await axios
      .get(url)
      .then(async (response) => {
        
          dispatch({
            type: 'ELECTION',
            payload: response.data
            
          });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};