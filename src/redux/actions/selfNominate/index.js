import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const selfNominate = (state) => {
  url = urlObj.selfNominate_url
  const form = new FormData();
  form.append('filedata',state.image)
  form.append('token',state.token)
  form.append('survey_id',state.survey_id)
  form.append('cons_id',state.cons_id)
  form.append('party_id',state.party_id)
  form.append('nominee_name',state.nominee_name)
  form.append('mobile_no',state.mobile_no)
  form.append('nominee_desc',state.nominee_desc)
  console.log(url)
  console.log(form)
  return async (dispatch, getState) => {
    await axios
      .post(url, form)
      .then(async (response) => {
        if (response.data[0].code == 500){
          dispatch({
          type: 'FAILED',
          payload: response.data.message
          
        });
        }
        else{
          console.log(response.data)
          dispatch({
            type: 'ELECTION',
            payload: response.data
            
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
};