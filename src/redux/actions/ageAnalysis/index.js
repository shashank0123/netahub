import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const ageAnalysis = (state) => {
  url = urlObj.ageAnalysis_url+"?token="+state.token+"&survey_id="+state.survey_id+"&nominee_id="+state.nominee_id;
  console.log(url)
  return async (dispatch, getState) => {
    await axios
   
      .get(url)
      .then(async (response) => {
       
          dispatch({
            type: 'ELECTION',
            payload: response.data
            
          });
        
      })
      .catch((error) => {
        console.log(error);
      });
  };
};