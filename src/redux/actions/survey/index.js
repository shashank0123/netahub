import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const survey = (state) => {
  url = urlObj.survey_url+"?token="+state.token+"&election_id="+state.election_id+"&survey_name="+state.survey_name+"&survey_desc="+state.survey_desc+"&from_date="+state.from_date+"&to_date="+state.to_date;
  return async (dispatch, getState) => {
    await axios
      .get(url)
      .then(async (response) => {
        if (response.data[0].code == 500){
          dispatch({
          type: 'FAILED',
          payload: response.data.message
          
        });
        }
        else{
          dispatch({
            type: 'ELECTION',
            payload: response.data
            
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
};