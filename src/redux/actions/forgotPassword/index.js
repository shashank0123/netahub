import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const forgotPassword = (mobile_no) => {
  var url = urlObj.forgotPassword_url + "?login=" + mobile_no;
  console.log(url)
  return async (dispatch, getState) => {
    await axios
      .get(url)
      .then(async (response) => {
        console.log(response)
        if (response.data[0].code == 500) {
          dispatch({
            type: 'FAILED',
            payload: response.data.message

          });
        }
        else {
          dispatch({
            type: 'ELECTION',
            payload: response.data

          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
};