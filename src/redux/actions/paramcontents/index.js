import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const paramcontents = (token, user_id) => {
  url = urlObj.paramcontents_url+"?token="+token+"&referrer_id="+user_id+"&nominee_id=&survey_id=&content_type=RNE";
  console.log(url)
  return async (dispatch, getState) => {
    console.log(url)
    await axios
      .get(url)
      .then(async (response) => {
        
          dispatch({
            type: 'ELECTION',
            payload: response.data
            
          });
      
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};