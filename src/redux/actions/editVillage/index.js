import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const editVillage = (state) => {
  var url = urlObj.editVillage_url + "?token=" + state.token + "&village_id=" + state.village_id + "&village_name=" + state.village_name + "&pincode=" + state.pincode + "&block_id=" + state.block_id;
  console.log(url)
  return async (dispatch, getState) => {
    await axios
      .get(url)
      .then(async (response) => {
        if (response.data[0].code == 500) {
          dispatch({
            type: 'FAILED',
            payload: response.data.message

          });
        }
        else {
          dispatch({
            type: 'ELECTION',
            payload: response.data

          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
};