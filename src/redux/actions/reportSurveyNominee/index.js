import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const reportSurveyNominee = (state) => {
  url = urlObj.reportSurveyNominee_url+"?token="+state.token+"&survey_id="+state.survey_id;
  return async (dispatch, getState) => {
    await axios
      .get(url)
      .then(async (response) => {
        
          dispatch({
            type: 'ELECTION',
            payload: response.data
            
          });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};