import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const editSociety = (state) => {
  url = urlObj.editSociety_url+"?token="+state.token+"&society_id="+state.society_id+"&society_name="+state.society_name+"&district_id="+state.district_id;
  return async (dispatch, getState) => {
    await axios
      .get(url)
      .then(async (response) => {
        if (response.data[0].code == 500){
          dispatch({
          type: 'FAILED',
          payload: response.data.message
          
        });
        }
        else{
          dispatch({
            type: 'ELECTION',
            payload: response.data
            
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
};