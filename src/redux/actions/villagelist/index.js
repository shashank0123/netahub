import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const villagelist = (state) => {
  url = urlObj.villagelist_url+"?token="+state.token+"&block_id="+state.block_id;
  return async (dispatch, getState) => {
    console.log(url)
    await axios
      .get(url)
      .then(async (response) => {
        if (response.data[0].code == 500){
          dispatch({
          type: 'FAILED',
          payload: response.data.message
          
        });
        }
        else{
          dispatch({
            type: 'ELECTION',
            payload: response.data
            
          });
        }
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};