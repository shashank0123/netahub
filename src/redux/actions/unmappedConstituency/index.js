import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const unmappedConstituency = (state) => {
  console.log(state)
  url = urlObj.unmappedConstituency_url+"?token="+state.token+"&location_type="+state.location_type+"&location_id="+state.location_id+"&election_id="+state.election_id;
  console.log(url)
  return async (dispatch, getState) => {
    await axios
      .get(url)
      .then(async (response) => {
        if (response.data[0] != undefined && response.data[0].code == 500){

          dispatch({
          type: 'FAILED',
          payload: response.data[0].message
          
        });
        }
        else{
          dispatch({
            type: 'ELECTION',
            payload: response.data
            
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
};