import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const dashboard = (token) => {
  url = urlObj.dashboard_url + "?token=" + token;
  return async (dispatch, getState) => {
    await axios
      .get(url)
      .then(async (response) => {

        dispatch({
          type: 'ELECTION',
          payload: response.data

        });

      })
      .catch((error) => {
        console.log(error);
      });
  };
};