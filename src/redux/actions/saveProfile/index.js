import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const saveProfile = (state) => {
  // 'updateuserprofile'=>array('dbProc'=>'upd_user_profile','params'=>array('token',  'display_name', 'email_id',,, )),
  url = urlObj.saveProfile_url+"?token="+state.token+"&gender="+state.selectedValue1+"&occupation="+state.selectedValue2+"&dateofbirth="+state.dateofbirth+"&qualification="+state.selectedValue3+"&email="+state.email+"&display_name="+state.display_name;
  console.log(url)
  return async (dispatch, getState) => {
    await axios
      .get(url)
      .then(async (response) => {
        if (response.data[0].code == 500){
          dispatch({
          type: 'FAILED',
          payload: response.data.message
          
        });
        }
        else{
          dispatch({
            type: 'ELECTION',
            payload: response.data
            
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
};