import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const collegelist = (state) => {
  var url = urlObj.collegelist_url+"?token="+state.token;
  return async (dispatch, getState) => {
    await axios
      .get(url)
      .then(async (response) => {
        
          dispatch({
            type: 'ELECTION',
            payload: response.data
            
          });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};