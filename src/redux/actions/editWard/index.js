import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const editWard = (state) => {
  url = urlObj.editWard_url+"?token="+state.token+"&ward_id="+state.ward_id+"&ward_name="+state.ward_name+"&village_id="+state.village_id;
  return async (dispatch, getState) => {
    await axios
      .get(url)
      .then(async (response) => {
        if (response.data[0].code == 500){
          dispatch({
          type: 'FAILED',
          payload: response.data.message
          
        });
        }
        else{
          dispatch({
            type: 'ELECTION',
            payload: response.data
            
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
};