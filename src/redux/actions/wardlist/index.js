import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const wardlist = (state) => {
  url = urlObj.wardlist_url+"?token="+state.token+"&village_id="+state.village_id;
  console.log(url)
  return async (dispatch, getState) => {
    console.log(url)
    await axios
      .get(url)
      .then(async (response) => {
        
          dispatch({
            type: 'ELECTION',
            payload: response.data
            
          });
      
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};