import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const reportSurvey = (state) => {
  url = urlObj.reportSurvey_url + "?token=" + state.token + "&location_type=" + state.location_type;
  return async (dispatch, getState) => {
    await axios
      .get(url)
      .then(async (response) => {

        dispatch({
          type: 'ELECTION',
          payload: response.data

        });

      })
      .catch((error) => {
        console.log(error);
      });
  };
};