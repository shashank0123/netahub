const initialState = {
    editPincode: [],
    editPincode_error:{}
  };
  
  export const editPincode = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,editPincode:action.payload, editPincode_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,editPincode_error:action.payload, editPincode:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default editPincode;