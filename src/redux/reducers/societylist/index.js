const initialState = {
    societylist: [],
    societylist_error:{}
  };
  
  export const societylist = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,societylist:action.payload, societylist_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,societylist_error:action.payload, societylist:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default societylist;