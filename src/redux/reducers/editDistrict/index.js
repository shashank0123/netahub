const initialState = {
    editDistrict: [],
    editDistrict_error:{}
  };
  
  export const editDistrict = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,editDistrict:action.payload, editDistrict_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,editDistrict_error:action.payload, editDistrict:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default editDistrict;