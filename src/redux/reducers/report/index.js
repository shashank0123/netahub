const initialState = {
    report: [],
    report_error:{}
  };
  
  export const report = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,report:action.payload, report_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,report_error:action.payload, report:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default report;