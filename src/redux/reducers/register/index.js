const initialState = {
    register: [],
    register_error:{}
  };
  
  export const register = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,register:action.payload, register_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,register_error:action.payload, register:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default register;