const initialState = {
    userstate: [],
    userstate_error:{}
  };
  
  export const userstate = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,userstate:action.payload, userstate_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,userstate_error:action.payload, userstate:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default userstate;