const initialState = {
    districtlist: [],
    districtlist_error:{}
  };
  
  export const districtlist = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,districtlist:action.payload, districtlist_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,districtlist_error:action.payload, districtlist:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default districtlist;