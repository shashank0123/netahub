const initialState = {
    usercollege: [],
    usercollege_error:{}
  };
  
  export const usercollege = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,usercollege:action.payload, usercollege_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,usercollege_error:action.payload, usercollege:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default usercollege;