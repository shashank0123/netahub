const initialState = {
    tehsillist: [],
    tehsillist_error:{}
  };
  
  export const tehsillist = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,tehsillist:action.payload, tehsillist_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,tehsillist_error:action.payload, tehsillist:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default tehsillist;