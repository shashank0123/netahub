const initialState = {
    forgotPassword: [],
    forgotPassword_error:{}
  };
  
  export const forgotPassword = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,forgotPassword:action.payload, forgotPassword_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,forgotPassword_error:action.payload, forgotPassword:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default forgotPassword;