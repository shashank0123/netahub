const initialState = {
    professionAnalysis: [],
    professionAnalysis_error:{}
  };
  
  export const professionAnalysis = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,professionAnalysis:action.payload, professionAnalysis_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,professionAnalysis_error:action.payload, professionAnalysis:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default professionAnalysis;