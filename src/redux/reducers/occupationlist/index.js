const initialState = {
    occupationlist: [],
    occupationlist_error:{}
  };
  
  export const occupationlist = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,occupationlist:action.payload, occupationlist_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,occupationlist_error:action.payload, occupationlist:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default occupationlist;