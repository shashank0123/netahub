const initialState = {
    party: [],
    party_error:{}
  };
  
  export const party = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,party:action.payload, party_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,party_error:action.payload, party:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default party;