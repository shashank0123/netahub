const initialState = {
    editVillage: [],
    editVillage_error:{}
  };
  
  export const editVillage = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,editVillage:action.payload, editVillage_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,editVillage_error:action.payload, editVillage:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default editVillage;