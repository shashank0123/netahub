const initialState = {
    saveProfile: [],
    saveProfile_error:{}
  };
  
  export const saveProfile = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,saveProfile:action.payload, saveProfile_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,saveProfile_error:action.payload, saveProfile:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default saveProfile;