const initialState = {
    otpScreen: [],
    otpScreen_error:{}
  };
  
  export const otpScreen = (state = initialState, action) => {
    switch (action.type) {
      case 'OTPSCREEN': {
        // console.log(action.payload)
        return { ...state,otpScreen:action.payload, otpScreen_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,otpScreen_error:action.payload, otpScreen:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default otpScreen;