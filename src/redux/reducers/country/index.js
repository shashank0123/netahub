const initialState = {
    country: [],
    country_error:{}
  };
  
  export const country = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,country:action.payload, country_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,country_error:action.payload, country:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default country;