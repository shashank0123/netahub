const initialState = {
    usertehsil: [],
    usertehsil_error:{}
  };
  
  export const usertehsil = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,usertehsil:action.payload, usertehsil_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,usertehsil_error:action.payload, usertehsil:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default usertehsil;