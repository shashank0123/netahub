const initialState = {
    deleteConstituency: [],
    deleteConstituency_error:{}
  };
  
  export const deleteConstituency = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,deleteConstituency:action.payload, deleteConstituency_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,deleteConstituency_error:action.payload, deleteConstituency:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default deleteConstituency;