const initialState = {
    unmappedElection: [],
    unmappedElection_error:{}
  };
  
  export const unmappedElection = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,unmappedElection:action.payload, unmappedElection_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,unmappedElection_error:action.payload, unmappedElection:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default unmappedElection;