const initialState = {
    reportSurveyNominee: [],
    reportSurveyNominee_error:{}
  };
  
  export const reportSurveyNominee = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,reportSurveyNominee:action.payload, reportSurveyNominee_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,reportSurveyNominee_error:action.payload, reportSurveyNominee:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default reportSurveyNominee;