const initialState = {
    selfNominate: [],
    selfNominate_error:{}
  };
  
  export const selfNominate = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,selfNominate:action.payload, selfNominate_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,selfNominate_error:action.payload, selfNominate:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default selfNominate;