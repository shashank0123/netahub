const initialState = {
    editTehsil: [],
    editTehsil_error:{}
  };
  
  export const editTehsil = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,editTehsil:action.payload, editTehsil_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,editTehsil_error:action.payload, editTehsil:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default editTehsil;