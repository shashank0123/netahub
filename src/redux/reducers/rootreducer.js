import { combineReducers } from "redux"
import login from './login'
import elections from './elections'
import addConstituency from './addConstituency'
import addNominee from './addNominee'
import ageAnalysis from './ageAnalysis'
import blockAnalysis from './blockAnalysis'
import blocklist from './blocklist'
import CastVote from './castVote'
import castVoteSave from './castVoteSave'
import contribute from './contribute'
import collegelist from './collegelist'
import country from './country'
import countryAnalysis from './countryAnalysis'
import dashboard from './dashboard'
import deleteConstituency from './deleteConstituency'
import districtAnalysis from './districtAnalysis'
import districtlist from './districtlist'
import editBlock from './editBlock'
import editCollege from './editCollege'
import editCountry from './editCountry'
import editDistrict from './editDistrict'
import editPincode from './editPincode'
import editSociety from './editSociety'
import editState from './editState'
import editTehsil from './editTehsil'
import editVillage from './editVillage'
import editWard from './editWard'
import faq from './faq'
import feedback from './feedback'
import forgotPassword from './forgotPassword'
import genderAnalysis from './genderAnalysis'
import insertUserCons from './insertUserCons'
import myConstituency from './myConstituency'
import nagarPanchayatAnalysis from './nagarPanchayatAnalysis'
import nomineeDetails from './nomineeDetails'
import notification from './notification'
import otpScreen from './otpScreen'
import occupationlist from './occupationlist'
import party from './party'
import paramcontents from './paramcontents'
import professionAnalysis from './professionAnalysis'
import profile from './profile'
import profilepic from './profilepic'
import qualificationlist from './qualificationlist'
import register from './register'
import report from './report'
import reportSurvey from './reportSurvey'
import reportSurveyNominee from './reportSurveyNominee'
import saveProfile from './saveProfile'
import selfNominate from './selfNominate'
import setting from './setting'
import societylist from './societylist'
import stateAnalysis from './stateAnalysis'
import statelist from './statelist'
import survey from './survey'
import tehsilAnalysis from './tehsilAnalysis'
import tehsillist from './tehsillist'
import unmappedConstituency from './unmappedConstituency'
import unmappedElection from './unmappedElection'
import userblock from './userblock'
import usercountry from './usercountry'
import usercollege from './usercollege'
import userdistrict from './userdistrict'
import userstate from './userstate'
import usersociety from './usersociety'
import usertehsil from './usertehsil'
import uservillage from './uservillage'
import userward from './userward'
import villageAnalysis from './villageAnalysis'
import villagelist from './villagelist'
import wardAnalysis from './wardAnalysis'
import wardlist from './wardlist'
const rootReducer = combineReducers({
  login:login,
  elections:elections,
  addConstituency:addConstituency,
  addNominee:addNominee,
  ageAnalysis:ageAnalysis,
  blockAnalysis:blockAnalysis,
  blocklist:blocklist,
  CastVote:CastVote,
  castVoteSave:castVoteSave,
  contribute:contribute,
  collegelist:collegelist,
  country:country,
  countryAnalysis:countryAnalysis,
  dashboard:dashboard,
  deleteConstituency:deleteConstituency,
  districtAnalysis:districtAnalysis,
  districtlist:districtlist,
  editBlock:editBlock,
  editCollege:editCollege,
  editCountry:editCountry,
  editDistrict:editDistrict,
  editPincode:editPincode,
  editSociety:editSociety,
  editState:editState,
  editTehsil:editTehsil,
  editVillage:editVillage,
  editWard:editWard,
  faq:faq,
  feedback:feedback,
  forgotPassword:forgotPassword,
  genderAnalysis:genderAnalysis,
  insertUserCons:insertUserCons,
  myConstituency:myConstituency,
  nagarPanchayatAnalysis:nagarPanchayatAnalysis,
  nomineeDetails:nomineeDetails,
  notification:notification,
  otpScreen:otpScreen,
  occupationlist:occupationlist,
  paramcontents:paramcontents,
  party:party,
  professionAnalysis:professionAnalysis,
  profile:profile,
  profilepic:profilepic,
  qualificationlist:qualificationlist,
  register:register,
  report:report,
  reportSurvey:reportSurvey,
  reportSurveyNominee:reportSurveyNominee,
  saveProfile:saveProfile,
  selfNominate:selfNominate,
  setting:setting,
  societylist:societylist,
  stateAnalysis:stateAnalysis,
  statelist:statelist,
  survey:survey,
  tehsilAnalysis:tehsilAnalysis,
  tehsillist:tehsillist,
  unmappedConstituency:unmappedConstituency,
  unmappedElection:unmappedElection,
  userblock:userblock,
  usercountry:usercountry,
  usercollege:usercollege,
  userdistrict:userdistrict,
  userstate:userstate,
  usersociety:usersociety,
  usertehsil:usertehsil,
  uservillage:uservillage,
  userward:userward,
  villageAnalysis:villageAnalysis,
  villagelist:villagelist,
  wardAnalysis:wardAnalysis,
  wardlist:wardlist,
})

export default rootReducer
