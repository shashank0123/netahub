const initialState = {
    profilepic: {},
    profilepic_error:{}
  };
  
  export const profilepic = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,profilepic:action.payload, profilepic_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,profilepic_error:action.payload, profilepic:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default profilepic;