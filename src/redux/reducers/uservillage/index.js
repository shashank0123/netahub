const initialState = {
    uservillage: [],
    uservillage_error:{}
  };
  
  export const uservillage = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,uservillage:action.payload, uservillage_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,uservillage_error:action.payload, uservillage:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default uservillage;