const initialState = {
    editState: [],
    editState_error:{}
  };
  
  export const editState = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,editState:action.payload, editState_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,editState_error:action.payload, editState:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default editState;