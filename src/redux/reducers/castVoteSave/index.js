const initialState = {
    castVoteSave: [],
    castVoteSave_error:{}
  };
  
  export const castVoteSave = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,castVoteSave:action.payload, castVoteSave_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,castVoteSave_error:action.payload, castVoteSave:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default castVoteSave;