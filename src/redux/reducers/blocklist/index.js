const initialState = {
    blocklist: [],
    blocklist_error:{}
  };
  
  export const blocklist = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,blocklist:action.payload, blocklist_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,blocklist_error:action.payload, blocklist:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default blocklist;