const initialState = {
    editBlock: [],
    editBlock_error:{}
  };
  
  export const editBlock = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,editBlock:action.payload, editBlock_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,editBlock_error:action.payload, editBlock:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default editBlock;