const initialState = {
    profile: [],
    profile_error:{}
  };
  
  export const profile = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        console.log(action.payload)
        return { ...state,profile:action.payload, profile_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,profile_error:action.payload, profile:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default profile;