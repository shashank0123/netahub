const initialState = {
    dashboard: [],
    dashboard_error:{}
  };
  
  export const dashboard = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,dashboard:action.payload, dashboard_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,dashboard_error:action.payload, dashboard:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default dashboard;