const initialState = {
    nagarPanchayatAnalysis: [],
    nagarPanchayatAnalysis_error:{}
  };
  
  export const nagarPanchayatAnalysis = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,nagarPanchayatAnalysis:action.payload, nagarPanchayatAnalysis_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,nagarPanchayatAnalysis_error:action.payload, nagarPanchayatAnalysis:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default nagarPanchayatAnalysis;