const initialState = {
    genderAnalysis: [],
    genderAnalysis_error:{}
  };
  
  export const genderAnalysis = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,genderAnalysis:action.payload, genderAnalysis_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,genderAnalysis_error:action.payload, genderAnalysis:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default genderAnalysis;