const initialState = {
    stateAnalysis: [],
    stateAnalysis_error:{}
  };
  
  export const stateAnalysis = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,stateAnalysis:action.payload, stateAnalysis_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,stateAnalysis_error:action.payload, stateAnalysis:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default stateAnalysis;