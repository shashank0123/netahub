const initialState = {
    usersociety: [],
    usersociety_error:{}
  };
  
  export const usersociety = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,usersociety:action.payload, usersociety_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,usersociety_error:action.payload, usersociety:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default usersociety;