const initialState = {
    villageAnalysis: [],
    villageAnalysis_error:{}
  };
  
  export const villageAnalysis = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,villageAnalysis:action.payload, villageAnalysis_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,villageAnalysis_error:action.payload, villageAnalysis:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default villageAnalysis;