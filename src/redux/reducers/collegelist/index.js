const initialState = {
    collegelist: [],
    collegelist_error:{}
  };
  
  export const collegelist = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,collegelist:action.payload, collegelist_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,collegelist_error:action.payload, collegelist:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default collegelist;