const initialState = {
    blockAnalysis: [],
    blockAnalysis_error:{}
  };
  
  export const blockAnalysis = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,blockAnalysis:action.payload, blockAnalysis_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,blockAnalysis_error:action.payload, blockAnalysis:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default blockAnalysis;