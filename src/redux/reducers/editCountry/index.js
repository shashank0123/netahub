const initialState = {
    editCountry: [],
    editCountry_error:{}
  };
  
  export const editCountry = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,editCountry:action.payload, editCountry_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,editCountry_error:action.payload, editCountry:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default editCountry;