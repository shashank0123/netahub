const initialState = {
    userward: [],
    userward_error:{}
  };
  
  export const userward = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,userward:action.payload, userward_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,userward_error:action.payload, userward:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default userward;