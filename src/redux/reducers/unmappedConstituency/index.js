const initialState = {
    unmappedConstituency: [],
    unmappedConstituency_error:{}
  };
  
  export const unmappedConstituency = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,unmappedConstituency:action.payload, unmappedConstituency_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,unmappedConstituency_error:action.payload, unmappedConstituency:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default unmappedConstituency;