const initialState = {
    addConstituency: [],
    addConstituency_error:{}
  };
  
  export const addConstituency = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,addConstituency:action.payload, addConstituency_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,addConstituency_error:action.payload, addConstituency:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default addConstituency;