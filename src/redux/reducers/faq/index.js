const initialState = {
    faq: [],
    faq_error:{}
  };
  
  export const faq = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,faq:action.payload, faq_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,faq_error:action.payload, faq:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default faq;