const initialState = {
    qualificationlist: [],
    qualificationlist_error:{}
  };
  
  export const qualificationlist = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,qualificationlist:action.payload, qualificationlist_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,qualificationlist_error:action.payload, qualificationlist:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default qualificationlist;