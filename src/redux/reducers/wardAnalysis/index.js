const initialState = {
    wardAnalysis: [],
    wardAnalysis_error:{}
  };
  
  export const wardAnalysis = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,wardAnalysis:action.payload, wardAnalysis_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,wardAnalysis_error:action.payload, wardAnalysis:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default wardAnalysis;