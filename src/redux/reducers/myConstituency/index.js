const initialState = {
    myConstituency: [],
    myConstituency_error:{}
  };
  
  export const myConstituency = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,myConstituency:action.payload, myConstituency_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,myConstituency_error:action.payload, myConstituency:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default myConstituency;