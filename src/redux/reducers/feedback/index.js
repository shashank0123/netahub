const initialState = {
    feedback: [],
    feedback_error:{}
  };
  
  export const feedback = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,feedback:action.payload, feedback_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,feedback_error:action.payload, feedback:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default feedback;