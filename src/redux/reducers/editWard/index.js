const initialState = {
    editWard: [],
    editWard_error:{}
  };
  
  export const editWard = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,editWard:action.payload, editWard_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,editWard_error:action.payload, editWard:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default editWard;