const initialState = {
    CastVote: [],
    CastVote_error:{}
  };
  
  export const CastVote = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,CastVote:action.payload, CastVote_error:"" };
      }
      case 'FAILED': {
        console.log(action.payload)
        return { ...state,CastVote_error:action.payload, CastVote:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default CastVote;