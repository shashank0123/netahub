const initialState = {
    tehsilAnalysis: [],
    tehsilAnalysis_error:{}
  };
  
  export const tehsilAnalysis = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,tehsilAnalysis:action.payload, tehsilAnalysis_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,tehsilAnalysis_error:action.payload, tehsilAnalysis:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default tehsilAnalysis;