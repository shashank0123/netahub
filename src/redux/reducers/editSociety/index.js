const initialState = {
    editSociety: [],
    editSociety_error:{}
  };
  
  export const editSociety = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,editSociety:action.payload, editSociety_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,editSociety_error:action.payload, editSociety:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default editSociety;