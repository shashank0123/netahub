const initialState = {
    reportSurvey: [],
    reportSurvey_error:{}
  };
  
  export const reportSurvey = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,reportSurvey:action.payload, reportSurvey_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,reportSurvey_error:action.payload, reportSurvey:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default reportSurvey;