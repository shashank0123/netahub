const initialState = {
    statelist: [],
    statelist_error:{}
  };
  
  export const statelist = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,statelist:action.payload, statelist_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,statelist_error:action.payload, statelist:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default statelist;