const initialState = {
    ageAnalysis: [],
    ageAnalysis_error:{}
  };
  
  export const ageAnalysis = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,ageAnalysis:action.payload, ageAnalysis_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,ageAnalysis_error:action.payload, ageAnalysis:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default ageAnalysis;