const initialState = {
    usercountry: [],
    usercountry_error:{}
  };
  
  export const usercountry = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,usercountry:action.payload, usercountry_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,usercountry_error:action.payload, usercountry:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default usercountry;