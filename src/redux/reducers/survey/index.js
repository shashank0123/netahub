const initialState = {
    survey: [],
    survey_error:{}
  };
  
  export const survey = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,survey:action.payload, survey_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,survey_error:action.payload, survey:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default survey;