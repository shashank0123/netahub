const initialState = {
    elections: [],
    elections_error:{}
  };
  
  export const elections = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,elections:action.payload, elections_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,elections_error:action.payload, elections:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default elections;