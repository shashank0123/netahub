const initialState = {
    userblock: [],
    userblock_error:{}
  };
  
  export const userblock = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,userblock:action.payload, userblock_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,userblock_error:action.payload, userblock:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default userblock;