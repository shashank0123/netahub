const initialState = {
    wardlist: [],
    wardlist_error:{}
  };
  
  export const wardlist = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,wardlist:action.payload, wardlist_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,wardlist_error:action.payload, wardlist:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default wardlist;