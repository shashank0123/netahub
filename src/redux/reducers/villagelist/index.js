const initialState = {
    villagelist: [],
    villagelist_error:{}
  };
  
  export const villagelist = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,villagelist:action.payload, villagelist_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,villagelist_error:action.payload, villagelist:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default villagelist;