const initialState = {
    contribute: [],
    contribute_error:{}
  };
  
  export const contribute = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,contribute:action.payload, contribute_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,contribute_error:action.payload, contribute:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default contribute;