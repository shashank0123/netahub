const initialState = {
    nomineeDetails: [],
    nomineeDetails_error:{}
  };
  
  export const nomineeDetails = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,nomineeDetails:action.payload, nomineeDetails_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,nomineeDetails_error:action.payload, nomineeDetails:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default nomineeDetails;