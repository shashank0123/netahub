const initialState = {
    login: {}
  };
  
  export const login = (state = initialState, action) => {
    switch (action.type) {
      case 'LOGIN': {
        // console.log(action.payload)
        return { ...state,login:action.payload };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,login_error:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default login;