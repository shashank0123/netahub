const initialState = {
    districtAnalysis: [],
    districtAnalysis_error:{}
  };
  
  export const districtAnalysis = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,districtAnalysis:action.payload, districtAnalysis_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,districtAnalysis_error:action.payload, districtAnalysis:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default districtAnalysis;