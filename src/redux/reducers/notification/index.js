const initialState = {
    notification: [],
    notification_error:{}
  };
  
  export const notification = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,notification:action.payload, notification_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,notification_error:action.payload, notification:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default notification;