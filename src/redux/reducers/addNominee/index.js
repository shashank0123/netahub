const initialState = {
    addNominee: [],
    addNominee_error:{}
  };
  
  export const addNominee = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,addNominee:action.payload, addNominee_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,addNominee_error:action.payload, addNominee:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default addNominee;