const initialState = {
    editCollege: [],
    editCollege_error:{}
  };
  
  export const editCollege = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,editCollege:action.payload, editCollege_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,editCollege_error:action.payload, editCollege:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default editCollege;