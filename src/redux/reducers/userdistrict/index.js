const initialState = {
    userdistrict: [],
    userdistrict_error:{}
  };
  
  export const userdistrict = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,userdistrict:action.payload, userdistrict_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,userdistrict_error:action.payload, userdistrict:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default userdistrict;