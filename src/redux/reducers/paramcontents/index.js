const initialState = {
    paramcontents: [],
    paramcontents_error:{}
  };
  
  export const paramcontents = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,paramcontents:action.payload, paramcontents_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,paramcontents_error:action.payload, paramcontents:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default paramcontents;