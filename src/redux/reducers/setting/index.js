const initialState = {
    setting: [],
    setting_error:{}
  };
  
  export const setting = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,setting:action.payload, setting_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,setting_error:action.payload, setting:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default setting;