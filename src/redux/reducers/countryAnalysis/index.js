const initialState = {
    countryAnalysis: [],
    countryAnalysis_error:{}
  };
  
  export const countryAnalysis = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,countryAnalysis:action.payload, countryAnalysis_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,countryAnalysis_error:action.payload, countryAnalysis:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default countryAnalysis;