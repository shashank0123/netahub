const initialState = {
    insertUserCons: [],
    insertUserCons_error:{}
  };
  
  export const insertUserCons = (state = initialState, action) => {
    switch (action.type) {
      case 'ELECTION': {
        // console.log(action.payload)
        return { ...state,insertUserCons:action.payload, insertUserCons_error:"" };
      }
      case 'FAILED': {
        // console.log(action.payload)
        return { ...state,insertUserCons_error:action.payload, insertUserCons:[] };
      }
      default: {
        return state;
      }
    }
  };
  export default insertUserCons;