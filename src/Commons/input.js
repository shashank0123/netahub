import React, {Component} from 'react';
import {View, Platform, StyleSheet,Dimensions,TouchableOpacity,Image,Alert} from 'react-native';
import {Text } from 'react-native-elements';
import { Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Images } from '../utils'

const { width, height } = Dimensions.get('window');
let age =  100;

export default class InputField extends Component {
  
  createTwoButtonAlert = () =>
  Alert.alert(
    `${this.props.alertMessage}`
  );

  render(props) {
    return (
      <View style={styles.container}>
        <Text style={styles.inputLabel}>{this.props.name}</Text>
        <View style={{flexDirection:'row',justifyContent:'flex-start',alignItems:'center'}}>
        <Input 
        style={{fontSize:14,fontWeight:'bold',width:"80%",marginTop:8,paddingRight:18}}
        placeholder={this.props.placeholder}
        secureTextEntry={this.props.secureTextEntry}
        onChangeText={this.props.onChangeText}
        keyboardType={this.props.keyboardType}
        maxLength={this.props.maxLength}
       
     
        errorStyle={{color: 'red'}}
        errorMessage={this.props.errorMessage}
         
        />
          <TouchableOpacity  style={{position:'absolute', left:'90%',top:'27%'}}
          onPress={this.createTwoButtonAlert}>
              <Image source={Images.info} style={styles.profileIcon} />
            </TouchableOpacity>
        </View>
        
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    
    marginLeft: 5,
    // paddingTop: 10,
  },
  inputLabel: {
    paddingLeft: 15,
    fontWeight:'bold'
    

  },
  profileIcon: {
    height: height * 0.025,
    width: height * 0.025,
    borderRadius: 50,
    marginLeft: width * 0.03,
},
});
