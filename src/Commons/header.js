import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Dimensions,
  Image, Picker, FlatList, Modal,
  I18nManager,
  RefreshControl,
  fetchData,
  StyleSheet
} from "react-native";
import memoize from "lodash.memoize";
const { width, height } = Dimensions.get('window');
import { Images } from '../utils';
import i18n from "i18n-js";
// const translationGetters = {
//   // lazy requires (metro bundler does not support symlinks)
//   en: () => require("../../assets/en-US.json"),
//   hi: () => require("../../assets/hi.json")
// };
const translate = memoize(
  (key, config) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key)
);
class Header extends Component {
  constructor(props) {
    super(props);
   
    this.state = {
      show: false,
      profile: {},
      profile_pic: 'user.png',
      updatestatus: '1'
    }
  }
  render() {
    return (
      <View style={styles.upperBar}>
        <View style={styles.backIconContainer}>
          <TouchableOpacity
            onPress={this.props.onPressBack}
          >
            <Image source={Images.backIcon} style={styles.barMenuIcon} />
          </TouchableOpacity>
        </View>
        <View style={styles.headingContainer}>
          <Text style={styles.barText}>{translate(this.props.title)}</Text>
        </View>
        {/* <View style={{ marginLeft: -10 }}>
          <TouchableOpacity
            onPress={this.props.onPressNotification}
          >
            <Image source={this.props.bellIcon} style={styles.bellIcon1} />
          </TouchableOpacity>
        </View> */}
         
         { this.props.notification ? 
       <View style={{ marginLeft: -10 }}>
       <TouchableOpacity
        onPress={() =>
          this.props.navigation.navigate('NotificationScreen')}
      >
         <Image source={Images.bell} style={styles.bellIcon1} />
       </TouchableOpacity>
     </View>
        : <></>
  }
        <View>
          <TouchableOpacity
            onPress={this.props.onPressProfile}
          >
            <Image source={{ uri: 'https://netahub.com/images/user/' + this.props.profile_pic }} style={styles.profileIcon} />
          </TouchableOpacity>
        </View>

{/* { this.props.profilePic ? 
       <View style={{ marginLeft: -10 }}>
       <TouchableOpacity
        onPress={() =>
          this.props.navigation.navigate('NotificationScreen')}
      >
         <Image source={Images.bell} style={styles.bellIcon1} />
       </TouchableOpacity>
     </View>
        : <></>
  } */}


        { this.props.refer ? 
        <View style={{ marginLeft: 8 }}>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('ReferAndEarn')}
          >
            <Image source={Images.refer} style={styles.bellIcon} />
          </TouchableOpacity>
        </View>
        : <></>
  }
      </View>
    );
  }
}
export default Header;
const styles = StyleSheet.create({
  upperBar: {
    width,
    height: height * 0.075,
    backgroundColor: '#00b8e6',
    flexDirection: 'row',
    alignItems: 'center',
  },
  backIconContainer: {
  },
  headingContainer: {
    width: '50%',
    marginLeft: '10%',
    alignItems: 'center',
  },
  barText: {
    color: "#000000",
    fontSize: height * 0.03,
    fontWeight: 'bold'
  },
  barMenuIcon: {
    height: height * 0.04,
    width: height * 0.04,
    marginLeft: width * 0.04,
  },
  profileIcon: {
    height: height * 0.045,
    width: height * 0.045,
    borderRadius: 50,
    marginLeft: width * 0.02,
  },
  bellIcon: {
    height: height * 0.038,
    width: height * 0.0426,
  },
  bellIcon1: {
    height: height * 0.038,
    width: height * 0.035,
  },
});