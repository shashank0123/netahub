export default {
  backIcon: require('../assets/Images/back.png'),
  parlmnt: require('../assets/Images/parl.jpeg'),
  share: require('../assets/Images/share.png'),
  // share1: require('../assets/Images/share1.png'),
  home: require('../assets/Images/home.png'),
  profilePic: require('../assets/Images/profile_pic.png'),
  gender: require('../assets/Images/gender.png'),
  block: require('../assets/Images/block.png'),
  state: require('../assets/Images/states.png'),
  district: require('../assets/Images/district.png'),
  country: require('../assets/Images/countries.png'),
  ward: require('../assets/Images/ward.png'),
  village: require('../assets/Images/village.png'),
  tehsil: require('../assets/Images/tehsil.png'),
  age: require('../assets/Images/age.png'),
  nagar: require('../assets/Images/nagar.png'),
  netaLogo: require('../assets/Images/netahub.png'),
  date_calendar: require('../assets/Images/calender_1.png'),
  netaHubLogo: require('../assets/Images/netaHubLogo.jpeg'),
  pencil: require('../assets/Images/pencil.png'),
  // refer: require('../assets/Images/refer.png'),
  refer: require('../assets/Images/Refer&eran.png'),
  // bell: require('../assets/Images/alarm.png'),
  bell: require('../assets/Images/BellIcon.png'),
  delet: require('../assets/Images/delet.png'),
  trash: require('../assets/Images/trash.png'),
  googleLogo: require('../assets/Images/googleLogo.png'),
  fbLogo: require('../assets/Images/fbLogo.png'),
  dropDown: require('../assets/Images/dropDown.png'),

  dashboard1: require('../assets/Images/Dashboard1.jpg'),
  dashboard2: require('../assets/Images/Dashboard2.jpg'),
  election1: require('../assets/Images/Election1.jpg'),
  election2: require('../assets/Images/Election2.jpg'),
  report1: require('../assets/Images/Report1.jpg'),
  report2: require('../assets/Images/Report2.jpg'),
  settings1: require('../assets/Images/Settings1.jpg'),
  settings2: require('../assets/Images/Settings2.jpg'),
  info: require('../assets/Images/info.png'),
  prof: require('../assets/Images/myprofile.png'),
  constituency: require('../assets/Images/constituency.png'),
  feedback: require('../assets/Images/feedback.png'),
  faq: require('../assets/Images/faq.png'),
  logout: require('../assets/Images/logout.png'),
  wallet: require('../assets/Images/wallet.png'),
  myProfile: require('../assets/Images/myprofile.png'),

  reffrSignup: require('../assets/Images/referSignup.png'),
  reffrSignup2: require('../assets/Images/reffrSignup2.png'),

  reffrRegistr: require('../assets/Images/reffrRegistr.png'),
  reffrRegistr2: require('../assets/Images/reffrRegistr2.png'),

  refferReward: require('../assets/Images/refferReward.png'),
  invitation: require('../assets/Images/invitation.png'),
  profile1: require('../assets/Images/profile1.png'),
  profile2: require('../assets/Images/profile2.png'),
  star: require('../assets/Images/star.png'),
  palceholderImage: require('../assets/Images/user.png'),









};
